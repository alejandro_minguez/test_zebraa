﻿
/*

"--> Comprobar expresión para SqlStatementSource para ver SQL

EXEC [LOG].[SP_EXECUTION_START]
N'" + @[System::ExecutionInstanceGUID] + "',
N'" + @[System::PackageID]   + "',
N'" + @[System::PackageName] + "',
N'" + @[System::UserName]    + "',
0,
N'EN PROGRESO',
0,
0;"


EXEC [LOG].[SP_EXECUTION]
N'{D77A2674-C555-45C9-9FC9-B9113A6E3062}',
N'{2E8F5EFA-9F89-4B4A-9CA5-2950D6708CD0}',
N'_PLANTILLA_HOTUSA',
N'BISMART\alex.minguez',
0,
N'EN PROGRESO',
0,
0;

*/


/*

"--Código a implementar en el control
EXEC [LOG].[SP_EXECUTION_END]
N'" + @[System::ExecutionInstanceGUID] + "',
N'FINALIZADO',
" + (DT_WSTR,10)@[User::Regerr] +",
" + (DT_WSTR,10)@[User::Regerr] +";"

--Código a implementar en el control
EXEC [LOG].[SP_EXECUTION_END]
N'{D77A2674-C555-45C9-9FC9-B9113A6E3062}',
N'FINALIZADO',
0,
0;
*/

CREATE procedure [LOG].[SP_EXECUTION_END] 
  	@ExecutionInstanceGUID varchar(80),
	@Status varchar(100),
	@Regok	int,
	@Regerr int
	AS
BEGIN

UPDATE [LOG].[EXECUTION]
	SET
	 FEC_FIN_EJEC = GETDATE()
	,COD_ESTADO   = 1
	,DES_ESTADO   = @Status
	,DES_REGOK    = @Regok
	,DES_REGERR   = @Regerr
WHERE [ExecutionInstanceGUID] = @ExecutionInstanceGUID

END