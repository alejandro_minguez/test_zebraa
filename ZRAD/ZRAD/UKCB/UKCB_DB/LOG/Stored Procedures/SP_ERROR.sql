﻿CREATE PROCEDURE [LOG].[SP_ERROR] (
				  @Paquete varchar(40)
				, @Tarea varchar(40)
				, @ErrorCode int
				, @Observaciones nvarchar(254)
				, @Fecha varchar(40)
				, @Usuario varchar(40)
				, @IdentificadorEjecucion varchar(80)
				)
AS
BEGIN
	SET NOCOUNT ON;	
	INSERT INTO [lOG].[ERROR]
		( [DES_SSISPACK]
		 ,[DES_SSISTARE]
		 ,[COD_SSISERRO]
		 ,[DES_OBSERLOG]
		 ,[FEC_SSISEJEC]
		 ,[DES_SSISUSUA]
		 ,[DES_IDENSSIS])
	VALUES ( @Paquete
	        ,@Tarea
			,@ErrorCode
			,@Observaciones
			,CONVERT (DATETIME ,@Fecha,121)
			,@Usuario
			,@IdentificadorEjecucion)
END