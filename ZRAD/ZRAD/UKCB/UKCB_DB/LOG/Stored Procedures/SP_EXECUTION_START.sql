﻿

CREATE procedure [LOG].[SP_EXECUTION_START] 
	@ExecutionInstanceGUID varchar(80),
	@PackageID varchar(80),
  	@Package varchar(100),
  	@ExecutedBy varchar(100),
	@CodStatus int,
  	@Status  varchar(100),
	@Regok	int,
	@Regerr int
	 as
BEGIN

	INSERT INTO [LOG].[EXECUTION]
			  ( [ExecutionInstanceGUID]
			   ,[ID_PAQUETE]
			   ,[DES_PAQUETE]
			   ,[FEC_EJEC]
			   ,[DES_USUARIO]
			   ,[COD_ESTADO]
			   ,[DES_ESTADO]
			   ,[DES_REGOK]
			   ,[DES_REGERR])
	VALUES(@ExecutionInstanceGUID
	      ,@PackageID
	      ,@Package
		  ,getdate()
		  ,@ExecutedBy
		  ,@CodStatus
		  ,@Status
		  ,@Regok
		  ,@Regerr)

END