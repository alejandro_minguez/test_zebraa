﻿CREATE TABLE [STG].[CLAIM_MOVEMENT_FACT] (
    [CLMID]                        VARCHAR (100)   NULL,
    [UWDAT]                        DATETIME        NULL,
    [TRANDAT]                      DATETIME        NULL,
    [NTRANS]                       INT             NULL,
    [AMOUNT]                       DECIMAL (15, 2) NULL,
    [MOVEMENT_CATEGORY_IDENTIFIER] INT             NULL,
    [CLAIM_STATUS_IDENTIFIER]      INT             NULL
);

