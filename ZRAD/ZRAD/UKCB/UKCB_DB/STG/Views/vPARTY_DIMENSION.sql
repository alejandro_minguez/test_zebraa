﻿


CREATE VIEW  [STG].[vPARTY_DIMENSION] AS

WITH CTE AS (SELECT DISTINCT [SUBBCH] FROM [STG].[ENGINEERING] WITH(NOLOCK) WHERE [SUBBCH] !='-3')

SELECT 
		[TEXT_IDENTIFIER] AS [PARTY_ANCHOR_IDENTIFIER],
		[TEXT_CODE] AS [PARTY_CODE],
		[TEXT_NAME] AS [PARTY_NAME],
		[TEXT_DESCRIPTION] AS [PARTY_DESCRIPTION],
        [TEXT_CODE] AS [PARTY_IDENTIFICATION_NUMBER],
        CAST('19000101' AS DATETIME) AS [PARTY_ANNIVERSARY_DATE],
        [TEXT_IDENTIFIER] AS [PARTY_TYPE_IDENTIFIER],
        [TEXT_IDENTIFIER] AS [GENDER_TYPE_IDENTIFIER],
        [TEXT_IDENTIFIER] AS [MARITAL_STATUS_IDENTIFIER],
		GETDATE() AS [RECORD_INCLUSION_DATE],
		GETDATE() AS [RECORD_UPDATE_DATE],
		[SOURCE_SYSTEM_IDENTIFIER] AS [SOURCE_SYSTEM_IDENTIFIER]
FROM [STG].[TEXT]

UNION ALL

SELECT
       ROW_NUMBER() OVER(ORDER BY [SUBBCH]) AS [PARTY_ANCHOR_IDENTIFIER],
       [SUBBCH] AS  [PARTY_CODE],
       [SUBBCH] AS  [PARTY_NAME],
       [SUBBCH] AS  [PARTY_DESCRIPTION],
       '-1' AS [PARTY_IDENTIFICATION_NUMBER],
       CAST('19000101' AS DATETIME) AS [PARTY_ANNIVERSARY_DATE],
       1 AS [PARTY_TYPE_IDENTIFIER],
       -1 AS [GENDER_TYPE_IDENTIFIER],
       -1 AS [MARITAL_STATUS_IDENTIFIER],
       GETDATE() AS [RECORD_INCLUSION_DATE],
       GETDATE() AS [RECORD_UPDATE_DATE],
       -1 AS [SOURCE_SYSTEM_IDENTIFIER]
FROM CTE