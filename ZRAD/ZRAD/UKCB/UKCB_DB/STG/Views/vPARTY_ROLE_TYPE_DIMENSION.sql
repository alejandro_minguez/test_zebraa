﻿


CREATE VIEW [STG].[vPARTY_ROLE_TYPE_DIMENSION] AS 

/*Insertamos los miembros desconocidos*/
SELECT 
	  [TEXT_IDENTIFIER] AS [PARTY_ROLE_TYPE_ANCHOR_IDENTIFIER],
      [TEXT_CODE] AS [PARTY_ROLE_TYPE_CODE],
      [TEXT_NAME] AS [PARTY_ROLE_TYPE_NAME],
      [TEXT_DESCRIPTION] AS [PARTY_ROLE_TYPE_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      [SOURCE_SYSTEM_IDENTIFIER] AS [SOURCE_SYSTEM_IDENTIFIER]
FROM [STG].[TEXT]

UNION ALL

/*Insertamos el resto de valores*/
SELECT 
	  1 AS [PARTY_ROLE_TYPE_ANCHOR_IDENTIFIER],
      'BR' AS [PARTY_ROLE_TYPE_CODE],
      'BRANCH' AS [PARTY_ROLE_TYPE_NAME],
      'BRANCH' AS [PARTY_ROLE_TYPE_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      -1 AS [SOURCE_SYSTEM_IDENTIFIER]