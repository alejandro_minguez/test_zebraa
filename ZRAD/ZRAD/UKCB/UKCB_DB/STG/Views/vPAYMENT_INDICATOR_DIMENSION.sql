﻿


CREATE VIEW [STG].[vPAYMENT_INDICATOR_DIMENSION] AS 

/*Insertamos los miembros desconocidos*/
SELECT 
	  [TEXT_IDENTIFIER] AS [PAYMENT_INDICATOR_ANCHOR_IDENTIFIER],
      [TEXT_CODE] AS [PAYMENT_INDICATOR_CODE],
      [TEXT_NAME] AS [PAYMENT_INDICATOR_NAME],
      [TEXT_DESCRIPTION] AS [PAYMENT_INDICATOR_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      [SOURCE_SYSTEM_IDENTIFIER] AS [SOURCE_SYSTEM_IDENTIFIER]
FROM [STG].[TEXT]

UNION ALL

/*Insertamos el resto de valores*/
SELECT 
	  1 AS [PAYMENT_INDICATOR_ANCHOR_IDENTIFIER],
      '1' AS [PAYMENT_INDICATOR_CODE],
      'EXECUTED' AS [PAYMENT_INDICATOR_NAME],
      'EXECUTED' AS [PAYMENT_INDICATOR_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      -1 AS [SOURCE_SYSTEM_IDENTIFIER]

UNION ALL

SELECT 
	  2 AS [PAYMENT_INDICATOR_ANCHOR_IDENTIFIER],
      '2' AS [PAYMENT_INDICATOR_CODE],
      'CANCELED' AS [PAYMENT_INDICATOR_NAME],
      'CANCELED' AS [PAYMENT_INDICATOR_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      -1 AS [SOURCE_SYSTEM_IDENTIFIER]

UNION ALL

SELECT 
	  3 AS [PAYMENT_INDICATOR_ANCHOR_IDENTIFIER],
      '3' AS [PAYMENT_INDICATOR_CODE],
      'PAID' AS [PAYMENT_INDICATOR_NAME],
      'PAID' AS [PAYMENT_INDICATOR_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      -1 AS [SOURCE_SYSTEM_IDENTIFIER]