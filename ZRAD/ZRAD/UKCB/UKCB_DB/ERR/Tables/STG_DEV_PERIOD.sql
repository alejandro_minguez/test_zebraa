﻿CREATE TABLE [ERR].[STG_DEV_PERIOD] (
    [TECH_PARAMETER_CLASS] VARCHAR (100) NULL,
    [TECH_PARAMETER_CODE]  VARCHAR (100) NULL,
    [DOMAIN]               VARCHAR (50)  NULL,
    [FULL_DESCRIPTION]     VARCHAR (255) NULL,
    [USE]                  VARCHAR (3)   NULL,
    [DATEVALID_FROM]       DATE          NULL,
    [DATEVALID_TO]         DATE          NULL,
    [$sq_row_hash]         BIGINT        NULL,
    [$sq_execution_id]     BIGINT        NOT NULL,
    [$sq_operation_id]     TINYINT       NULL
);

