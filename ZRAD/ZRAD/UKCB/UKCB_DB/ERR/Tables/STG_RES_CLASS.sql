﻿CREATE TABLE [ERR].[STG_RES_CLASS] (
    [RESERVING_CLASS_NAME] VARCHAR (100) NULL,
    [RESERVING_CLASS_CODE] VARCHAR (50)  NULL,
    [MIN_BASE_DATE_FILTER] DATETIME      NULL,
    [LARGE_LOSS_SPLIT]     VARCHAR (3)   NULL,
    [LARGE_LOSS_METHOD]    VARCHAR (50)  NULL,
    [DATEVALID_TO]         DATETIME      NULL,
    [DATEVALID_FROM]       DATETIME      NULL,
    [COUNTRY]              VARCHAR (10)  NULL,
    [BUSINESS_UNIT]        VARCHAR (10)  NULL,
    [$sq_row_hash]         BIGINT        NULL,
    [$sq_execution_id]     BIGINT        NOT NULL,
    [$sq_operation_id]     TINYINT       NULL
);

