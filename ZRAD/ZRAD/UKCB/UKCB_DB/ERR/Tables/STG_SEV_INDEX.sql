﻿CREATE TABLE [ERR].[STG_SEV_INDEX] (
    [SEVERITY_INDEX_ID]          VARCHAR (50)  NULL,
    [SEVERITY_INDEX_NAME]        VARCHAR (255) NULL,
    [CURRENCY]                   VARCHAR (10)  NULL,
    [FIRST_YEARMONTH]            INT           NULL,
    [LAST_YEARMONTH]             INT           NULL,
    [BACKWARD_MONTHLY_INFLATION] FLOAT (53)    NULL,
    [FORWARD_MONTHLY_INFLATION]  FLOAT (53)    NULL,
    [DATEVALID_FROM]             DATETIME      NULL,
    [DATEVALID_TO]               DATETIME      NULL,
    [$sq_row_hash]               BIGINT        NULL,
    [$sq_execution_id]           BIGINT        NOT NULL,
    [$sq_operation_id]           TINYINT       NULL
);

