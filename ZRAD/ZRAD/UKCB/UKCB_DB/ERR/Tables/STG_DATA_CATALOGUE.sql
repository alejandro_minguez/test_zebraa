﻿CREATE TABLE [ERR].[STG_DATA_CATALOGUE] (
    [CATALOGUE_ATTRIBUTE_NAME]        VARCHAR (100)  NULL,
    [CATALOGUE_ATTRIBUTE_DESCRIPTION] VARCHAR (255)  NULL,
    [ZRAD_TABLE_NAME]                 VARCHAR (75)   NULL,
    [ZRAD_TABLE_ALIAS_NAME]           VARCHAR (20)   NULL,
    [ZRAD_TABLE_COLUMN_NAME]          VARCHAR (75)   NULL,
    [ZRAD_JOIN_TYPE]                  VARCHAR (20)   NULL,
    [ZRAD_JOIN_TABLE]                 VARCHAR (1000) NULL,
    [ZRAD_JOIN_FIELDS]                VARCHAR (350)  NULL,
    [ZRAD_JOIN_FILTER_CONDITION]      VARCHAR (1000) NULL,
    [ZRAD_JOIN_ALIAS]                 VARCHAR (20)   NULL,
    [ZRAD_JOIN_ORDER]                 INT            NULL,
    [ZRAD_FILTER_CONDITION]           VARCHAR (1000) NULL,
    [$sq_row_hash]                    BIGINT         NULL,
    [$sq_execution_id]                BIGINT         NOT NULL,
    [$sq_operation_id]                TINYINT        NULL
);

