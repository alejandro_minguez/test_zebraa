﻿

CREATE PROCEDURE [SEG].[SP_RESERVING_CLASS_CLASIFICATION] AS

DECLARE @bu AS VARCHAR(10)='UKZM'
DECLARE @query VARCHAR(MAX)

---------------------------------------------
/* OBTENEMOS LA LISTA DE COLUMNAS A MOSTRAR*/
---------------------------------------------

DECLARE @columnName AS VARCHAR(200)
DECLARE @listaColumnas AS VARCHAR(MAX)=''
DECLARE C_COLUMNAS CURSOR FOR
       SELECT COLUMN_NAME FROM [ZQ].[VW_MAIN_CLASS_COLUMNS] -- Cogemos las columnas fijas
	   UNION ALL
	   SELECT COLUMN_NAME FROM [ZQ].[VW_SPLIT_CLASS_COLUMNS] -- Cogemos las columnas necesarias para la clasificación de claims en clases

OPEN C_COLUMNAS

FETCH NEXT FROM C_COLUMNAS   
INTO @columnName

WHILE @@FETCH_STATUS = 0  
BEGIN  
       PRINT @columnName
       SET @listaColumnas=@listaColumnas+','+@columnName+ CHAR(13) + CHAR(10)
       FETCH NEXT FROM C_COLUMNAS   
       INTO @columnName
END   
CLOSE C_COLUMNAS 
DEALLOCATE C_COLUMNAS

--SELECT @listaColumnas


-------------------------------------------
/* OBTENEMOS LA LISTA DE JOINS A REALIZAR*/
-------------------------------------------

DECLARE @join AS VARCHAR(MAX)
DECLARE @listaJoins AS VARCHAR(MAX)=''
DECLARE C_JOINS CURSOR FOR
	   SELECT [JOIN_TO_EXECUTE] FROM (
       SELECT [JOIN_TO_EXECUTE], [JOIN_ORDER] FROM [ZQ].[VW_SPLIT_CLASS_JOINS] -- Joins necesarios para la calisifación de claims en clases
	   UNION ALL
       SELECT [JOIN_TO_EXECUTE], [JOIN_ORDER] FROM [ZQ].[VW_MAIN_CLASS_JOINS]  -- Joins necesarios para obtener las columnas fijas
	   ) BASE ORDER BY [JOIN_ORDER]

OPEN C_JOINS

FETCH NEXT FROM C_JOINS   
INTO @join

WHILE @@FETCH_STATUS = 0  
BEGIN  
       PRINT @join
       SET @listaJoins=@listaJoins+@join+ CHAR(13) + CHAR(10)
       FETCH NEXT FROM C_JOINS   
       INTO @join
END   
CLOSE C_JOINS 
DEALLOCATE C_JOINS

--SELECT @listaJoins


--------------------------------------------------------------
/* CREAMOS LOS CASE PARA UTILIZAR LAS EXPRESIONES RESULARES */
--------------------------------------------------------------

DECLARE @case1 AS VARCHAR(MAX)
DECLARE @case2 AS VARCHAR(MAX)

--si solo tenemos 1 atributo no necesitamos el case
IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]) = 1
BEGIN
	SELECT DISTINCT @case1=LEVEL_1_TRASLATE_NAME FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]
END
ELSE
BEGIN
	SET @case1 = 'CASE LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)
	SELECT  DISTINCT @case1 = @case1 +'WHEN '''+LEVEL_1_CATALOG_NAME+''' THEN '+LEVEL_1_TRASLATE_NAME + CHAR(13) + CHAR(10)
	FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]
	SET  @case1 = @case1 + 'ELSE '''' 
	END'
END
--SELECT @case1

--si solo tenemos 1 atributo no necesitamos el case
-- MVG: DEBERÍAMOS CONTROLAR EL CASO DE QUE NO HAYA NINGÚN ATRIBUTO DE SEGUNDO NIVEL? SERÍA RECOMENDABLE, PARA NO METER EL CASE.
IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]) = 1
BEGIN
	SELECT DISTINCT @case2=LEVEL_2_TRASLATE_NAME FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]
END
ELSE
BEGIN
	SET @case2 = 'CASE LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)
	SELECT  DISTINCT @case2 = @case2 +'WHEN '''+LEVEL_2_CATALOG_NAME+''' THEN '+LEVEL_2_TRASLATE_NAME + CHAR(13) + CHAR(10)
	FROM [ZQ].[VW_SPLIT_CLASS_TRASLATIONS]
	SET  @case2 = @case2 + 'ELSE '''' 
	END'
END
--SELECT @case2


/*formamos consulta*/
SET @query = 'SELECT COALESCE(RCD.[RESERVING_CLASS_CODE],'''+@bu+'_UNDEF'') AS [RESERVING_CLASS_CODE],
COALESCE(RCD.[RESERVING_CLASS_NAME],''Undefined'') AS [RESERVING_CLASS_NAME]
'+ @listaColumnas +
' FROM [ZRAD].[CLAIM_MOVEMENT_FACT] AS CMF WITH (NOLOCK)
'+ @listaJoins
+ 'LEFT JOIN [ZQ].[VW_SPLIT_CLASS_TRASLATIONS] AS RCD WITH (NOLOCK) ON 
[dbo].[RegExIsMatch](COALESCE(RCD.[LEVEL_1_PATTERN],''''),
'+@case1 +',1) = 1 
AND [dbo].[RegExIsMatch](COALESCE(RCD.[LEVEL_2_PATTERN],''''),
'+@case2+',1) = 1 
AND RCD.[RESERVING_CLASS_CODE] <> '''+@bu+'_UNDEF'''


select @query