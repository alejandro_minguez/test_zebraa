﻿CREATE TABLE [SEG].[LDG_REPORT_PARAMS] (
    [TECH_PARAMETER_CLASS] VARCHAR (100) NULL,
    [TECH_PARAMETER_CODE]  VARCHAR (100) NULL,
    [PARAMETER]            VARCHAR (255) NULL,
    [VALUE]                VARCHAR (255) NULL,
    [DATEVALID_FROM]       DATE          NULL,
    [DATEVALID_TO]         DATE          NULL,
    [$sq_execution_id]     BIGINT        NULL,
    [FILE_NAME]            VARCHAR (100) NULL,
    [INSERT_DATE]          DATETIME      NULL,
    [ACTIVE]               TINYINT       NULL
);

