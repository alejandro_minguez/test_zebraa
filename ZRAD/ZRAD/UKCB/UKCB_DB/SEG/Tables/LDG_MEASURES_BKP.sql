﻿CREATE TABLE [SEG].[LDG_MEASURES_BKP] (
    [TECH_PARAMETER_CLASS] VARCHAR (100) NULL,
    [TECH_PARAMETER_CODE]  VARCHAR (100) NULL,
    [DOMAIN]               VARCHAR (255) NULL,
    [MEASURE_NAME]         VARCHAR (255) NULL,
    [MEASURE_DESCRIPTION]  VARCHAR (255) NULL,
    [USE]                  VARCHAR (3)   NULL,
    [DATEVALID_FROM]       DATE          NULL,
    [DATEVALID_TO]         DATE          NULL,
    [$sq_execution_id]     BIGINT        NULL,
    [FILE_NAME]            VARCHAR (100) NULL,
    [INSERT_DATE]          DATETIME      NULL,
    [ACTIVE]               INT           NULL
);

