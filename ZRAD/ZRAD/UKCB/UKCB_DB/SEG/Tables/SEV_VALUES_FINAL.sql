﻿CREATE TABLE [SEG].[SEV_VALUES_FINAL] (
    [RESERVING_CLASS_CODE]       VARCHAR (50)  NULL,
    [SEVERITY_INDEX_ID]          VARCHAR (50)  NULL,
    [SEVERITY_INDEX_NAME]        VARCHAR (255) NULL,
    [CURRENCY]                   VARCHAR (10)  NULL,
    [MIN_BASE_DATE]              INT           NULL,
    [MAX_BASE_DATE]              INT           NULL,
    [FIRST_YEARMONTH]            INT           NULL,
    [LAST_YEARMONTH]             INT           NULL,
    [BACKWARD_MONTHLY_INFLATION] FLOAT (53)    NULL,
    [FORWARD_MONTHLY_INFLATION]  FLOAT (53)    NULL,
    [YEAR_MONTH]                 VARCHAR (10)  NULL,
    [MONTH_VALUE]                FLOAT (53)    NULL,
    [INFLATION_BASE_YEARMONTH]   INT           NULL,
    [INFLATION_BASE_VALUE]       FLOAT (53)    NULL,
    [FINAL_SEVERITY_VALUE]       FLOAT (53)    NULL,
    [DATEVALID_FROM]             DATE          NULL,
    [DATEVALID_TO]               DATE          NULL
);

