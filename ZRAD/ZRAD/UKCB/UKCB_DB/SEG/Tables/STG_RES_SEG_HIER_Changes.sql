﻿CREATE TABLE [SEG].[STG_RES_SEG_HIER$Changes] (
    [RESERVINGCLASS_CODE]   VARCHAR (50)  NULL,
    [RESERVINGCLASS_NAME]   VARCHAR (100) NULL,
    [AGREGRATION_PRIORITY]  TINYINT       NULL,
    [SEGMENT_VARIABLE_NAME] VARCHAR (100) NULL,
    [CUSTOM_VALUE]          VARCHAR (255) NULL,
    [SEGMENT_TYPE]          VARCHAR (255) NULL,
    [AGGREGATE]             VARCHAR (1)   NULL,
    [USE_IC_TRIANGLE]       VARCHAR (1)   NULL,
    [USE_EP_TRIANGLE]       VARCHAR (1)   NULL,
    [DATEVALID_FROM]        DATE          NULL,
    [DATEVALID_TO]          DATE          NULL,
    [$sq_row_hash]          BIGINT        NULL,
    [$sq_execution_id]      BIGINT        NOT NULL,
    [$sq_operation_id]      TINYINT       NULL
);

