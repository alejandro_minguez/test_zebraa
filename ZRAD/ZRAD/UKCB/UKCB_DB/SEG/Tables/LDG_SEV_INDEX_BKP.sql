﻿CREATE TABLE [SEG].[LDG_SEV_INDEX_BKP] (
    [SEVERITY_INDEX_ID]          VARCHAR (50)  NULL,
    [SEVERITY_INDEX_NAME]        VARCHAR (255) NULL,
    [CURRENCY]                   VARCHAR (10)  NULL,
    [FIRST_YEARMONTH]            INT           NULL,
    [LAST_YEARMONTH]             INT           NULL,
    [BACKWARD_MONTHLY_INFLATION] FLOAT (53)    NULL,
    [FORWARD_MONTHLY_INFLATION]  FLOAT (53)    NULL,
    [DATEVALID_FROM]             DATE          NULL,
    [DATEVALID_TO]               DATE          NULL,
    [$sq_execution_id]           BIGINT        NULL,
    [FILE_NAME]                  VARCHAR (100) NULL,
    [INSERT_DATE]                DATETIME      NULL,
    [ACTIVE]                     TINYINT       NULL
);

