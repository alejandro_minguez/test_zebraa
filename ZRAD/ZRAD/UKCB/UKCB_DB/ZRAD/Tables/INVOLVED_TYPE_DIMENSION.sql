﻿CREATE TABLE [ZRAD].[INVOLVED_TYPE_DIMENSION] (
    [INVOLVED_TYPE_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [INVOLVED_TYPE_CODE]              VARCHAR (50)  NOT NULL,
    [INVOLVED_TYPE_NAME]              VARCHAR (100) NOT NULL,
    [INVOLVED_TYPE_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]           DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]              DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]        NUMERIC (12)  NOT NULL,
    CONSTRAINT [INVOLVED_TYPE_DIMENSION_PK] PRIMARY KEY CLUSTERED ([INVOLVED_TYPE_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [INVOLVED_TYPE_DIMENSION_AK] UNIQUE NONCLUSTERED ([INVOLVED_TYPE_CODE] ASC)
);




GO
CREATE NONCLUSTERED INDEX [INVOLVED_TYPE_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[INVOLVED_TYPE_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [INVOLVED_TYPE_DIMENSION_INVOLVED_TYPE_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[INVOLVED_TYPE_DIMENSION]([INVOLVED_TYPE_ANCHOR_IDENTIFIER] ASC);

