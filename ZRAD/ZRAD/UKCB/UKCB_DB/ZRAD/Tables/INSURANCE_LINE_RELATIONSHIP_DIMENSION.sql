﻿CREATE TABLE [ZRAD].[INSURANCE_LINE_RELATIONSHIP_DIMENSION] (
    [INSURANCE_LINE_IDENTIFIER]              NUMERIC (12) NOT NULL,
    [INSURANCE_LINE_RELATIONSHIP_IDENTIFIER] NUMERIC (12) NOT NULL,
    [RECORD_INCLUSION_DATE]                  DATETIME     NOT NULL,
    [RECORD_UPDATE_DATE]                     DATETIME     NOT NULL,
    CONSTRAINT [INSURANCE_LINE_RELATIONSHIP_DIMENSION_PK] PRIMARY KEY CLUSTERED ([INSURANCE_LINE_IDENTIFIER] ASC, [INSURANCE_LINE_RELATIONSHIP_IDENTIFIER] ASC),
    CONSTRAINT [INSURANCE_LINE_RELATIONSHIP_DIMENSION_INSURANCE_LINE_DIMENSION_FK] FOREIGN KEY ([INSURANCE_LINE_IDENTIFIER]) REFERENCES [ZRAD].[INSURANCE_LINE_DIMENSION] ([INSURANCE_LINE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [INSURANCE_LINE_RELATIONSHIP_DIMENSION_INSURANCE_LINE_DIMENSION_FK1] FOREIGN KEY ([INSURANCE_LINE_RELATIONSHIP_IDENTIFIER]) REFERENCES [ZRAD].[INSURANCE_LINE_DIMENSION] ([INSURANCE_LINE_ANCHOR_IDENTIFIER])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [INSURANCE_LINE_RELATIONSHIP_DIMENSION_INSURANCE_LINE_IDENTIFIER_IDX]
    ON [ZRAD].[INSURANCE_LINE_RELATIONSHIP_DIMENSION]([INSURANCE_LINE_IDENTIFIER] ASC, [INSURANCE_LINE_RELATIONSHIP_IDENTIFIER] ASC);

