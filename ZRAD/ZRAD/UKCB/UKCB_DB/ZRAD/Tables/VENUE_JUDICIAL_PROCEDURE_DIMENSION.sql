﻿CREATE TABLE [ZRAD].[VENUE_JUDICIAL_PROCEDURE_DIMENSION] (
    [VENUE_JUDICIAL_PROCEDURE_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [VENUE_JUDICIAL_PROCEDURE_CODE]              VARCHAR (50)  NOT NULL,
    [VENUE_JUDICIAL_PROCEDURE_NAME]              VARCHAR (100) NOT NULL,
    [VENUE_JUDICIAL_PROCEDURE_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]                      DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                         DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]                   NUMERIC (12)  NOT NULL,
    CONSTRAINT [VENUE_JUDICIAL_PROCEDURE_DIMENSION_PK] PRIMARY KEY CLUSTERED ([VENUE_JUDICIAL_PROCEDURE_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [VENUE_JUDICIAL_PROCEDURE_DIMENSION_AK] UNIQUE NONCLUSTERED ([VENUE_JUDICIAL_PROCEDURE_CODE] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [VENUE_JUDICIAL_PROCEDURE_DIMENSION_VENUE_JUDICIAL_PROCEDURE_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[VENUE_JUDICIAL_PROCEDURE_DIMENSION]([VENUE_JUDICIAL_PROCEDURE_ANCHOR_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [VENUE_JUDICIAL_PROCEDURE_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[VENUE_JUDICIAL_PROCEDURE_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);

