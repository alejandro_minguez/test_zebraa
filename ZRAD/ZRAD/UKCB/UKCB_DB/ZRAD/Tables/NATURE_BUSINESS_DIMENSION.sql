﻿CREATE TABLE [ZRAD].[NATURE_BUSINESS_DIMENSION] (
    [NATURE_BUSINESS_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [NATURE_BUSINESS_CODE]              VARCHAR (50)  NOT NULL,
    [NATURE_BUSINESS_NAME]              VARCHAR (100) NOT NULL,
    [NATURE_BUSINESS_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]             DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]          NUMERIC (12)  NOT NULL,
    CONSTRAINT [NATURE_BUSINESS_DIMENSION_PK] PRIMARY KEY CLUSTERED ([NATURE_BUSINESS_ANCHOR_IDENTIFIER] ASC)
);

