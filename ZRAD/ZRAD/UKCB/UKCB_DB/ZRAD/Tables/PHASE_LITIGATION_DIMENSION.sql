﻿CREATE TABLE [ZRAD].[PHASE_LITIGATION_DIMENSION] (
    [PHASE_LITIGATION_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [PHASE_LITIGATION_CODE]              VARCHAR (50)  NOT NULL,
    [PHASE_LITIGATION_NAME]              VARCHAR (100) NOT NULL,
    [PHASE_LITIGATION_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]              DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                 DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]           NUMERIC (12)  NOT NULL,
    CONSTRAINT [PHASE_LITIGATION_DIMENSION_PK] PRIMARY KEY CLUSTERED ([PHASE_LITIGATION_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [PHASE_LITIGATION_DIMENSION_AK] UNIQUE NONCLUSTERED ([PHASE_LITIGATION_CODE] ASC)
);




GO
CREATE NONCLUSTERED INDEX [PHASE_LITIGATION_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[PHASE_LITIGATION_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PHASE_LITIGATION_DIMENSION_PHASE_LITIGATION_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[PHASE_LITIGATION_DIMENSION]([PHASE_LITIGATION_ANCHOR_IDENTIFIER] ASC);

