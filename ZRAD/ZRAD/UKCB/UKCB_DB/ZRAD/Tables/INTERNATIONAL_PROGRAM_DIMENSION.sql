﻿CREATE TABLE [ZRAD].[INTERNATIONAL_PROGRAM_DIMENSION] (
    [INTERNATIONAL_PROGRAM_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [INTERNATIONAL_PROGRAM_CODE]              VARCHAR (50)  NOT NULL,
    [INTERNATIONAL_PROGRAM_NAME]              VARCHAR (100) NOT NULL,
    [INTERNATIONAL_PROGRAM_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]                   DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                      DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]                NUMERIC (12)  NOT NULL,
    CONSTRAINT [INTERNATIONAL_PROGRAM_DIMENSION_PK] PRIMARY KEY NONCLUSTERED ([INTERNATIONAL_PROGRAM_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [INTERNATIONAL_PROGRAM_DIMENSION_AK] UNIQUE NONCLUSTERED ([INTERNATIONAL_PROGRAM_CODE] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [INTERNATIONAL_PROGRAM_DIMENSION_INTERNATIONAL_PROGRAM_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[INTERNATIONAL_PROGRAM_DIMENSION]([INTERNATIONAL_PROGRAM_ANCHOR_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [INTERNATIONAL_PROGRAM_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[INTERNATIONAL_PROGRAM_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);


GO
CREATE UNIQUE CLUSTERED INDEX [INTERNATIONAL_PROGRAM__DIMENSION_PK]
    ON [ZRAD].[INTERNATIONAL_PROGRAM_DIMENSION]([INTERNATIONAL_PROGRAM_ANCHOR_IDENTIFIER] ASC);

