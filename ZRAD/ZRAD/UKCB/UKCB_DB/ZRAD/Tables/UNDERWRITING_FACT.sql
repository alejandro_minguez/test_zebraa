﻿CREATE TABLE [ZRAD].[UNDERWRITING_FACT] (
    [UNDERWRITING_ENDORSEMENT_IDENTIFIER]               NUMERIC (12)    NOT NULL,
    [PERIL_IDENTIFIER]                                  NUMERIC (12)    NOT NULL,
    [INSURANCE_LINE_IDENTIFIER]                         NUMERIC (12)    NOT NULL,
    [WRITTEN_PREMIUM_AMOUNT]                            DECIMAL (15, 2) NOT NULL,
    [EARNED_RATIO_AMOUNT]                               DECIMAL (15, 2) NOT NULL,
    [ASSUMED_WRITTEN_PREMIUM_AMOUNT]                    DECIMAL (15, 2) NOT NULL,
    [APPLIED_DEDUCTIBLE_AMOUNT]                         DECIMAL (15, 2) NOT NULL,
    [POLICY_COST_AMOUNT]                                DECIMAL (15, 2) NOT NULL,
    [FINANCIAL_TRANSACTIONS_TAX_AMOUNT]                 DECIMAL (15, 2) NOT NULL,
    [ADDITIONAL_FRACTION_AMOUNT]                        DECIMAL (15, 2) NOT NULL,
    [NET_WRITTEN_PREMIUM_AMOUNT]                        DECIMAL (15, 2) NOT NULL,
    [UNEARNED_PREMIUM_RESERVE_AMOUNT]                   DECIMAL (15, 2) NOT NULL,
    [EXPOSURE_MEASURE_AMOUNT]                           DECIMAL (15, 2) NOT NULL,
    [EXPOSURE_UNIT_AMOUNT]                              DECIMAL (15, 2) NOT NULL,
    [RECORD_INCLUSION_DATE]                             DATETIME        NOT NULL,
    [RECORD_UPDATE_DATE]                                DATETIME        NOT NULL,
    [COINSURANCE_PREMIUM_PERCENT]                       DECIMAL (7, 4)  NOT NULL,
    [REINSURANCE_PREMIUM_PERCENT]                       DECIMAL (7, 4)  NOT NULL,
    [COINSURANCE_PREMIUM_AMOUNT]                        DECIMAL (15, 2) NOT NULL,
    [REINSURANCE_PREMIUM_AMOUNT]                        DECIMAL (15, 2) NOT NULL,
    [COMMISSION_AMOUNT]                                 DECIMAL (15, 2) NOT NULL,
    [EARNED_COMMISSION_AMOUNT]                          DECIMAL (15, 2) NOT NULL,
    [PRODUCT_INSURED_IDENTIFIER]                        NUMERIC (12)    NOT NULL,
    [COVERAGE_LOCAL_IDENTIFIER]                         NUMERIC (12)    NOT NULL,
    [COVERAGE_GENERAL_INSURANCE_GLOBAL_IDENTIFIER]      NUMERIC (12)    NOT NULL,
    [COVERAGE_LOCAL_STATUTORY_REPORTING_IDENTIFIER]     NUMERIC (12)    NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]                          NUMERIC (12)    NOT NULL,
    [PHYSICAL_OBJECT_IDENTIFIER]                        NUMERIC (12)    NOT NULL,
    [INSURED_IDENTIFIER]                                NUMERIC (12)    NOT NULL,
    [BONUS_CLASS_POLICY_IDENTIFIER]                     NUMERIC (12)    NOT NULL,
    [FUNCTIONAL_CURRENCY_IDENTIFIER]                    NUMERIC (12)    NOT NULL,
    [COVERAGE_STATUS_IDENTIFIER]                        NUMERIC (12)    NOT NULL,
    [MAIN_POLICY_CURRENCY_IDENTIFIER]                   NUMERIC (12)    NOT NULL,
    [INDICATOR_PRIOR_SERVICE_LOCAL_INCIDENT_IDENTIFIER] NUMERIC (3)     NOT NULL,
    [CUSTOMER_IDENTIFIER]                               NUMERIC (12)    NOT NULL,
    [TAX_CATEGORY_VEHICLE_IDENTIFIER]                   NUMERIC (12)    NOT NULL,
    [MAIN_DRIVER_IDENTIFIER]                            NUMERIC (12)    NOT NULL,
    [VEHICLE_LOCATION_MOST_TIME_IDENTIFIER]             NUMERIC (12)    NOT NULL,
    [OVERNIGHT_VEHICLE_LOCATION_IDENTIFIER]             NUMERIC (12)    NOT NULL,
    [PROCESS_IDENTIFIER]                                NUMERIC (12)    NOT NULL,
    [CLAIM_TRIGGER_BASIS_IDENTIFIER]                    NUMERIC (12)    NOT NULL,
    [POLICYHOLDER_DIVIDEND_AMOUNT]                      DECIMAL (15, 2) NOT NULL,
    [NATURE_BUSINESS_IDENTIFIER]                        NUMERIC (12)    NOT NULL,
    [LINE_BUSINESS_IDENTIFIER]                          NUMERIC (12)    NOT NULL,
    [UNDERWRITING_POLICY_ANCHOR_IDENTIFIER]             NUMERIC (12)    NULL,
    CONSTRAINT [UNDERWRITING_FACT_PK] PRIMARY KEY CLUSTERED ([UNDERWRITING_ENDORSEMENT_IDENTIFIER] ASC, [PERIL_IDENTIFIER] ASC, [COVERAGE_LOCAL_IDENTIFIER] ASC, [INSURANCE_LINE_IDENTIFIER] ASC, [SOURCE_SYSTEM_IDENTIFIER] ASC),
    CONSTRAINT [UNDERWRITING_FACT_BONUS_CLASS_DIMENSION_FK] FOREIGN KEY ([BONUS_CLASS_POLICY_IDENTIFIER]) REFERENCES [ZRAD].[BONUS_CLASS_DIMENSION] ([BONUS_CLASS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_CLAIM_TRIGGER_BASIS_DIMENSION_FK] FOREIGN KEY ([CLAIM_TRIGGER_BASIS_IDENTIFIER]) REFERENCES [ZRAD].[CLAIM_TRIGGER_BASIS_DIMENSION] ([CLAIM_TRIGGER_BASIS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK] FOREIGN KEY ([COVERAGE_LOCAL_IDENTIFIER]) REFERENCES [ZRAD].[COVERAGE_DIMENSION] ([COVERAGE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK1] FOREIGN KEY ([COVERAGE_GENERAL_INSURANCE_GLOBAL_IDENTIFIER]) REFERENCES [ZRAD].[COVERAGE_DIMENSION] ([COVERAGE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK2] FOREIGN KEY ([COVERAGE_LOCAL_STATUTORY_REPORTING_IDENTIFIER]) REFERENCES [ZRAD].[COVERAGE_DIMENSION] ([COVERAGE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_COVERAGE_STATUS_DIMENSION_FK] FOREIGN KEY ([COVERAGE_STATUS_IDENTIFIER]) REFERENCES [ZRAD].[COVERAGE_STATUS_DIMENSION] ([COVERAGE_STATUS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_CURRENCY_DIMENSION_FK] FOREIGN KEY ([FUNCTIONAL_CURRENCY_IDENTIFIER]) REFERENCES [ZRAD].[CURRENCY_DIMENSION] ([CURRENCY_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_FLAG_INDICATOR_DIMENSION_FK] FOREIGN KEY ([INDICATOR_PRIOR_SERVICE_LOCAL_INCIDENT_IDENTIFIER]) REFERENCES [ZRAD].[FLAG_INDICATOR_DIMENSION] ([FLAG_INDICATOR_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_INSURANCE_LINE_DIMENSION_FK] FOREIGN KEY ([INSURANCE_LINE_IDENTIFIER]) REFERENCES [ZRAD].[INSURANCE_LINE_DIMENSION] ([INSURANCE_LINE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_LINE_BUSINESS_DIMENSION_FK] FOREIGN KEY ([LINE_BUSINESS_IDENTIFIER]) REFERENCES [ZRAD].[LINE_BUSINESS_DIMENSION] ([LINE_BUSINESS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_NATURE_BUSINESS_DIMENSION_FK] FOREIGN KEY ([NATURE_BUSINESS_IDENTIFIER]) REFERENCES [ZRAD].[NATURE_BUSINESS_DIMENSION] ([NATURE_BUSINESS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK] FOREIGN KEY ([INSURED_IDENTIFIER]) REFERENCES [ZRAD].[PARTY_ROLE_DIMENSION] ([PARTY_ROLE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK1] FOREIGN KEY ([CUSTOMER_IDENTIFIER]) REFERENCES [ZRAD].[PARTY_ROLE_DIMENSION] ([PARTY_ROLE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK2] FOREIGN KEY ([MAIN_DRIVER_IDENTIFIER]) REFERENCES [ZRAD].[PARTY_ROLE_DIMENSION] ([PARTY_ROLE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PERIL_DIMENSION_FK] FOREIGN KEY ([PERIL_IDENTIFIER]) REFERENCES [ZRAD].[PERIL_DIMENSION] ([PERIL_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PHYSICAL_OBJECT_DIMENSION_FK] FOREIGN KEY ([PHYSICAL_OBJECT_IDENTIFIER]) REFERENCES [ZRAD].[PHYSICAL_OBJECT_DIMENSION] ([PHYSICAL_OBJECT_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK] FOREIGN KEY ([VEHICLE_LOCATION_MOST_TIME_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_DIMENSION] ([PLACE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK1] FOREIGN KEY ([OVERNIGHT_VEHICLE_LOCATION_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_DIMENSION] ([PLACE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PROCESS_DIMENSION_FK] FOREIGN KEY ([PROCESS_IDENTIFIER]) REFERENCES [ZRAD].[PROCESS_DIMENSION] ([PROCESS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_PRODUCT_DIMENSION_FK] FOREIGN KEY ([PRODUCT_INSURED_IDENTIFIER]) REFERENCES [ZRAD].[PRODUCT_DIMENSION] ([PRODUCT_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_TAX_CATEGORY_DIMENSION_FK] FOREIGN KEY ([TAX_CATEGORY_VEHICLE_IDENTIFIER]) REFERENCES [ZRAD].[TAX_CATEGORY_DIMENSION] ([TAX_CATEGORY_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_FACT_UNDERWRITING_ENDORSEMENT_DIMENSION_FK] FOREIGN KEY ([UNDERWRITING_ENDORSEMENT_IDENTIFIER]) REFERENCES [ZRAD].[UNDERWRITING_ENDORSEMENT_DIMENSION] ([UNDERWRITING_ENDORSEMENT_ANCHOR_IDENTIFIER])
);


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_BONUS_CLASS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_CLAIM_TRIGGER_BASIS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK2];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_STATUS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_CURRENCY_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_FLAG_INDICATOR_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_INSURANCE_LINE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_LINE_BUSINESS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK2];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PERIL_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PHYSICAL_OBJECT_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PROCESS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PRODUCT_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_SOURCE_SYSTEM_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_TAX_CATEGORY_DIMENSION_FK];




GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_BONUS_CLASS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_CLAIM_TRIGGER_BASIS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_DIMENSION_FK2];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_COVERAGE_STATUS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_CURRENCY_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_FLAG_INDICATOR_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_INSURANCE_LINE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_LINE_BUSINESS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PARTY_ROLE_DIMENSION_FK2];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PERIL_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PHYSICAL_OBJECT_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PLACE_DIMENSION_FK1];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PROCESS_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_PRODUCT_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_SOURCE_SYSTEM_DIMENSION_FK];


GO
ALTER TABLE [ZRAD].[UNDERWRITING_FACT] NOCHECK CONSTRAINT [UNDERWRITING_FACT_TAX_CATEGORY_DIMENSION_FK];


GO






