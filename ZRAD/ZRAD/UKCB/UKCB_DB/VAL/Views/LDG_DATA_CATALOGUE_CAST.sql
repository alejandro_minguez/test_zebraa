﻿





CREATE VIEW  [VAL].[LDG_DATA_CATALOGUE$CAST] AS

with mysource as ( 
	select *,  
		try_cast(LTRIM(RTRIM([CATALOGUE_ATTRIBUTE_NAME])) as varchar(100)) as [CATALOGUE_ATTRIBUTE_NAME_output], 
		try_cast(LTRIM(RTRIM([CATALOGUE_ATTRIBUTE_DESCRIPTION])) as varchar(255)) as [CATALOGUE_ATTRIBUTE_DESCRIPTION_output], 
		try_cast(LTRIM(RTRIM([ZRAD_TABLE_NAME])) as varchar(75)) as [ZRAD_TABLE_NAME_output], 
		try_cast(LTRIM(RTRIM([ZRAD_TABLE_ALIAS_NAME])) as varchar(20)) as [ZRAD_TABLE_ALIAS_NAME_output], 
		try_cast(LTRIM(RTRIM([ZRAD_TABLE_COLUMN_NAME])) as varchar(75)) as [ZRAD_TABLE_COLUMN_NAME_output], 
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_TYPE])) as varchar(20)) as [ZRAD_JOIN_TYPE_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_TABLE])) as varchar(1000)) as [ZRAD_JOIN_TABLE_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_FIELDS])) as varchar(350)) as [ZRAD_JOIN_FIELDS_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_FILTER_CONDITION])) as varchar(1000)) as [ZRAD_JOIN_FILTER_CONDITION_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_ALIAS])) as varchar(20)) as [ZRAD_JOIN_ALIAS_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_ORDER])) as INT) as [ZRAD_JOIN_ORDER_output],
		try_cast(LTRIM(RTRIM([ZRAD_FILTER_CONDITION])) as varchar(1000)) as [ZRAD_FILTER_CONDITION_output],
		try_cast(LTRIM(RTRIM([ZRAD_JOIN_SOURCE])) as varchar(3)) as [ZRAD_JOIN_SOURCE_output]
	from  [SEG].[LDG_DATA_CATALOGUE])


,mySourceWithFlags as ( 
	select *, 
		case when  [CATALOGUE_ATTRIBUTE_NAME_output] is null and [CATALOGUE_ATTRIBUTE_NAME] is not null then 1 else 0 end as [CATALOGUE_ATTRIBUTE_NAME_error], 
		case when  [CATALOGUE_ATTRIBUTE_DESCRIPTION_output] is null and [CATALOGUE_ATTRIBUTE_DESCRIPTION] is not null then 1 else 0 end as [CATALOGUE_ATTRIBUTE_DESCRIPTION_error], 
		case when  [ZRAD_TABLE_NAME_output] is null and [ZRAD_TABLE_NAME] is not null then 1 else 0 end as [ZRAD_TABLE_NAME_error], 
		case when  [ZRAD_TABLE_ALIAS_NAME_output] is null and [ZRAD_TABLE_ALIAS_NAME] is not null then 1 else 0 end as [ZRAD_TABLE_ALIAS_NAME_error], 
		case when  [ZRAD_TABLE_COLUMN_NAME_output] is null and [ZRAD_TABLE_COLUMN_NAME] is not null then 1 else 0 end as [ZRAD_TABLE_COLUMN_NAME_error], 
		case when  [ZRAD_JOIN_TYPE_output] is null and [ZRAD_JOIN_TYPE] is not null then 1 else 0 end as [ZRAD_JOIN_TYPE_error],
		case when  [ZRAD_JOIN_TABLE_output] is null and [ZRAD_JOIN_TABLE] is not null then 1 else 0 end as [ZRAD_JOIN_TABLE_error],
		case when  [ZRAD_JOIN_FIELDS_output] is null and [ZRAD_JOIN_FIELDS] is not null then 1 else 0 end as [ZRAD_JOIN_FIELDS_error],
		case when  [ZRAD_JOIN_FILTER_CONDITION_output] is null and [ZRAD_JOIN_FILTER_CONDITION] is not null then 1 else 0 end as [ZRAD_JOIN_FILTER_CONDITION_error],
		case when  [ZRAD_JOIN_ALIAS_output] is null and [ZRAD_JOIN_ALIAS] is not null then 1 else 0 end as [ZRAD_JOIN_ALIAS_error],
		case when  [ZRAD_JOIN_ORDER_output] is null and [ZRAD_JOIN_ORDER] is not null then 1 else 0 end as [ZRAD_JOIN_ORDER_error],
		case when  [ZRAD_FILTER_CONDITION_output] is null and [ZRAD_FILTER_CONDITION] is not null then 1 else 0 end as [ZRAD_FILTER_CONDITION_error],
		case when  [ZRAD_JOIN_SOURCE_output] is null and [ZRAD_JOIN_SOURCE] is not null then 1 else 0 end as [ZRAD_JOIN_SOURCE_error] 
	from mysource) 

select * , [CATALOGUE_ATTRIBUTE_NAME_error]+ [CATALOGUE_ATTRIBUTE_DESCRIPTION_error]+ [ZRAD_TABLE_NAME_error]+ [ZRAD_TABLE_ALIAS_NAME_error]+ [ZRAD_TABLE_COLUMN_NAME_error]+ [ZRAD_JOIN_TYPE_error] + [ZRAD_JOIN_FILTER_CONDITION_error]+ [ZRAD_JOIN_FIELDS_error] + [ZRAD_JOIN_TABLE_error]+  [ZRAD_JOIN_ALIAS_error] + +[ZRAD_JOIN_ORDER_error] + [ZRAD_FILTER_CONDITION_error] + [ZRAD_JOIN_SOURCE_error] as [$sq_transformation_error_count]  from mySourceWithFlags;