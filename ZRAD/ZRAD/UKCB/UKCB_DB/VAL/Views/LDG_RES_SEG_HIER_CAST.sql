﻿




CREATE VIEW  [VAL].[LDG_RES_SEG_HIER$CAST] AS

 with mysource as ( 
	select *,  
		try_cast([RESERVINGCLASS_CODE] as varchar(50)) as [RESERVINGCLASS_CODE_output], 
		try_cast([RESERVINGCLASS_NAME] as varchar(100)) as [RESERVINGCLASS_NAME_output], 
		try_cast([AGREGRATION_PRIORITY] as tinyint) as [AGREGRATION_PRIORITY_output], 
		try_cast([SEGMENT_VARIABLE_NAME] as varchar(100)) as [SEGMENT_VARIABLE_NAME_output], 
		try_cast([CUSTOM_VALUE] as varchar(255)) as [CUSTOM_VALUE_output], 
		try_cast(SEGMENT_TYPE as varchar(255)) as [SEGMENT_TYPE_output], 		
		try_cast([AGGREGATE] as varchar(3)) as [AGGREGATE_output], 
		try_cast([USE_IC_TRIANGLE] as varchar(3)) as [USE_IC_TRIANGLE_output], 
		try_cast([USE_EP_TRIANGLE] as varchar(3)) as [USE_EP_TRIANGLE_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
	from  [SEG].[LDG_RES_SEG_HIER]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos  

,mySourceWithFlags as ( 
	select *, 
		case when  [RESERVINGCLASS_CODE_output] is null and [RESERVINGCLASS_CODE] is not null then 1 else 0 end as [RESERVINGCLASS_CODE_error], 
		case when  [RESERVINGCLASS_NAME_output] is null and [RESERVINGCLASS_NAME] is not null then 1 else 0 end as [RESERVINGCLASS_NAME_error], 
		case when  [AGREGRATION_PRIORITY_output] is null and [AGREGRATION_PRIORITY] is not null then 1 else 0 end as [AGREGRATION_PRIORITY_error], 
		case when  [SEGMENT_VARIABLE_NAME_output] is null and [SEGMENT_VARIABLE_NAME] is not null then 1 else 0 end as [SEGMENT_VARIABLE_NAME_error], 
		case when  [CUSTOM_VALUE_output] is null and [CUSTOM_VALUE] is not null then 1 else 0 end as [CUSTOM_VALUE_error], 
		case when  [SEGMENT_TYPE_output] is null and SEGMENT_TYPE is not null then 1 else 0 end as [SEGMENT_TYPE_error], 
		case when  [AGGREGATE_output] is null and [AGGREGATE] is not null then 1 else 0 end as [AGGREGATE_error], 
		case when  [USE_IC_TRIANGLE_output] is null and [USE_IC_TRIANGLE] is not null then 1 else 0 end as [USE_IC_TRIANGLE_error], 
		case when  [USE_EP_TRIANGLE_output] is null and [USE_EP_TRIANGLE] is not null then 1 else 0 end as [USE_EP_TRIANGLE_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
	from mysource) 

select * , [RESERVINGCLASS_CODE_error]+ [RESERVINGCLASS_NAME_error]+ [AGREGRATION_PRIORITY_error]+ [SEGMENT_VARIABLE_NAME_error]+ [CUSTOM_VALUE_error]+ SEGMENT_TYPE_ERROR+[AGGREGATE_error]+ [USE_IC_TRIANGLE_error]+ [USE_EP_TRIANGLE_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;