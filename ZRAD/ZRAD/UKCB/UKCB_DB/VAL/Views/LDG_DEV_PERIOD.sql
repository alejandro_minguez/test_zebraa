﻿


CREATE VIEW  [VAL].[LDG_DEV_PERIOD] AS

SELECT TECH_PARAMETER_CLASS_output AS TECH_PARAMETER_CLASS,
TECH_PARAMETER_CODE_output AS TECH_PARAMETER_CODE,
[DOMAIN_output] AS [DOMAIN]
,[FULL_DESCRIPTION_output] AS [FULL_DESCRIPTION]
,[USE_output] AS [USE]
,COALESCE([DATEVALID_FROM_output],cast('19000101' as date)) AS [DATEVALID_FROM]
,COALESCE([DATEVALID_TO_output],cast('29991231'as date)) AS [DATEVALID_TO]
FROM [VAL].[LDG_DEV_PERIOD$CAST]
WHERE [$sq_transformation_error_count] = 0