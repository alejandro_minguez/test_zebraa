﻿



CREATE VIEW  [VAL].[LDG_RES_SEG_HIER] AS

SELECT 
       [RESERVINGCLASS_CODE_output] AS [RESERVINGCLASS_CODE]
      ,[RESERVINGCLASS_NAME_output] AS [RESERVINGCLASS_NAME]
      ,[AGREGRATION_PRIORITY_output] AS [AGREGRATION_PRIORITY]
      ,[SEGMENT_VARIABLE_NAME_output] AS [SEGMENT_VARIABLE_NAME]
      ,[CUSTOM_VALUE_output] AS [CUSTOM_VALUE]
	  ,SEGMENT_TYPE_output AS SEGMENT_TYPE
      ,IIF([AGGREGATE_output] = 'No', '0', '1') AS [AGGREGATE]
      ,IIF([USE_IC_TRIANGLE_output] = 'Yes', '1', '0') AS [USE_IC_TRIANGLE]
      ,IIF([USE_EP_TRIANGLE_output] = 'Yes', '1', '0') AS [USE_EP_TRIANGLE]
      ,COALESCE([DATEVALID_FROM_output],cast('19000101' as date)) AS [DATEVALID_FROM]
      ,COALESCE([DATEVALID_TO_output],cast('29991231'as date)) AS [DATEVALID_TO]
  FROM [VAL].[LDG_RES_SEG_HIER$CAST]
  WHERE [$sq_transformation_error_count] = 0