﻿

CREATE VIEW  [VAL].[LDG_SEV_INDEX$CAST] AS

with mysource as ( 
	select *,  
		try_cast([SEVERITY_INDEX_ID] as varchar(50)) as [SEVERITY_INDEX_ID_output], 
		try_cast([SEVERITY_INDEX_NAME] as varchar(255)) as [SEVERITY_INDEX_NAME_output], 
		try_cast([CURRENCY] as varchar(10)) as [CURRENCY_output], 
		try_cast([FIRST_YEARMONTH] as int) as [FIRST_YEARMONTH_output], 
		try_cast([LAST_YEARMONTH] as int) as [LAST_YEARMONTH_output], 
		try_cast([BACKWARD_MONTHLY_INFLATION] as float) as [BACKWARD_MONTHLY_INFLATION_output], 
		try_cast([FORWARD_MONTHLY_INFLATION] as float) as [FORWARD_MONTHLY_INFLATION_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
	from  [SEG].[LDG_SEV_INDEX]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos 

,mySourceWithFlags as ( 
	select *, 
		case when  [SEVERITY_INDEX_ID_output] is null and [SEVERITY_INDEX_ID] is not null then 1 else 0 end as [SEVERITY_INDEX_ID_error], 
		case when  [SEVERITY_INDEX_NAME_output] is null and [SEVERITY_INDEX_NAME] is not null then 1 else 0 end as [SEVERITY_INDEX_NAME_error], 
		case when  [CURRENCY_output] is null and [CURRENCY] is not null then 1 else 0 end as [CURRENCY_error], 
		case when  [FIRST_YEARMONTH_output] is null and [FIRST_YEARMONTH] is not null then 1 else 0 end as [FIRST_YEARMONTH_error], 
		case when  [LAST_YEARMONTH_output] is null and [LAST_YEARMONTH] is not null then 1 else 0 end as [LAST_YEARMONTH_error], 
		case when  [BACKWARD_MONTHLY_INFLATION_output] is null and [BACKWARD_MONTHLY_INFLATION] is not null then 1 else 0 end as [BACKWARD_MONTHLY_INFLATION_error], 
		case when  [FORWARD_MONTHLY_INFLATION_output] is null and [FORWARD_MONTHLY_INFLATION] is not null then 1 else 0 end as [FORWARD_MONTHLY_INFLATION_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
	from mysource) 

select * , [SEVERITY_INDEX_ID_error]+ [SEVERITY_INDEX_NAME_error]+ [CURRENCY_error]+ [FIRST_YEARMONTH_error]+ [LAST_YEARMONTH_error]+ [BACKWARD_MONTHLY_INFLATION_error]+ [FORWARD_MONTHLY_INFLATION_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;