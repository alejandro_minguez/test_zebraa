﻿


CREATE VIEW  [VAL].[LDG_SEV_INDEX] AS

 SELECT [SEVERITY_INDEX_ID_output] AS [SEVERITY_INDEX_ID]
      ,[SEVERITY_INDEX_NAME_output] AS [SEVERITY_INDEX_NAME]
      ,[CURRENCY_output] AS [CURRENCY]
      ,[FIRST_YEARMONTH_output] AS [FIRST_YEARMONTH]
      ,[LAST_YEARMONTH_output] AS [LAST_YEARMONTH]
      ,[BACKWARD_MONTHLY_INFLATION_output] AS [BACKWARD_MONTHLY_INFLATION]
      ,[FORWARD_MONTHLY_INFLATION_output] AS [FORWARD_MONTHLY_INFLATION]
      ,COALESCE([DATEVALID_FROM_output],cast('19000101' as date)) AS [DATEVALID_FROM]
      ,COALESCE([DATEVALID_TO_output],cast('29991231'as date)) AS [DATEVALID_TO]
  FROM [VAL].[LDG_SEV_INDEX$CAST]
  WHERE [$sq_transformation_error_count] = 0