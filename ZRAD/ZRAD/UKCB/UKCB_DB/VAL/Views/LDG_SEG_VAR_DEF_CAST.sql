﻿




CREATE VIEW  [VAL].[LDG_SEG_VAR_DEF$CAST] AS
 
 with mysource as ( 
	select *,  
		try_cast([SEGMENT_VARIABLE_NAME] as varchar(100)) as [SEGMENT_VARIABLE_NAME_output], 
		try_cast([SEGMENT_VARIABLE_CUSTOM_VALUE] as varchar(150)) as [SEGMENT_VARIABLE_CUSTOM_VALUE_output], 
		try_cast([LEVEL_1_CATALOG_NAME] as varchar(100)) as [LEVEL_1_CATALOG_NAME_output], 
		try_cast([LEVEL_1_FILTER] as varchar(20)) as [LEVEL_1_FILTER_output], 
		try_cast([LEVEL_1_VALUES] as varchar(150)) as [LEVEL_1_VALUES_output], 
		try_cast([LEVEL_2_CATALOG_NAME] as varchar(100)) as [LEVEL_2_CATALOG_NAME_output], 
		try_cast([LEVEL_2_FILTER] as varchar(20)) as [LEVEL_2_FILTER_output], 
		try_cast([LEVEL_2_VALUES] as varchar(150)) as [LEVEL_2_VALUES_output], 
		try_cast([LEVEL_3_CATALOG_NAME] as varchar(100)) as [LEVEL_3_CATALOG_NAME_output], 
		try_cast([LEVEL_3_FILTER] as varchar(20)) as [LEVEL_3_FILTER_output], 
		try_cast([LEVEL_3_VALUES] as varchar(150)) as [LEVEL_3_VALUES_output], 
		try_cast([LEVEL_4_CATALOG_NAME] as varchar(100)) as [LEVEL_4_CATALOG_NAME_output], 
		try_cast([LEVEL_4_FILTER] as varchar(20)) as [LEVEL_4_FILTER_output], 
		try_cast([LEVEL_4_VALUES] as varchar(150)) as [LEVEL_4_VALUES_output], 
		try_cast([LEVEL_5_CATALOG_NAME] as varchar(100)) as [LEVEL_5_CATALOG_NAME_output], 
		try_cast([LEVEL_5_FILTER] as varchar(20)) as [LEVEL_5_FILTER_output], 
		try_cast([LEVEL_5_VALUES] as varchar(150)) as [LEVEL_5_VALUES_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
	from  [SEG].[LDG_SEG_VAR_DEF]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos  

,mySourceWithFlags as ( 
	select *, 
		case when  [SEGMENT_VARIABLE_NAME_output] is null and [SEGMENT_VARIABLE_NAME] is not null then 1 else 0 end as [SEGMENT_VARIABLE_NAME_error], 
		case when  [SEGMENT_VARIABLE_CUSTOM_VALUE_output] is null and [SEGMENT_VARIABLE_CUSTOM_VALUE] is not null then 1 else 0 end as [SEGMENT_VARIABLE_CUSTOM_VALUE_error], 
		case when  [LEVEL_1_CATALOG_NAME_output] is null and [LEVEL_1_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_1_CATALOG_NAME_error], 
		case when  [LEVEL_1_FILTER_output] is null and [LEVEL_1_FILTER] is not null then 1 else 0 end as [LEVEL_1_FILTER_error], 
		case when  [LEVEL_1_VALUES_output] is null and [LEVEL_1_VALUES] is not null then 1 else 0 end as [LEVEL_1_VALUES_error],
		case when  [LEVEL_2_CATALOG_NAME_output] is null and [LEVEL_2_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_2_CATALOG_NAME_error], 
		case when  [LEVEL_2_FILTER_output] is null and [LEVEL_2_FILTER] is not null then 1 else 0 end as [LEVEL_2_FILTER_error], 
		case when  [LEVEL_2_VALUES_output] is null and [LEVEL_2_VALUES] is not null then 1 else 0 end as [LEVEL_2_VALUES_error], 
		case when  [LEVEL_3_CATALOG_NAME_output] is null and [LEVEL_3_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_3_CATALOG_NAME_error], 
		case when  [LEVEL_3_FILTER_output] is null and [LEVEL_3_FILTER] is not null then 1 else 0 end as [LEVEL_3_FILTER_error], 
		case when  [LEVEL_3_VALUES_output] is null and [LEVEL_3_VALUES] is not null then 1 else 0 end as [LEVEL_3_VALUES_error], 
		case when  [LEVEL_4_CATALOG_NAME_output] is null and [LEVEL_4_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_4_CATALOG_NAME_error], 
		case when  [LEVEL_4_FILTER_output] is null and [LEVEL_4_FILTER] is not null then 1 else 0 end as [LEVEL_4_FILTER_error], 
		case when  [LEVEL_4_VALUES_output] is null and [LEVEL_4_VALUES] is not null then 1 else 0 end as [LEVEL_4_VALUES_error], 
		case when  [LEVEL_5_CATALOG_NAME_output] is null and [LEVEL_5_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_5_CATALOG_NAME_error], 
		case when  [LEVEL_5_FILTER_output] is null and [LEVEL_5_FILTER] is not null then 1 else 0 end as [LEVEL_5_FILTER_error], 
		case when  [LEVEL_5_VALUES_output] is null and [LEVEL_5_VALUES] is not null then 1 else 0 end as [LEVEL_5_VALUES_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
	from mysource) 

select * , [SEGMENT_VARIABLE_NAME_error]+ [SEGMENT_VARIABLE_CUSTOM_VALUE_error]+ [LEVEL_1_CATALOG_NAME_error]+ [LEVEL_1_FILTER_error]+ [LEVEL_1_VALUES_error]+ [LEVEL_2_CATALOG_NAME_error]+ [LEVEL_2_FILTER_error]+ [LEVEL_2_VALUES_error]+ [LEVEL_3_CATALOG_NAME_error]+ [LEVEL_3_FILTER_error]+ [LEVEL_3_VALUES_error]+ [LEVEL_4_CATALOG_NAME_error]+ [LEVEL_4_FILTER_error]+ [LEVEL_4_VALUES_error]+ [LEVEL_5_CATALOG_NAME_error]+ [LEVEL_5_FILTER_error]+ [LEVEL_5_VALUES_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;