﻿















CREATE VIEW  [VAL].[LDG_RES_CLASS$CAST] AS

 with mysource as ( 
	select *,  
		try_cast([RESERVING_CLASS_NAME] as varchar(100)) as [RESERVING_CLASS_NAME_output], 
		try_cast([RESERVING_CLASS_CODE] as varchar(50)) as [RESERVING_CLASS_CODE_output], 
		try_cast([MIN_BASE_DATE_FILTER] as date) as [MIN_BASE_DATE_FILTER_output], 
		try_cast([LARGE_LOSS_SPLIT] as varchar(3)) as [LARGE_LOSS_SPLIT_output], 
		try_cast([LARGE_LOSS_METHOD] as varchar(50)) as [LARGE_LOSS_METHOD_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output],
		try_cast([COUNTRY] as varchar(10)) as [COUNTRY_output], 
		try_cast([BUSINESS_UNIT] as varchar(10)) as [BUSINESS_UNIT_output] 
	from  [SEG].[LDG_RES_CLASS]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos 

,mySourceWithFlags as ( 
	select *, 
		case when  [RESERVING_CLASS_NAME_output] is null and [RESERVING_CLASS_NAME] is not null then 1 else 0 end as [RESERVING_CLASS_NAME_error], 
		case when  [RESERVING_CLASS_CODE_output] is null and [RESERVING_CLASS_CODE] is not null then 1 else 0 end as [RESERVING_CLASS_CODE_error], 
		case when  [MIN_BASE_DATE_FILTER_output] is null and [MIN_BASE_DATE_FILTER] is not null then 1 else 0 end as [MIN_BASE_DATE_FILTER_error], 
		case when  [LARGE_LOSS_SPLIT_output] is null and [LARGE_LOSS_SPLIT] is not null then 1 else 0 end as [LARGE_LOSS_SPLIT_error], 
		case when  [LARGE_LOSS_METHOD_output] is null and [LARGE_LOSS_METHOD] is not null then 1 else 0 end as [LARGE_LOSS_METHOD_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [COUNTRY_output] is null and [COUNTRY] is not null then 1 else 0 end as [COUNTRY_error], 
		case when  [BUSINESS_UNIT_output] is null and [BUSINESS_UNIT] is not null then 1 else 0 end as [BUSINESS_UNIT_error] 
	from mysource) 

select * , [RESERVING_CLASS_NAME_error]+ [RESERVING_CLASS_CODE_error]+ [MIN_BASE_DATE_FILTER_error]+ [LARGE_LOSS_SPLIT_error]+ [LARGE_LOSS_METHOD_error]+ [DATEVALID_TO_error]+ [DATEVALID_FROM_error]+ [COUNTRY_error]+ [BUSINESS_UNIT_error] as [$sq_transformation_error_count]  from mySourceWithFlags;