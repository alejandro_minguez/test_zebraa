﻿






CREATE VIEW [VAL].[UKCB_ENGINEERING] AS

SELECT [ID]
	  ,IIF([BUSTYPE_output] IS NULL OR RTRIM(LTRIM([BUSTYPE_output]))='','-3',UPPER(RTRIM(LTRIM([BUSTYPE_output])))) AS BUSTYPE
      ,IIF([ACCDAT_output] IS NULL,CAST('19000101' AS DATETIME),[ACCDAT_output]) AS ACCDAT
	  ,IIF([SUBBCH_output] IS NULL OR RTRIM(LTRIM([SUBBCH_output]))='','-3',UPPER(RTRIM(LTRIM([SUBBCH_output])))) AS SUBBCH
      ,IIF([NOTDAT_output] IS NULL,CAST('19000101' AS DATETIME),[NOTDAT_output]) AS NOTDAT
	  ,IIF([IRDIND_output] IS NULL OR RTRIM(LTRIM([IRDIND_output]))='','-3',UPPER(RTRIM(LTRIM([IRDIND_output])))) AS IRDIND
	  ,IIF([XLIND_output] IS NULL OR RTRIM(LTRIM([XLIND_output]))='','-3',UPPER(RTRIM(LTRIM([XLIND_output])))) AS XLIND
	  ,IIF([NOI_output] IS NULL OR RTRIM(LTRIM([NOI_output]))='','-3',UPPER(RTRIM(LTRIM([NOI_output])))) AS NOI
	  ,IIF([CLOSS_output] IS NULL OR RTRIM(LTRIM([CLOSS_output]))='','-3',UPPER(RTRIM(LTRIM([CLOSS_output])))) AS CLOSS
	  ,IIF([BCH_output] IS NULL OR RTRIM(LTRIM([BCH_output]))='','-3',UPPER(RTRIM(LTRIM([BCH_output])))) AS BCH
      ,IIF([STATCHG_output] IS NULL,-3,[STATCHG_output]) AS STATCHG -- Se va a usar como lookup
      ,IIF([NTRANS_output] IS NULL,0,[NTRANS_output])  AS NTRANS
      ,IIF([TRANDAT_output] IS NULL,CAST('19000101' AS DATETIME),[TRANDAT_output]) AS TRANDAT
	  ,IIF([MTIND_output] IS NULL OR RTRIM(LTRIM([MTIND_output]))='','-3',UPPER(RTRIM(LTRIM([MTIND_output])))) AS MTIND
      ,IIF([LEGENT_output] IS NULL,0,[LEGENT_output]) AS LEGENT
      ,IIF([NOP_output] IS NULL,-3,[NOP_output]) AS NOP -- Se va a usar como lookup
      ,IIF([PAID_output] IS NULL,0,[PAID_output]) AS PAID
      ,IIF([OSCHG_output] IS NULL,0,[OSCHG_output]) AS OSCHG
      ,IIF([COB_output] IS NULL,-3,[COB_output]) AS COB -- Se va a usar como lookup
      ,IIF([UWDAT_output] IS NULL,CAST('19000101' AS DATETIME),[UWDAT_output]) AS UWDAT
	  ,IIF([COY_output] IS NULL OR RTRIM(LTRIM([COY_output]))='','-3',UPPER(RTRIM(LTRIM([COY_output])))) AS COY
      ,IIF([SETTDAT_output] IS NULL,CAST('19000101' AS DATETIME),[SETTDAT_output]) AS SETTDAT
	  ,IIF([CATEGORY_output] IS NULL OR RTRIM(LTRIM([CATEGORY_output]))='','-3',UPPER(RTRIM(LTRIM([CATEGORY_output])))) AS CATEGORY
	  ,IIF([CATEGRY2_output] IS NULL OR RTRIM(LTRIM([CATEGRY2_output]))='','-3',UPPER(RTRIM(LTRIM([CATEGRY2_output])))) AS CATEGRY2
      ,IIF([INCRDPAY_output] IS NULL,0,[INCRDPAY_output])  AS INCRDPAY
	  ,IIF([LARGEIND_output] IS NULL OR RTRIM(LTRIM([LARGEIND_output]))='','-3',UPPER(RTRIM(LTRIM([LARGEIND_output])))) AS LARGEIND
      ,IIF([TRPRD_output] IS NULL,0,[TRPRD_output]) AS TRPRD
	  ,IIF([BTYPE_output] IS NULL OR RTRIM(LTRIM([BTYPE_output]))='','-3',UPPER(RTRIM(LTRIM([BTYPE_output])))) AS BTYPE
	  ,IIF([CLMID_output] IS NULL OR RTRIM(LTRIM([CLMID_output]))='','-3',UPPER(RTRIM(LTRIM([CLMID_output])))) AS CLMID
	  ,IIF([ZCZL_output] IS NULL OR RTRIM(LTRIM([ZCZL_output]))='','-3',UPPER(RTRIM(LTRIM([ZCZL_output])))) AS ZCZL
	  ,IIF([SUBCAT_output] IS NULL OR RTRIM(LTRIM([SUBCAT_output]))='','-3',UPPER(RTRIM(LTRIM([SUBCAT_output])))) AS SUBCAT
	  ,IIF([ELGRP_output] IS NULL OR RTRIM(LTRIM([ELGRP_output]))='','-3',UPPER(RTRIM(LTRIM([ELGRP_output])))) AS ELGRP
      ,IIF([MAXINCD_output] IS NULL,0,[MAXINCD_output]) AS MAXINCD
	  ,IIF([LATESTOS_output] IS NULL,0,[LATESTOS_output]) AS LATESTOS
      ,[I90IND_output] AS I90IND
      ,[PUMAIND_output] AS PUMAIND
	  ,IIF([LSIND_output] IS NULL OR RTRIM(LTRIM([LSIND_output]))='','-3',UPPER(RTRIM(LTRIM([LSIND_output])))) AS LSIND
	  ,IIF([CLMID_output] IS NULL OR RTRIM(LTRIM([CLMID_output]))='','-3',
			IIF([NOTDAT_output] IS NULL,'-3',
				IIF( ('070113' <= SUBSTRING([CLMID_output],20,6)) AND (SUBSTRING([CLMID_output],20,6) <= '114826') AND YEAR([NOTDAT_output]) = 2016, 'ZEUS', 'COGE'))) AS [SYSTEM]
  FROM [VAL].[UKCB_ENGINEERING$CAST]