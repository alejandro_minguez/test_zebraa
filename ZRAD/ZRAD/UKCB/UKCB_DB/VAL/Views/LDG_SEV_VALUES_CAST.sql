﻿



CREATE VIEW  [VAL].[LDG_SEV_VALUES$CAST] AS
 
 with mysource as ( 
	select *,  
		try_cast([SEVERITY_INDEX_ID] as varchar(50)) as [SEVERITY_INDEX_ID_output], 
		try_cast([SEVERITY_INDEX_NAME] as varchar(255)) as [SEVERITY_INDEX_NAME_output], 
		try_cast([SEVERITY_YEARMONTH] as int) as [SEVERITY_YEARMONTH_output], 
		try_cast([SEVERITY_VALUE] as float) as [SEVERITY_VALUE_output]
	from  [SEG].[LDG_SEV_VALUES]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos    

,mySourceWithFlags as ( 
	select *, 
		case when  [SEVERITY_INDEX_ID_output] is null and [SEVERITY_INDEX_ID] is not null then 1 else 0 end as [SEVERITY_INDEX_ID_error], 
		case when  [SEVERITY_INDEX_NAME_output] is null and [SEVERITY_INDEX_NAME] is not null then 1 else 0 end as [SEVERITY_INDEX_NAME_error], 
		case when  [SEVERITY_YEARMONTH_output] is null and [SEVERITY_YEARMONTH] is not null then 1 else 0 end as [SEVERITY_YEARMONTH_error], 
		case when  [SEVERITY_VALUE_output] is null and [SEVERITY_VALUE] is not null then 1 else 0 end as [SEVERITY_VALUE_error] 
	from mysource) 
	
select * , [SEVERITY_INDEX_ID_error]+ [SEVERITY_INDEX_NAME_error]+ [SEVERITY_YEARMONTH_error]+ [SEVERITY_VALUE_error] as [$sq_transformation_error_count]  from mySourceWithFlags;