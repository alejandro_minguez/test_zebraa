﻿CREATE TABLE [ZRAD].[INSURANCE_SEGMENT_DIMENSION] (
    [INSURANCE_SEGMENT_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [INSURANCE_SEGMENT_CODE]              VARCHAR (50)  NOT NULL,
    [INSURANCE_SEGMENT_NAME]              VARCHAR (100) NOT NULL,
    [INSURANCE_SEGMENT_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]               DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                  DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]            NUMERIC (12)  NOT NULL,
    CONSTRAINT [INSURANCE_SEGMENT_DIMENSION_PK] PRIMARY KEY CLUSTERED ([INSURANCE_SEGMENT_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [INSURANCE_SEGMENT_DIMENSION_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [INSURANCE_SEGMENT_DIMENSION_AK] UNIQUE NONCLUSTERED ([INSURANCE_SEGMENT_CODE] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [INSURANCE_SEGMENT_DIMENSION_INSURANCE_SEGMENT_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[INSURANCE_SEGMENT_DIMENSION]([INSURANCE_SEGMENT_ANCHOR_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [INSURANCE_SEGMENT_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[INSURANCE_SEGMENT_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);

