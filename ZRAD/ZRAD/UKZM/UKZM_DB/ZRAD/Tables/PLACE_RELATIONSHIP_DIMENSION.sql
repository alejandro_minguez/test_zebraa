﻿CREATE TABLE [ZRAD].[PLACE_RELATIONSHIP_DIMENSION] (
    [PLACE_IDENTIFIER]                   NUMERIC (12) NOT NULL,
    [PLACE_RELATIONSHIP_IDENTIFIER]      NUMERIC (12) NOT NULL,
    [PLACE_RELATIONSHIP_TYPE_IDENTIFIER] NUMERIC (12) NOT NULL,
    CONSTRAINT [PLACE_RELATIONSHIP_DIMENSION_PK] PRIMARY KEY CLUSTERED ([PLACE_IDENTIFIER] ASC, [PLACE_RELATIONSHIP_IDENTIFIER] ASC),
    CONSTRAINT [PLACE_RELATIONSHIP_DIMENSION_PLACE_DIMENSION_FK] FOREIGN KEY ([PLACE_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_DIMENSION] ([PLACE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PLACE_RELATIONSHIP_DIMENSION_PLACE_DIMENSION_FK1] FOREIGN KEY ([PLACE_RELATIONSHIP_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_DIMENSION] ([PLACE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PLACE_RELATIONSHIP_DIMENSION_PLACE_RELATIONSHIP_TYPE_DIMENSION_FK] FOREIGN KEY ([PLACE_RELATIONSHIP_TYPE_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_RELATIONSHIP_TYPE_DIMENSION] ([PLACE_RELATIONSHIP_TYPE_ANCHOR_IDENTIFIER])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PLACE_RELATIONSHIP_DIMENSION_PLACE_IDENTIFIER_IDX]
    ON [ZRAD].[PLACE_RELATIONSHIP_DIMENSION]([PLACE_IDENTIFIER] ASC, [PLACE_RELATIONSHIP_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PLACE_RELATIONSHIP_DIMENSION_PLACE_RELATIONSHIP_TYPE_DIMENSION_IDX0]
    ON [ZRAD].[PLACE_RELATIONSHIP_DIMENSION]([PLACE_RELATIONSHIP_TYPE_IDENTIFIER] ASC);

