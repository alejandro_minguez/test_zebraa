﻿






CREATE VIEW [SEG].[VW_RESERVING_SEGMENT] 
AS


--select [ReservingClassCode]--,[LOCAL_LOB_GROUP]
--	  ,year (payment_date ) pay_year
--	  ,count(distinct [CLAIM_ID] ) claims
--      ,sum(cast( [NET_AMOUNT] as float)) net
--from (



SELECT pay.[ReservingClassCode]
      ,pay.[ReservingClassName]
      --,[LOCAL_LOB_GROUP]
      --,[LOCAL_LOB_BASE]
	  --,RSD_1.L1_Attrib
      ,coalesce(RSD_1.[ReservingSegmValue],RSD_1.[ReservingSegmCustomValue], 
				case  RSD_1.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_1_Value]
      ,coalesce(RSD_2.[ReservingSegmValue],RSD_2.[ReservingSegmCustomValue], 
				case  RSD_2.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_2_Value]
      ,coalesce(RSD_3.[ReservingSegmValue],RSD_3.[ReservingSegmCustomValue], 
				case  RSD_3.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_3_Value]
      ,coalesce(RSD_4.[ReservingSegmValue],RSD_4.[ReservingSegmCustomValue], 
				case  RSD_4.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_4_Value]
      ,coalesce(RSD_5.[ReservingSegmValue],RSD_5.[ReservingSegmCustomValue], 
				case  RSD_5.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_5_Value]
      ,coalesce(RSD_6.[ReservingSegmValue],RSD_6.[ReservingSegmCustomValue], 
				case  RSD_6.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_6_Value]
      ,coalesce(RSD_7.[ReservingSegmValue],RSD_7.[ReservingSegmCustomValue], 
				case  RSD_7.L1_Attrib	when 'Group LOB' then [LOCAL_LOB_GROUP] 
										when 'Line of Business code - Local'	then [LOCAL_LOB_BASE] 
										when 'Source System' then 'ZM' end )							as [ReservingSegm_7_Value]
	  ,[TRANSACTION_TYPE_NAME]
	  ,[TRANSACTION_TYPE_IDENTIFIER]
      ,[CLAIM_ID]
	  ,[CLAIM_ANCHOR_IDENTIFIER]
	  --,net_amount
  FROM 
	  ( 
		  SELECT distinct 
			   [ReservingClassCode]
			  ,[ReservingClassName]
			  ,[EVENT_TYPE_NAME] 
			  ,[PERIL_NAME_CL] 
			  ,[COVERAGE_NAME_CL] 
			  ,[TRANSACTION_TYPE_NAME]
			  ,[TRANSACTION_TYPE_IDENTIFIER]
			  ,[LOSS_TYPE_NAME]
			  ,[LOCAL_LOB_GROUP]
			  ,[LOCAL_LOB_BASE]
			  ,[CLAIM_ID]
			  ,[CLAIM_ANCHOR_IDENTIFIER]
		  FROM [SEG].[CLAIM_MOVEMENTS_BY_RES_CLASS] --[SEG].[VW_CLAIM_MOVEMENT_RES_CLAS]
		  )  pay

  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_1 on (	 pay.[ReservingClassCode] = RSD_1.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_1.L1_Pattern,''),
																	case  RSD_1.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_1.L2_Pattern,''),
																	case  RSD_1.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_1.[ReservingSegmOrder] = 1
																)

  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_2 on (	 pay.[ReservingClassCode] = RSD_2.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_2.L1_Pattern,''),
																	case  RSD_2.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_2.L2_Pattern,''),
																	case  RSD_2.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_2.[ReservingSegmOrder] = 2
																)


  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_3 on (	 pay.[ReservingClassCode] = RSD_3.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_3.L1_Pattern,''),
																	case  RSD_3.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_3.L2_Pattern,''),
																	case  RSD_3.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_3.[ReservingSegmOrder] = 3
																)


  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_4 on (	 pay.[ReservingClassCode] = RSD_4.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_4.L1_Pattern,''),
																	case  RSD_4.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_4.L2_Pattern,''),
																	case  RSD_4.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_4.[ReservingSegmOrder] = 4
																)


  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_5 on (	 pay.[ReservingClassCode] = RSD_5.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_5.L1_Pattern,''),
																	case  RSD_5.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_5.L2_Pattern,''),
																	case  RSD_5.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_5.[ReservingSegmOrder] = 5
																)


  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_6 on (	 pay.[ReservingClassCode] = RSD_6.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_6.L1_Pattern,''),
																	case  RSD_6.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_6.L2_Pattern,''),
																	case  RSD_6.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_6.[ReservingSegmOrder] = 6
																)


  LEFT JOIN	[SEG].[STG_RES_CLASS_SEGM_HIER]			 RSD_7 on (	 pay.[ReservingClassCode] = RSD_7.[ReservingClassCode] 
															and [dbo].[RegExIsMatch](coalesce(RSD_7.L1_Pattern,''),
																	case  RSD_7.L1_Attrib
																	when 'CAT Name'							then [EVENT_TYPE_NAME] 
																	when 'Cause of loss GI global code'		then [PERIL_NAME_CL] 
																	when 'Claim coverage name'				then [COVERAGE_NAME_CL] 
																	when 'Claim Expense payment type code'	then [TRANSACTION_TYPE_NAME]
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME]
																	when 'Group LOB'						then [LOCAL_LOB_GROUP]
																	when 'Line of Business code - Local'	then [LOCAL_LOB_BASE]
																	else ''
																	end 
																,1) =1 

															and [dbo].[RegExIsMatch](coalesce(RSD_7.L2_Pattern,''),
																	case  RSD_7.L2_Attrib
																	when 'LOSS TYPE'						then [LOSS_TYPE_NAME] 
																	else ''
																	end 
																,1) =1 
																--and coalesce([ReservingSegmValue],[ReservingSegmCustomValue])  is not null
																and RSD_7.[ReservingSegmOrder] = 7
																)

--where [CLAIM_ID]= '01161300019'
--where [CLAIM_ID] = '22163086783'
  --where  year (accident_date )  = 2016
  --and pay.[ReservingClassCode] = 'UKZM_03'


--) a
--group by a.[ReservingClassCode]
--         --,a.[LOCAL_LOB_GROUP]	  
--		 ,year (a.payment_date ) 
--order by 1,2,3