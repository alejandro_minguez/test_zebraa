﻿
CREATE VIEW [SEG].[vTHRESHOLD_ROW_SEVERITY_MONTH] AS

SELECT sv.[RESERVING_CLASS_CODE]
,sv.[SEVERITY_INDEX_ID]
,sv.[SEVERITY_INDEX_NAME]
,sv.[CURRENCY]
,th.[THRESHOLD_ORDER]
,th.[THRESHOLD_NAME]
,th.[THRESHOLD_VALUE]
,sv.[YEAR_MONTH]
,sv.[MONTH_VALUE]
,sv.[FINAL_SEVERITY_VALUE] AS SEVERITY_VALUE
,sv.[FINAL_SEVERITY_VALUE] * th.[THRESHOLD_VALUE] AS TH_SEV_MONTH_VALUE
,LEAD( sv.[FINAL_SEVERITY_VALUE] * th.[THRESHOLD_VALUE] ) over ( partition by sv.[RESERVING_CLASS_CODE], sv.[YEAR_MONTH] order by th.[THRESHOLD_ORDER] ) AS NEXT_TH_SEV_MONTH_VALUE 

FROM [SEG].[SEV_VALUES] sv
INNER JOIN [SEG].[STG_THRESHOLDS_ROW] th ON sv.[RESERVING_CLASS_CODE] = th.[RESERVING_CLASS_CODE] and sv.[SEVERITY_INDEX_ID] = th.[INFLATION_INDEX]

--order by 1,8,5