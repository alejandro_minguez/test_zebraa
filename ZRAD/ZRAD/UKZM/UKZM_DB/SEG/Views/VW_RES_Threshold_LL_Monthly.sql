﻿





CREATE VIEW [SEG].[VW_RES_Threshold_LL_Monthly] 
AS

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT sv.[RESERVING_CLASS_CODE]
      ,sv.[SEVERITY_INDEX_ID]
      ,sv.[SEVERITY_INDEX_NAME]
      ,sv.[Currency]
      ,th.[THRESHOLD_ORDER]
      ,th.[THRESHOLD_NAME]
      ,th.[THRESHOLD_VALUE] as [Threshold Value Orig]
      ,sv.[YEAR_MONTH]
	  ,sv.[MONTH_VALUE]
      ,sv.[FINAL_SEVERITY_VALUE]
	  ,sv.[FINAL_SEVERITY_VALUE] * th.[THRESHOLD_VALUE] as [Threshold Value Month]

--into [SEG].[STG_RES_Threshold_LL_Monthly]
  FROM [SEG].[SEV_VALUES] sv, [SEG].[STG_LL_THRESHOLDS_ROW] th
  where sv.[RESERVING_CLASS_CODE] = th.[RESERVING_CLASS_CODE]
	and	sv.[SEVERITY_INDEX_ID] = th.[INFLATION_INDEX]

--order by 1,2,5,8