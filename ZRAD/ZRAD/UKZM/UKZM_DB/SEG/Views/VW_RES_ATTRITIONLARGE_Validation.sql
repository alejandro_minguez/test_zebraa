﻿






CREATE VIEW [SEG].[VW_RES_ATTRITIONLARGE_Validation] 
AS

---------------------------------------------------------------------------------
--              SELECT VALIDACION ATTRITIONAL/LARGE ( Los 3 tipos )                  --
---------------------------------------------------------------------------------

SELECT 
	 [ReservingClassCode]
	,[CLAIM_ID]
	,[CLAIM_ANCHOR_IDENTIFIER]
	, [YEAR_MONTH]
	, Incurred
	,[AL_TH_Reached]									as 'AL_IFCURRENTLY'
	, max([AL_TH_Reached]) over ( partition by [CLAIM_ID] order by [YEAR_MONTH] )	as 'AL_AFTERCROSSING'
	, max([AL_TH_Reached]) over ( partition by [CLAIM_ID]  )			as 'AL_ONCEALWAYS'

FROM 
(
	SELECT [ReservingClassCode]
		  ,[CLAIM_ID]
		  ,[CLAIM_ANCHOR_IDENTIFIER]
		  ,[YEAR_MONTH]
		  ,[Threshold_1], [Threshold_2], [Threshold_3], [Threshold_4]
		  ,sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT])) as Incurred
		  ,		 CASE when [Threshold_4] is not null and sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))  >= [Threshold_4] 
			THEN 'TH4' 
			ELSE CASE when [Threshold_3] is not null and sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))  >= [Threshold_3] 
			THEN 'TH3' 
			ELSE CASE when [Threshold_2] is not null and sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))  >= [Threshold_2] 
			THEN 'TH2'
			ELSE CASE when [Threshold_1] is not null and sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))  >= [Threshold_1] 
			THEN 'TH1'
			ELSE 'TH0'
			END END END END  as 'AL_TH_Reached'

	 FROM [SEG].[CLAIM_MOVEMENTS_BY_RES_CLASS] ic
	 RIGHT JOIN  [SEG].[VW_RES_Threshold_LL_Monthly_PIV] ll on ( convert(varchar(6),ic.[PAYMENT_DATE],112) = ll.[YEAR_MONTH]
																and ic.[ReservingClassCode]= ll.[Reserving_Class_Code] )
		  WHERE [CLAIM_ID] = '01004244001'
		  --and YearMonth = 200205
	 group by 
		   [ReservingClassCode]
		  ,[CLAIM_ID]
		  ,[CLAIM_ANCHOR_IDENTIFIER]
		  ,[YEAR_MONTH] 
		  ,[Threshold_1], [Threshold_2], [Threshold_3], [Threshold_4]

) AL_CALC

--ORDER BY 	 
--     [ReservingClassCode]
--	,[CLAIM_ID]
--	,[CLAIM_ANCHOR_IDENTIFIER]
--    ,[YEAR_MONTH]