﻿






CREATE VIEW [SEG].[VW_RES_Threshold_LL_Monthly_PIV] 
AS
 
select [RESERVING_CLASS_CODE], [SEVERITY_INDEX_ID], [YEAR_MONTH], [Threshold 1] as [Threshold_1], [Threshold 2] as [Threshold_2], [Threshold 3] as [Threshold_3], [Threshold 4] as [Threshold_4]

 from 
(SELECT [RESERVING_CLASS_CODE], [SEVERITY_INDEX_ID], [THRESHOLD_NAME],[YEAR_MONTH],[Threshold Value Month]--, 'TH' + cast([Threshold Order] as nvarchar(4)) + ' : ' + cast([Threshold Value Orig] as nvarchar(30)) as 'TH Name'
    FROM [SEG].[VW_RES_Threshold_LL_Monthly]) AS SourceTable
PIVOT
(
AVG([Threshold Value Month])
FOR [THRESHOLD_NAME] IN ([Threshold 1], [Threshold 2], [Threshold 3], [Threshold 4])
) AS PivotTable