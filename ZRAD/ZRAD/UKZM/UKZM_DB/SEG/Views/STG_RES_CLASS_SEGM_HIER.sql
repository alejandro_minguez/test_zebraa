﻿


CREATE VIEW [SEG].[STG_RES_CLASS_SEGM_HIER] 
AS

SELECT sh.[ReservingClassCode]
      ,sh.[ReservingSegmOrder]
      ,sh.[ReservingSegmName]
      ,sh.[ReservingSegmCustomValue]
	  ,[ReservingSegmValue]
	  ,[L1_Attrib]
      ,[L1_Pattern]
      ,[L2_Attrib]
      ,[L2_Pattern]
      ,[L3_Attrib]
      ,[L3_Pattern]
	  ,coalesce(sh.use_in_IC, sh.use_in_EP )  as used_in_TG
  FROM [DZRADUKZM].[SEG].[STG_RES_SEGM_HIER] sh
  left outer join [DZRADUKZM].[SEG].[STG_RES_SEGM_DEF]  sd  on ( sh.[ReservingSegmName] = sd.[ReservingSegmName] )
 -- where coalesce(sh.use_in_IC, sh.use_in_EP ) is not null
  --and sh.[ReservingSegmName] not in ('LOB_GROUP','LOB_BASE' )
  --and [ReservingSegmValue] is not null
  --order by 1,2,3,5