﻿






CREATE VIEW [SEG].[VW_RES_CAPPEDEXCESS_Validation] 
AS
---------------------------------------------------------------------------------
--              SELECT VALIDACION CAPPED/EXCESS 			                  --
---------------------------------------------------------------------------------


	SELECT [ReservingClassCode]
		  ,[CLAIM_ID]
		  ,[CLAIM_ANCHOR_IDENTIFIER]
		  ,[YEAR_MONTH]
		  ,[THRESHOLD_NAME]
		  ,sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT])) as Incurred
		  ,sum([Threshold Value Month]) as [Threshold Value]
		  , CASE WHEN sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT])) <= sum([Threshold Value Month])
			THEN	  sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))
			ELSE  sum([Threshold Value Month]) 
			END as 'CAPPED'
		  
		  , CASE WHEN sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT])) <= sum([Threshold Value Month])
			THEN	 0
			ELSE   sum(cast([MOVEMENT_IS_LAST_RESERVE] as float)*([RESERVED_NET_AMOUNT]+[PAYMENT_NET_AMOUNT]))- sum([Threshold Value Month]) 
			END as 'EXCESS'

	 FROM [SEG].[CLAIM_MOVEMENTS_BY_RES_CLASS] ic
	 RIGHT JOIN  [SEG].[VW_RES_Threshold_LL_Monthly] ll on ( convert(varchar(6),ic.[PAYMENT_DATE],112) = ll.[YEAR_MONTH]
																and ic.[ReservingClassCode]= ll.[RESERVING_CLASS_CODE] )
		  WHERE [CLAIM_ID] = '01004244001'
		  --and YearMonth = 200205
	 group by 
		   [ReservingClassCode]
		  ,[CLAIM_ID]
		  ,[CLAIM_ANCHOR_IDENTIFIER]
		  ,[YEAR_MONTH] 
		  ,[THRESHOLD_NAME]


--ORDER BY 	 
--     [ReservingClassCode]
--	,[CLAIM_ID]
--	,[CLAIM_ANCHOR_IDENTIFIER]
--	,[YEAR_MONTH]
--	,[THRESHOLD_NAME]