﻿




CREATE VIEW [SEG].[VW_RESERVING_CLASS] 
AS

select	coalesce(RCD.[RESERVING_CLASS_CODE],'UKZM_UNDEF')			as [ReservingClassCode],
		coalesce(RCD.[RESERVING_CLASS_NAME],'Undefined')			as [ReservingClassName],
		--MLB.[MAJOR_GROUP_LINE_BUSINESS_DESCRIPTION] MajorLOB, 
	    MLB.[MAJOR_GROUP_LINE_BUSINESS_DESCRIPTION] AS [LOCAL_LOB_GROUP]						,
		GLB.[GROUP_LINE_BUSINESS_CODE] As [LOCAL_LOB_BASE]										,
--------------------------------------------------------------------------------------------
		UPD.[POLICY_TERM_START_DATE]								,
		UPD.[POLICY_CODE]											,
		UED.[UNDERWRITING_ENDORSEMENT_ANCHOR_IDENTIFIER]			,
		CLA.[CLAIM_NUMBER]											,
		CLA.[CLAIM_ANCHOR_IDENTIFIER]

from	[ZRAD].[CLAIM_DIMENSION]						CLA	
  join	[ZRAD].[UNDERWRITING_ENDORSEMENT_DIMENSION]		UED		on ( CLA.[UNDERWRITING_ENDORSEMENT_IDENTIFIER] = UED.[UNDERWRITING_ENDORSEMENT_ANCHOR_IDENTIFIER] )
  join	[ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]		UIT		on ( UED.[UNDERWRITING_ITEM_POLICY_IDENTIFIER] = UIT.[UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER] )
  join  [ZRAD].[UNDERWRITING_POLICY_DIMENSION]			UPD		on ( UIT.[UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER] = UPD.[UNDERWRITING_POLICY_ANCHOR_IDENTIFIER] )
  join  [ZRAD].[UNDERWRITING_FACT]						UWF		on ( UED.[UNDERWRITING_ENDORSEMENT_ANCHOR_IDENTIFIER] = UWF.[UNDERWRITING_ENDORSEMENT_IDENTIFIER] )
  join	[ZRAD].[LINE_BUSINESS_DIMENSION]				LOB		on ( UWF.[LINE_BUSINESS_IDENTIFIER] = LOB.[LINE_BUSINESS_ANCHOR_IDENTIFIER] ) -- TO DELETE AND USE PREV 1
  join	[ZRAD].[GROUP_LINE_BUSINESS_DIMENSION]			GLB		on ( LOB.[GROUP_LINE_BUSINESS_IDENTIFIER]		= GLB.[GROUP_LINE_BUSINESS_ANCHOR_IDENTIFIER] )
  join	[ZRAD].[MAJOR_GROUP_LINE_BUSINESS_DIMENSION]	MLB		on ( GLB.[MAJOR_GROUP_LINE_BUSINESS_IDENTIFIER]	= MLB.MAJOR_GROUP_LINE_BUSINESS_ANCHOR_IDENTIFIER )
  --join  [ZRAD].[VW_LOB_GLOBAL_TO_LOCAL]					LLB		on ( LOB.[LINE_BUSINESS_ANCHOR_IDENTIFIER] = LLB.[LINE_BUSINESS_IDENTIFIER])	


-- REGEX -- JOINS ------------------------------------------------------------------------


  LEFT JOIN [SEG].[STG_RES_CLASS_REGEX]					RCD		on ( [dbo].[RegExIsMatch](coalesce(RCD.[LEVEL_1_PATTERN],''),MLB.[MAJOR_GROUP_LINE_BUSINESS_DESCRIPTION] ,1) =1 and
																	 [dbo].[RegExIsMatch](coalesce(RCD.[LEVEL_2_PATTERN],''),GLB.[GROUP_LINE_BUSINESS_CODE],1) =1  and
																	 RCD.[RESERVING_CLASS_CODE] != 'UKZM_UNDEF')
-------------------------------------------------------------------------------------------




--and RCD.[ReservingClassCode]= 'UKZM_01'
--and MLB.[MAJOR_GROUP_LINE_BUSINESS_DESCRIPTION] = 'Motor'