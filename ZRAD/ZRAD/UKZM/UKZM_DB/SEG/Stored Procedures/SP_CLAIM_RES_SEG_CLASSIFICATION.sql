﻿
CREATE PROC [SEG].[SP_CLAIM_RES_SEG_CLASSIFICATION] AS 

-- Comprobamos si ya existe la tabla final para borrarla
/*
IF OBJECT_ID (N'SEG.CLAIM_RES_SEG_CLASSIFICATION', N'U') IS NOT NULL 
   DROP TABLE SEG.CLAIM_RES_SEG_CLASSIFICATION
*/

DECLARE @bu AS VARCHAR(10)='UKZM'
DECLARE @query AS VARCHAR(MAX)

---------------------------------------------
/* OBTENEMOS LA LISTA DE COLUMNAS A MOSTRAR*/
---------------------------------------------

DECLARE @columnName AS VARCHAR(200)
DECLARE @listaColumnas AS VARCHAR(MAX)=''
DECLARE C_COLUMNAS CURSOR FOR
	SELECT COLUMN_NAME FROM [ZQ].[VW_MAIN_SEG_COLUMNS]

OPEN C_COLUMNAS

FETCH NEXT FROM C_COLUMNAS   
INTO @columnName

WHILE @@FETCH_STATUS = 0  
BEGIN  
       PRINT @columnName
       SET @listaColumnas=@listaColumnas+','+@columnName+ CHAR(13) + CHAR(10)
       FETCH NEXT FROM C_COLUMNAS   
       INTO @columnName
END   
CLOSE C_COLUMNAS 
DEALLOCATE C_COLUMNAS

--Quitamos la primera coma
SET @listaColumnas = STUFF(@listaColumnas,1,1,'')
SELECT @listaColumnas


-------------------------------------------
/* OBTENEMOS LA LISTA DE JOINS A REALIZAR*/
-------------------------------------------

DECLARE @join AS VARCHAR(MAX)
DECLARE @listaJoins AS VARCHAR(MAX)=''
DECLARE C_JOINS CURSOR FOR
    SELECT [JOIN_TO_EXECUTE] FROM [ZQ].[VW_SPLIT_SEG_JOINS]
	ORDER BY [JOIN_ORDER]

OPEN C_JOINS

FETCH NEXT FROM C_JOINS   
INTO @join

WHILE @@FETCH_STATUS = 0  
BEGIN  
       PRINT @join
       SET @listaJoins=@listaJoins+@join+ CHAR(13) + CHAR(10)
       FETCH NEXT FROM C_JOINS   
       INTO @join
END   
CLOSE C_JOINS 
DEALLOCATE C_JOINS

--cambiamos el join contra la tabla de movement para que use el campo que tenemos en la tabla de
--los movements clasificados
/*
SET @listaJoins = REPLACE(@listaJoins,'CMF.[CLAIM_IDENTIFIER]','MAIN.[CLAIM_IDENTIFIER]')
SET @listaJoins = REPLACE(@listaJoins,'CMF.[TRANSACTION_TYPE_IDENTIFIER]','MAIN.[TRANSACTION_TYPE_IDENTIFIER]')
*/
SET @listaJoins = REPLACE(@listaJoins,'CMF.','MAIN.')

SELECT @listaJoins



--------------------------------------------------------------
/* CREAMOS CAMPOS DE SEGMENTOS POR NIVEL DE LA SELECT */
--------------------------------------------------------------
DECLARE @caseSelect1 AS VARCHAR(MAX) = ''
DECLARE @caseSelect2 AS VARCHAR(MAX) = ''
DECLARE @caseSelect3 AS VARCHAR(MAX) = ''
DECLARE @caseSelect4 AS VARCHAR(MAX) = ''
DECLARE @caseSelect5 AS VARCHAR(MAX) = ''
DECLARE @caseSelect6 AS VARCHAR(MAX) = ''
DECLARE @caseSelect7 AS VARCHAR(MAX) = ''
DECLARE @agregationLevel AS INT
DECLARE @cursor CURSOR
DECLARE @catName AS VARCHAR(400)
DECLARE @transName AS VARCHAR(400)



--=============================================================================--
---------- SELECT AGREGATION LEVEL 1 ----------
--=============================================================================--
SET @agregationLevel = 1

--Columna con el nombre de la jerarquia
SET @caseSelect1 = @caseSelect1 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect1 = @caseSelect1 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect1 = @caseSelect1+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect1 = @caseSelect1+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect1 = @caseSelect1+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect1 = @caseSelect1+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect1 = @caseSelect1+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect1 = @caseSelect1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect1 = @caseSelect1 + ' END)'
END

SET @caseSelect1=@caseSelect1+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect1



--=============================================================================--
---------- SELECT AGREGATION LEVEL 2 ----------
--=============================================================================--
SET @agregationLevel = 2


--Columna con el nombre de la jerarquia
SET @caseSelect2 = @caseSelect2 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect2 = @caseSelect2 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect2 = @caseSelect2+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect2 = @caseSelect2+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect2 = @caseSelect2+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect2 = @caseSelect2+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect2 = @caseSelect2+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect2 = @caseSelect2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect2 = @caseSelect2 + ' END)'
END

SET @caseSelect2=@caseSelect2+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect2



--=============================================================================--
---------- SELECT AGREGATION LEVEL 3 ----------
--=============================================================================--
SET @agregationLevel = 3

--Columna con el nombre de la jerarquia
SET @caseSelect3 = @caseSelect3 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect3 = @caseSelect3 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect3 = @caseSelect3+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect3 = @caseSelect3+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect3 = @caseSelect3+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect3 = @caseSelect3+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect3 = @caseSelect3+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect3 = @caseSelect3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect3 = @caseSelect3 + ' END)'
END

SET @caseSelect3=@caseSelect3+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect3


--=============================================================================--
---------- SELECT AGREGATION LEVEL 4 ----------
--=============================================================================--
SET @agregationLevel = 4

--Columna con el nombre de la jerarquia
SET @caseSelect4 = @caseSelect4 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect4 = @caseSelect4 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect4 = @caseSelect4+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect4 = @caseSelect4+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect4 = @caseSelect4 + ')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect4 = @caseSelect4+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect4 = @caseSelect4+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect4 = @caseSelect4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect4 = @caseSelect4 + ' END)'
END

SET @caseSelect4=@caseSelect4+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect4


--=============================================================================--
---------- SELECT AGREGATION LEVEL 5 ----------
--=============================================================================--
SET @agregationLevel = 5

--Columna con el nombre de la jerarquia
SET @caseSelect5 = @caseSelect5 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect5 = @caseSelect5 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect5 = @caseSelect5+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect5 = @caseSelect5+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect5 = @caseSelect5+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect5 = @caseSelect5+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect5 = @caseSelect5+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect5 = @caseSelect5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect5 = @caseSelect5 + ' END)'
END

SET @caseSelect5=@caseSelect5+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect5


--=============================================================================--
---------- SELECT AGREGATION LEVEL 6 ----------
--=============================================================================--
SET @agregationLevel = 6

--Columna con el nombre de la jerarquia
SET @caseSelect6 = @caseSelect6 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect6 = @caseSelect6 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect6 = @caseSelect6+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect6=@caseSelect6+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect6 = @caseSelect6+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect6 = @caseSelect6+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect6 = @caseSelect6+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect6 = @caseSelect6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect6 = @caseSelect6 + ' END)'
END

SET @caseSelect6=@caseSelect6+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect6


--=============================================================================--
---------- SELECT AGREGATION LEVEL 7 ----------
--=============================================================================--
SET @agregationLevel = 7

--Columna con el nombre de la jerarquia
SET @caseSelect7 = @caseSelect7 + ', T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME AS RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_NAME' + CHAR(13) + CHAR(10)
SET @caseSelect7 = @caseSelect7 + ', COALESCE(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_CUSTOM_VALUE, T'+CAST(@agregationLevel AS CHAR(1))+'.CUSTOM_VALUE'

--Comprobamos cuantos atributos diferentes tenemos
IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect7 = @caseSelect7+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SELECT DISTINCT @caseSelect7 = @caseSelect7+', '+CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseSelect7 = @caseSelect7+')'
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT SEGMENT_VARIABLE_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL) > 1
BEGIN
	--asignamos valor al cursor para iterar por los atributos del case
	SET @cursor = CURSOR SCROLL KEYSET FOR
		SELECT DISTINCT SEGMENT_VARIABLE_NAME, CATALOG_ATTRIBUTE_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Catalog Attribute' AND AGREGRATION_PRIORITY=@agregationLevel AND SEGMENT_VARIABLE_NAME IS NOT NULL
	OPEN @cursor
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName

	--Añadimos sentencia iif para que cuando sea un atributo que sale del main y no cruza, no ponga valor
	SET @caseSelect7 = @caseSelect7+', IIF(T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME IS NULL,NULL'
	SET @caseSelect7 = @caseSelect7+', CASE T'+CAST(@agregationLevel AS CHAR(1))+'.SEGMENT_VARIABLE_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseSelect7 = @caseSelect7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET @caseSelect7 = @caseSelect7 + ' END)'
END

SET @caseSelect7=@caseSelect7+') AS [RES_SEG_'+CAST(@agregationLevel AS CHAR(1))+'_VALUE]' + CHAR(13) + CHAR(10)
SELECT @caseSelect7



--------------------------------------------------------------
/* CREAMOS LOS LEFT JOIN POR NIVEL */
--------------------------------------------------------------

DECLARE @caseJoin1 AS VARCHAR(MAX) = ''
DECLARE @caseJoin2 AS VARCHAR(MAX) = ''
DECLARE @caseJoin3 AS VARCHAR(MAX) = ''
DECLARE @caseJoin4 AS VARCHAR(MAX) = ''
DECLARE @caseJoin5 AS VARCHAR(MAX) = ''
DECLARE @caseJoin6 AS VARCHAR(MAX) = ''
DECLARE @caseJoin7 AS VARCHAR(MAX) = ''
--DECLARE @agregationLevel AS INT


--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 1 ----------
--=============================================================================--

SET @agregationLevel = 1

SET @caseJoin1 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 1 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin1=@caseJoin1+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin1=@caseJoin1+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin1 = @caseJoin1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin1 = @caseJoin1 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin1


---------- REGEX LEVEL 2 - AGREGATION LEVEL 1 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin1=@caseJoin1+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin1=@caseJoin1+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin1 = @caseJoin1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin1 = @caseJoin1 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin1

---------- REGEX LEVEL 3 - AGREGATION LEVEL 1 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin1=@caseJoin1+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin1=@caseJoin1+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin1 = @caseJoin1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin1 = @caseJoin1 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin1


---------- REGEX LEVEL 4 - AGREGATION LEVEL 1 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin1=@caseJoin1+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin1=@caseJoin1+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin1 = @caseJoin1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin1 = @caseJoin1 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin1



---------- REGEX LEVEL 5 - AGREGATION LEVEL 1 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin1=@caseJoin1+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin1=@caseJoin1+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin1 = @caseJoin1+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin1 = @caseJoin1 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin1 = @caseJoin1 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin1

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 1 ----------
--=============================================================================--



--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 2 ----------
--=============================================================================--

SET @agregationLevel = 2

SET @caseJoin2 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 2 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin2=@caseJoin2+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin2=@caseJoin2+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin2 = @caseJoin2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin2 = @caseJoin2 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin2


---------- REGEX LEVEL 2 - AGREGATION LEVEL 2 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin2=@caseJoin2+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin2=@caseJoin2+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin2 = @caseJoin2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin2 = @caseJoin2 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin2

---------- REGEX LEVEL 3 - AGREGATION LEVEL 2 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin2=@caseJoin2+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin2=@caseJoin2+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin2 = @caseJoin2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin2 = @caseJoin2 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin2


---------- REGEX LEVEL 4 - AGREGATION LEVEL 2 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin2=@caseJoin2+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin2=@caseJoin2+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin2 = @caseJoin2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin2 = @caseJoin2 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin2



---------- REGEX LEVEL 5 - AGREGATION LEVEL 2 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin2=@caseJoin2+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin2=@caseJoin2+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin2 = @caseJoin2+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin2 = @caseJoin2 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin2 = @caseJoin2 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin2

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 2 ----------
--=============================================================================--




--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 3 ----------
--=============================================================================--

SET @agregationLevel = 3

SET @caseJoin3 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 3 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin3=@caseJoin3+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin3=@caseJoin3+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin3 = @caseJoin3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin3 = @caseJoin3 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin3


---------- REGEX LEVEL 2 - AGREGATION LEVEL 3 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin3=@caseJoin3+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin3=@caseJoin3+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin3 = @caseJoin3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin3 = @caseJoin3 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin3

---------- REGEX LEVEL 3 - AGREGATION LEVEL 3 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin3=@caseJoin3+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin3=@caseJoin3+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin3 = @caseJoin3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin3 = @caseJoin3 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin3


---------- REGEX LEVEL 4 - AGREGATION LEVEL 3 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin3=@caseJoin3+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin3=@caseJoin3+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin3 = @caseJoin3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin3 = @caseJoin3 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin3



---------- REGEX LEVEL 5 - AGREGATION LEVEL 3 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin3=@caseJoin3+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin3=@caseJoin3+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin3 = @caseJoin3+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin3 = @caseJoin3 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin3 = @caseJoin3 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin3

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 3 ----------
--=============================================================================--




--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 4 ----------
--=============================================================================--

SET @agregationLevel = 4

SET @caseJoin4 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 4 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin4=@caseJoin4+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin4=@caseJoin4+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin4 = @caseJoin4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin4 = @caseJoin4 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin4


---------- REGEX LEVEL 2 - AGREGATION LEVEL 4 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin4=@caseJoin4+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin4=@caseJoin4+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin4 = @caseJoin4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin4 = @caseJoin4 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin4

---------- REGEX LEVEL 3 - AGREGATION LEVEL 4 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin4=@caseJoin4+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin4=@caseJoin4+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin4 = @caseJoin4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin4 = @caseJoin4 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin4


---------- REGEX LEVEL 4 - AGREGATION LEVEL 4 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin4=@caseJoin4+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin4=@caseJoin4+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin4 = @caseJoin4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin4 = @caseJoin4 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin4



---------- REGEX LEVEL 5 - AGREGATION LEVEL 4 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin4=@caseJoin4+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin4=@caseJoin4+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin4 = @caseJoin4+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin4 = @caseJoin4 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin4 = @caseJoin4 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin4

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 4 ----------
--=============================================================================--




--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 5 ----------
--=============================================================================--

SET @agregationLevel = 5

SET @caseJoin5 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 5 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin5=@caseJoin5+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin5=@caseJoin5+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin5 = @caseJoin5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin5 = @caseJoin5 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin5


---------- REGEX LEVEL 2 - AGREGATION LEVEL 5 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin5=@caseJoin5+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin5=@caseJoin5+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin5 = @caseJoin5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin5 = @caseJoin5 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin5

---------- REGEX LEVEL 3 - AGREGATION LEVEL 5 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin5=@caseJoin5+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin5=@caseJoin5+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin5 = @caseJoin5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin5 = @caseJoin5 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin5


---------- REGEX LEVEL 4 - AGREGATION LEVEL 5 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin5=@caseJoin5+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin5=@caseJoin5+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin5 = @caseJoin5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin5 = @caseJoin5 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin5



---------- REGEX LEVEL 5 - AGREGATION LEVEL 5 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin5=@caseJoin5+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin5=@caseJoin5+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin5 = @caseJoin5+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin5 = @caseJoin5 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin5 = @caseJoin5 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin5

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 5 ----------
--=============================================================================--




--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 6 ----------
--=============================================================================--

SET @agregationLevel = 6

SET @caseJoin6 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 6 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin6=@caseJoin6+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin6=@caseJoin6+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin6 = @caseJoin6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin6 = @caseJoin6 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin6


---------- REGEX LEVEL 2 - AGREGATION LEVEL 6 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin6=@caseJoin6+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin6=@caseJoin6+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin6 = @caseJoin6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin6 = @caseJoin6 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin6

---------- REGEX LEVEL 3 - AGREGATION LEVEL 6 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin6=@caseJoin6+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin6=@caseJoin6+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin6 = @caseJoin6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin6 = @caseJoin6 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin6


---------- REGEX LEVEL 4 - AGREGATION LEVEL 6 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin6=@caseJoin6+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin6=@caseJoin6+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin6 = @caseJoin6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin6 = @caseJoin6 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin6



---------- REGEX LEVEL 5 - AGREGATION LEVEL 6 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin6=@caseJoin6+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin6=@caseJoin6+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin6 = @caseJoin6+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin6 = @caseJoin6 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin6 = @caseJoin6 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin6

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 6 ----------
--=============================================================================--




--=============================================================================--
---------- INICIO - JOIN AGREGATION LEVEL 7 ----------
--=============================================================================--

SET @agregationLevel = 7

SET @caseJoin7 = 'LEFT JOIN ZQ.VW_SEG_TRANSLATIONS T'+CAST(@agregationLevel AS CHAR(1))+' ON T'+CAST(@agregationLevel AS CHAR(1))+'.AGREGRATION_PRIORITY='+CAST(@agregationLevel AS CHAR(1))
+' AND T'+CAST(@agregationLevel AS CHAR(1))+'.RESERVING_CLASS_CODE=MAIN.RESERVING_CLASS_CODE' + CHAR(13) + CHAR(10)

---------- REGEX LEVEL 1 - AGREGATION LEVEL 7 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_1_CATALOG_NAME, LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), '
	SELECT DISTINCT @caseJoin7=@caseJoin7+LEVEL_1_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin7=@caseJoin7+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_1_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_1_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_1_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin7 = @caseJoin7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END   

	SET  @caseJoin7 = @caseJoin7 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin7


---------- REGEX LEVEL 2 - AGREGATION LEVEL 7 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_2_CATALOG_NAME, LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), '
	SELECT DISTINCT @caseJoin7=@caseJoin7+LEVEL_2_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin7=@caseJoin7+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_2_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_2_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_2_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin7 = @caseJoin7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin7 = @caseJoin7 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin7

---------- REGEX LEVEL 3 - AGREGATION LEVEL 7 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_3_CATALOG_NAME, LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), '
	SELECT DISTINCT @caseJoin7=@caseJoin7+LEVEL_3_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin7=@caseJoin7+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_3_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_3_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_3_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin7 = @caseJoin7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin7 = @caseJoin7 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin7


---------- REGEX LEVEL 4 - AGREGATION LEVEL 7 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_4_CATALOG_NAME, LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), '
	SELECT DISTINCT @caseJoin7=@caseJoin7+LEVEL_4_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin7=@caseJoin7+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_4_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_4_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_4_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin7 = @caseJoin7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin7 = @caseJoin7 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
--SELECT @caseJoin7



---------- REGEX LEVEL 5 - AGREGATION LEVEL 7 ----------
--asignamos valor al cursor para iterar por los atributos del case
SET @cursor = CURSOR SCROLL KEYSET FOR
	SELECT DISTINCT LEVEL_5_CATALOG_NAME, LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL
OPEN @cursor
FETCH NEXT FROM @cursor   
INTO @catName, @transName

IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
	WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) = 1
BEGIN
	--si solo tenemos 1 atributo no necesitamos el case
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), '
	SELECT DISTINCT @caseJoin7=@caseJoin7+LEVEL_5_TRANSLATE_NAME FROM [ZQ].[VW_SEG_TRANSLATIONS] WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel
	SET @caseJoin7=@caseJoin7+', 1)=1' + CHAR(13) + CHAR(10)
END
--si tenemos mas de 1 atributo, entonces formamos la sentencia case
ELSE IF (SELECT COUNT(DISTINCT LEVEL_5_CATALOG_NAME) FROM [ZQ].[VW_SEG_TRANSLATIONS] 
		WHERE SEGMENT_TYPE = 'Segment Variable' AND AGREGRATION_PRIORITY=@agregationLevel AND LEVEL_5_CATALOG_NAME IS NOT NULL) > 1
BEGIN
	SET @caseJoin7 = @caseJoin7+' AND [dbo].[RegExIsMatch](coalesce(T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_PATTERN,''''), ' + CHAR(13) + CHAR(10)
		+'CASE T'+CAST(@agregationLevel AS CHAR(1))+'.LEVEL_5_CATALOG_NAME ' + CHAR(13) + CHAR(10)

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SET @caseJoin7 = @caseJoin7 + 'WHEN '''+@catName+''' THEN '+@transName + CHAR(13) + CHAR(10)
	FETCH NEXT FROM @cursor   
	INTO @catName, @transName
	END  

	SET  @caseJoin7 = @caseJoin7 + 'ELSE '''' END, 1)=1' + CHAR(13) + CHAR(10)
END
SELECT @caseJoin7

--=============================================================================--
---------- FIN - JOIN AGREGATION LEVEL 7 ----------
--=============================================================================--



/*
------ Formamos consulta ------
*/

SET @query = 'SELECT ' + @listaColumnas 
+ @caseSelect1
+ @caseSelect2
+ @caseSelect3
+ @caseSelect4
+ @caseSelect5
+ @caseSelect6
+ @caseSelect7
--+ 'INTO SEG.CLAIM_RES_SEG_CLASSIFICATION 
+' FROM SEG.CLAIM_RES_CLASS_CLASSIFICATION AS MAIN '+ CHAR(13) + CHAR(10)
+ @listaJoins
+ @caseJoin1
+ @caseJoin2
+ @caseJoin3
+ @caseJoin4
+ @caseJoin5
+ @caseJoin6
+ @caseJoin7


--select @query
--PRINT @query

--Ejecutamos la consulta
exec @query