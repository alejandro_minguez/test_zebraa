﻿CREATE TABLE [SEG].[CLAIM_RES_CLASS_SPLIT] (
    [RESERVING_CLASS_CODE]    VARCHAR (50) NULL,
    [CLAIM_ANCHOR_IDENTIFIER] NUMERIC (12) NULL,
    [CLAIM_NUMBER]            VARCHAR (30) NULL,
    [YEAR_MONTH]              VARCHAR (10) NULL,
    [THRESHOLD_NAME]          VARCHAR (50) NULL,
    [TH_SEV_MONTH_VALUE]      FLOAT (53)   NULL,
    [INCURRED]                FLOAT (53)   NULL,
    [EXCESS]                  FLOAT (53)   NULL,
    [CAPPED]                  FLOAT (53)   NULL,
    [ATT_LARGE_AC]            FLOAT (53)   NULL,
    [ATT_LARGE_IC]            FLOAT (53)   NULL,
    [ATT_LARGE_OA]            FLOAT (53)   NULL
);

