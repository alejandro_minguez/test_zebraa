﻿CREATE TABLE [SEG].[LDG_RES_Severity_Values] (
    [Severity_Index_ID] VARCHAR (25)  NULL,
    [Value_YearMonth]   INT           NULL,
    [Value]             FLOAT (53)    NULL,
    [Execution_id]      INT           NOT NULL,
    [FileName]          VARCHAR (100) NOT NULL
);



