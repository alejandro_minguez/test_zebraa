﻿CREATE TABLE [SEG].[STG_CLAIMS_AGRUP] (
    [CLAIM_IDENTIFIER]            NUMERIC (12) NOT NULL,
    [TRANSACTION_TYPE_IDENTIFIER] NUMERIC (12) NOT NULL,
    [CLAIM_STATUS_IDENTIFIER]     NUMERIC (12) NOT NULL,
    [CLAIM_DATE]                  DATE         NULL,
    [ORIGIN_DATE]                 DATE         NULL,
    [P_OF_MTH]                    DATE         NULL,
    [P_OF_QTR]                    DATE         NULL,
    [P_OF_YEAR]                   DATE         NULL,
    [M_A_INC]                     FLOAT (53)   NULL,
    [M_A_TPAY]                    FLOAT (53)   NULL,
    [M_A_TPAY_SETTLED]            FLOAT (53)   NULL,
    [M_A_TPAY_OPEN]               FLOAT (53)   NULL,
    [M_A_TRES]                    FLOAT (53)   NULL,
    [M_A_TPAY_REC]                FLOAT (53)   NULL,
    [M_A_INC_REC]                 FLOAT (53)   NULL,
    [M_A_INC_OPEN]                FLOAT (53)   NULL,
    [M_A_TPAY_ALAE]               FLOAT (53)   NULL,
    [M_A_INC_ALAE]                FLOAT (53)   NULL,
    [BASE_M_CURRENT]              FLOAT (53)   NULL,
    [BASE_M_MAX]                  FLOAT (53)   NULL,
    [BASE_M_MAX_TODATE]           FLOAT (53)   NULL
);

