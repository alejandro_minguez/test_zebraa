﻿CREATE TABLE [SEG].[STG_RES_Severity_Values] (
    [ReservingClass Code] VARCHAR (25)  NOT NULL,
    [Severity_Index_ID]   VARCHAR (25)  NULL,
    [Severity Index Name] VARCHAR (255) NULL,
    [Currency]            VARCHAR (3)   NULL,
    [YearMonth]           INT           NULL,
    [Severity]            FLOAT (53)    NULL,
    [DateValid From]      DATETIME      NULL,
    [DateValid To]        DATETIME      NULL,
    [Version_id]          INT           NOT NULL,
    [FileName]            VARCHAR (100) NOT NULL
);



