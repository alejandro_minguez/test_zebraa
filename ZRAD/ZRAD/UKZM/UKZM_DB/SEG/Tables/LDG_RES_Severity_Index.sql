﻿CREATE TABLE [SEG].[LDG_RES_Severity_Index] (
    [Severity_Index_ID]          VARCHAR (25)  NULL,
    [Severity Index Name]        VARCHAR (255) NULL,
    [Currency]                   VARCHAR (3)   NULL,
    [First_YearMonth]            INT           NULL,
    [Last_YearMonth]             INT           NULL,
    [Backward_monthly_inflation] FLOAT (53)    NULL,
    [Forward_monthly_inflation]  FLOAT (53)    NULL,
    [DateValid From]             DATETIME      NULL,
    [DateValid To]               DATETIME      NULL,
    [Execution_id]               INT           NOT NULL,
    [FileName]                   VARCHAR (100) NOT NULL
);

