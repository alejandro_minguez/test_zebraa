﻿CREATE TABLE [SEG].[STG_RES_PARAMS] (
    [ReservingClassCode]    VARCHAR (25)  NOT NULL,
    [ParameterType]         VARCHAR (255) NOT NULL,
    [ParameterCode]         VARCHAR (25)  NOT NULL,
    [ParameterName]         VARCHAR (255) NOT NULL,
    [ParameterValue_Char]   VARCHAR (255) NULL,
    [ParameterValue_Number] FLOAT (53)    NULL,
    [ParameterValue_Date]   DATE          NULL,
    [DateValid From]        DATETIME      NULL,
    [DateValid To]          DATETIME      NULL,
    [Execution_id]          BIGINT        NULL,
    [FILE_NAME]             VARCHAR (255) NULL
);

