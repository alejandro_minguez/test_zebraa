﻿CREATE TABLE [SEG].[STG_RES_SEGM_HIER] (
    [ReservingClassCode]       VARCHAR (25)  NOT NULL,
    [ReservingSegmOrder]       BIGINT        NOT NULL,
    [ReservingSegmName]        VARCHAR (255) NOT NULL,
    [ReservingSegmCustomValue] VARCHAR (255) NULL,
    [Aggregate]                VARCHAR (3)   NULL,
    [Use_in_IC]                VARCHAR (3)   NULL,
    [Use_in_EP]                VARCHAR (3)   NULL,
    [DateValid From]           DATETIME      NULL,
    [DateValid To]             DATETIME      NULL,
    [version_id]               INT           NOT NULL
);

