﻿CREATE TABLE [SEG].[STG_RES_SEGM_DEF] (
    [ReservingSegmName]  VARCHAR (255) NOT NULL,
    [ReservingSegmValue] VARCHAR (255) NULL,
    [L1_Attrib]          VARCHAR (255) NULL,
    [L1_Pattern]         VARCHAR (255) NULL,
    [L2_Attrib]          VARCHAR (255) NULL,
    [L2_Pattern]         VARCHAR (255) NULL,
    [L3_Attrib]          VARCHAR (255) NULL,
    [L3_Pattern]         VARCHAR (255) NULL,
    [L4_Attrib]          VARCHAR (255) NULL,
    [L4_Pattern]         VARCHAR (255) NULL,
    [L5_Attrib]          VARCHAR (255) NULL,
    [L5_Pattern]         VARCHAR (255) NULL,
    [DateValid From]     DATETIME      NULL,
    [DateValid To]       DATETIME      NULL,
    [Version_id]         BIGINT        NULL,
    [is_Last_version]    BIT           NOT NULL
);

