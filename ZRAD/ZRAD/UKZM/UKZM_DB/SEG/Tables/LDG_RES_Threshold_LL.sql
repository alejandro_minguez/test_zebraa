﻿CREATE TABLE [SEG].[LDG_RES_Threshold_LL] (
    [ReservingClass Code] VARCHAR (25)  NULL,
    [Inflation Index]     VARCHAR (25)  NULL,
    [Inflation Base Date] INT           NULL,
    [Currency]            VARCHAR (3)   NULL,
    [Threshold 1]         FLOAT (53)    NULL,
    [Threshold 2]         FLOAT (53)    NULL,
    [Threshold 3]         FLOAT (53)    NULL,
    [Threshold 4]         FLOAT (53)    NULL,
    [Execution_id]        INT           NULL,
    [FileName]            VARCHAR (100) NULL,
    [DateValid From]      DATETIME      NULL,
    [DateValid To]        DATETIME      NULL
);



