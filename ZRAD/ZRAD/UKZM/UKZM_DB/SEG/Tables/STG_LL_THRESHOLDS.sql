﻿CREATE TABLE [SEG].[STG_LL_THRESHOLDS] (
    [RESERVING_CLASS_CODE]     VARCHAR (50)  NULL,
    [RESERVING_CLASS_NAME]     VARCHAR (100) NULL,
    [LARGE_LOSS_METHOD]        VARCHAR (50)  NULL,
    [BASED_MEASURE]            VARCHAR (255) NULL,
    [THRESHOLD_1]              FLOAT (53)    NULL,
    [THRESHOLD_2]              FLOAT (53)    NULL,
    [THRESHOLD_3]              FLOAT (53)    NULL,
    [THRESHOLD_4]              FLOAT (53)    NULL,
    [CURRENCY]                 VARCHAR (10)  NULL,
    [INFLATION_INDEX]          VARCHAR (50)  NULL,
    [INFLATION_BASE_YEARMONTH] INT           NULL,
    [DATEVALID_FROM]           DATE          NULL,
    [DATEVALID_TO]             DATE          NULL,
    [$sq_row_hash]             BIGINT        NULL,
    [$sq_execution_id]         BIGINT        NOT NULL
);

