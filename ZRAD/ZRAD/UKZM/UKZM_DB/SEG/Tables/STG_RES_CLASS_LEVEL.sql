﻿CREATE TABLE [SEG].[STG_RES_CLASS_LEVEL] (
    [ReservingClassCode]        VARCHAR (25)  NOT NULL,
    [ReservingClassName]        VARCHAR (255) NULL,
    [L1_Pattern_Group LOB_NAME] VARCHAR (255) NULL,
    [L2_Pattern_LOCAL_LOB_CODE] VARCHAR (255) NULL,
    [DateValid From]            DATETIME      NULL,
    [DateValid To]              DATETIME      NULL,
    [Version_id]                BIGINT        NULL,
    [is_Last_version]           BIT           NOT NULL
);

