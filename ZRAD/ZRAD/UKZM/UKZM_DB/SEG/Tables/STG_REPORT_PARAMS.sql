﻿CREATE TABLE [SEG].[STG_REPORT_PARAMS] (
    [TECH_PARAMETER_CLASS] VARCHAR (100) NULL,
    [TECH_PARAMETER_CODE]  VARCHAR (100) NULL,
    [PARAMETER]            VARCHAR (70)  NULL,
    [VALUE]                VARCHAR (20)  NULL,
    [DATEVALID_FROM]       DATE          NULL,
    [DATEVALID_TO]         DATE          NULL,
    [$sq_row_hash]         BIGINT        NULL,
    [$sq_execution_id]     BIGINT        NOT NULL
);

