﻿CREATE TABLE [SEG].[STG_THRESHOLDS_ROW] (
    [RESERVING_CLASS_CODE]     VARCHAR (50)  NULL,
    [RESERVING_CLASS_NAME]     VARCHAR (100) NULL,
    [LARGE_LOSS_METHOD]        VARCHAR (50)  NULL,
    [THRESHOLD_ORDER]          SMALLINT      NULL,
    [THRESHOLD_NAME]           VARCHAR (50)  NULL,
    [THRESHOLD_VALUE]          FLOAT (53)    NULL,
    [CURRENCY]                 VARCHAR (10)  NULL,
    [INFLATION_INDEX]          VARCHAR (50)  NULL,
    [INFLATION_BASE_YEARMONTH] INT           NULL,
    [DATEVALID_FROM]           DATE          NULL,
    [DATEVALID_TO]             DATE          NULL
);

