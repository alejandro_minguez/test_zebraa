﻿CREATE TABLE [SEG].[LDG_RES_SEGM_DEF] (
    [ReservingSegmName]  VARCHAR (255) NOT NULL,
    [ReservingSegmValue] VARCHAR (255) NULL,
    [L1_Attrib]          VARCHAR (255) NULL,
    [L1_Filter]          VARCHAR (25)  NULL,
    [L1_Values]          VARCHAR (255) NULL,
    [L2_Attrib]          VARCHAR (255) NULL,
    [L2_Filter]          VARCHAR (25)  NULL,
    [L2_Values]          VARCHAR (255) NULL,
    [L3_Attrib]          VARCHAR (255) NULL,
    [L3_Filter]          VARCHAR (25)  NULL,
    [L3_Values]          VARCHAR (255) NULL,
    [L4_Attrib]          VARCHAR (255) NULL,
    [L4_Filter]          VARCHAR (25)  NULL,
    [L4_Values]          VARCHAR (255) NULL,
    [L5_Attrib]          VARCHAR (255) NULL,
    [L5_Filter]          VARCHAR (25)  NULL,
    [L5_Values]          VARCHAR (255) NULL,
    [DateValid From]     DATETIME      NULL,
    [DateValid To]       DATETIME      NULL,
    [Execution_id]       INT           NULL,
    [FileName]           VARCHAR (100) NULL
);

