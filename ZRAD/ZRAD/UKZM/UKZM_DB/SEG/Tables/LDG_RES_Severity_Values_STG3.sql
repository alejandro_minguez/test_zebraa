﻿CREATE TABLE [SEG].[LDG_RES_Severity_Values_STG3] (
    [ReservingClass Code]        VARCHAR (25)  NOT NULL,
    [Severity_Index_ID]          VARCHAR (25)  NULL,
    [Severity Index Name]        VARCHAR (255) NULL,
    [Currency]                   VARCHAR (3)   NULL,
    [MinBase_YearMonth]          INT           NULL,
    [MaxBase_YearMonth]          INT           NULL,
    [First_YearMonth]            INT           NULL,
    [Last_YearMonth]             INT           NULL,
    [Backward_monthly_inflation] FLOAT (53)    NULL,
    [Forward_monthly_inflation]  FLOAT (53)    NULL,
    [YearMonth_Value]            INT           NULL,
    [Monthly_Value]              FLOAT (53)    NULL,
    [InflationBase_YearMonth]    INT           NULL,
    [InflationBase_Value]        FLOAT (53)    NULL,
    [Final_Value]                FLOAT (53)    NULL,
    [DateValid From]             DATETIME      NULL,
    [DateValid To]               DATETIME      NULL,
    [Version_id]                 INT           NOT NULL,
    [FileName]                   VARCHAR (100) NOT NULL
);

