﻿CREATE TABLE [SEG].[STG_RES_CLASS_DIM] (
    [ReservingClassCode] VARCHAR (25)  NOT NULL,
    [ReservingClassName] VARCHAR (255) NULL,
    [Country]            VARCHAR (2)   NOT NULL,
    [BU]                 VARCHAR (2)   NOT NULL,
    [LL_Split]           BIT           NULL,
    [LL_Method]          VARCHAR (25)  NULL,
    [Min_Base_Date]      DATETIME      NULL,
    [DateValid From]     DATETIME      NULL,
    [DateValid To]       DATETIME      NULL,
    [Version_id]         BIGINT        NULL,
    [is_Last_version]    BIT           NOT NULL
);

