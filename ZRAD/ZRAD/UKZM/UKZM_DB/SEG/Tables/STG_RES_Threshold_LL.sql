﻿CREATE TABLE [SEG].[STG_RES_Threshold_LL] (
    [ReservingClass Code] VARCHAR (25)  NOT NULL,
    [Inflation Index]     VARCHAR (25)  NULL,
    [Currency]            VARCHAR (3)   NULL,
    [Threshold Order]     INT           NULL,
    [Threshold Name]      VARCHAR (25)  NULL,
    [Threshold Value]     FLOAT (53)    NULL,
    [DateValid From]      DATETIME      NULL,
    [DateValid To]        DATETIME      NULL,
    [Version_id]          INT           NULL,
    [FileName]            VARCHAR (100) NULL
);

