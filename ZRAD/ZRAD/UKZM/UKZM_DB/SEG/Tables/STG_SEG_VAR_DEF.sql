﻿CREATE TABLE [SEG].[STG_SEG_VAR_DEF] (
    [SEGMENT_VARIABLE_NAME]         VARCHAR (100) NULL,
    [SEGMENT_VARIABLE_CUSTOM_VALUE] VARCHAR (150) NULL,
    [LEVEL_1_CATALOG_NAME]          VARCHAR (100) NULL,
    [LEVEL_1_FILTER]                VARCHAR (20)  NULL,
    [LEVEL_1_VALUES]                VARCHAR (150) NULL,
    [LEVEL_2_CATALOG_NAME]          VARCHAR (100) NULL,
    [LEVEL_2_FILTER]                VARCHAR (20)  NULL,
    [LEVEL_2_VALUES]                VARCHAR (150) NULL,
    [LEVEL_3_CATALOG_NAME]          VARCHAR (100) NULL,
    [LEVEL_3_FILTER]                VARCHAR (20)  NULL,
    [LEVEL_3_VALUES]                VARCHAR (150) NULL,
    [LEVEL_4_CATALOG_NAME]          VARCHAR (100) NULL,
    [LEVEL_4_FILTER]                VARCHAR (20)  NULL,
    [LEVEL_4_VALUES]                VARCHAR (150) NULL,
    [LEVEL_5_CATALOG_NAME]          VARCHAR (100) NULL,
    [LEVEL_5_FILTER]                VARCHAR (20)  NULL,
    [LEVEL_5_VALUES]                VARCHAR (150) NULL,
    [DATEVALID_FROM]                DATE          NULL,
    [DATEVALID_TO]                  DATE          NULL,
    [$sq_row_hash]                  BIGINT        NULL,
    [$sq_execution_id]              BIGINT        NOT NULL
);

