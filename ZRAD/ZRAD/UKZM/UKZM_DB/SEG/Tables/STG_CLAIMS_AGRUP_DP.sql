﻿CREATE TABLE [SEG].[STG_CLAIMS_AGRUP_DP] (
    [CLAIM_IDENTIFIER]            NUMERIC (12) NULL,
    [TRANSACTION_TYPE_IDENTIFIER] NUMERIC (12) NULL,
    [CLAIM_DATE]                  DATE         NULL,
    [CLAIM_DATE_NEW]              DATE         NULL,
    [YEAR_MONTH_NEW]              AS           (CONVERT([char](6),[CLAIM_DATE_NEW],(112))),
    [DUMMY]                       TINYINT      NULL,
    [P_OF_MTH]                    DATE         NULL,
    [P_OF_QTR]                    DATE         NULL,
    [P_OF_YEAR]                   DATE         NULL,
    [DP_DMY]                      AS           (datediff(month,[P_OF_YEAR],[CLAIM_DATE_NEW])+(1)),
    [DP_DQY]                      AS           (datediff(quarter,[P_OF_YEAR],[CLAIM_DATE_NEW])+(1)),
    [DP_DYY]                      AS           (datediff(year,[P_OF_YEAR],[CLAIM_DATE_NEW])+(1)),
    [DP_DMQ]                      AS           (datediff(month,[P_OF_QTR],[CLAIM_DATE_NEW])+(1)),
    [DP_DQQ]                      AS           (datediff(quarter,[P_OF_QTR],[CLAIM_DATE_NEW])+(1)),
    [DP_DYQ]                      AS           (datediff(year,[P_OF_QTR],[CLAIM_DATE_NEW])+(1)),
    [DP_DMM]                      AS           (datediff(month,[P_OF_MTH],[CLAIM_DATE_NEW])+(1)),
    [DP_DQM]                      AS           (datediff(quarter,[P_OF_MTH],[CLAIM_DATE_NEW])+(1)),
    [DP_DYM]                      AS           (datediff(year,[P_OF_MTH],[CLAIM_DATE_NEW])+(1))
);

