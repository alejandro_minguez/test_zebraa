﻿CREATE TABLE [SEG].[LDG_RES_Severity_Values_STG1] (
    [Severity_Index_ID]   VARCHAR (25)  NULL,
    [Value_YearMonth]     INT           NULL,
    [Value]               FLOAT (53)    NULL,
    [NextValue_YearMonth] INT           NULL,
    [NextValue]           FLOAT (53)    NULL,
    [DateValid From]      DATETIME      NULL,
    [DateValid To]        DATETIME      NULL,
    [Execution_id]        INT           NOT NULL,
    [FileName]            VARCHAR (100) NOT NULL
);

