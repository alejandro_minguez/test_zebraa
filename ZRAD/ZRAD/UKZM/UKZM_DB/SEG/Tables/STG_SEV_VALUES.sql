﻿CREATE TABLE [SEG].[STG_SEV_VALUES] (
    [SEVERITY_INDEX_ID]   VARCHAR (50)  NULL,
    [SEVERITY_INDEX_NAME] VARCHAR (255) NULL,
    [SEVERITY_YEARMONTH]  INT           NULL,
    [SEVERITY_VALUE]      FLOAT (53)    NULL,
    [$sq_row_hash]        BIGINT        NULL,
    [$sq_execution_id]    BIGINT        NOT NULL
);

