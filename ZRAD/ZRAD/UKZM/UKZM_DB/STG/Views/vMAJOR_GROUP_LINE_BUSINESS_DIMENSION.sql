﻿







CREATE VIEW [STG].[vMAJOR_GROUP_LINE_BUSINESS_DIMENSION] AS 
 
SELECT cast([TEXT_IDENTIFIER] as int)  AS [MAJOR_GROUP_LINE_BUSINESS_ANCHOR_IDENTIFIER],
      [TEXT_CODE] AS MAJOR_LINE_BUSINESS_CODE,
      [TEXT_NAME] AS MAJOR_LINE_BUSINESS_NAME,
      [TEXT_DESCRIPTION] AS MAJOR_LINE_BUSINESS_DESCRIPTION,
      [RECORD_INCLUSION_DATE] AS [RECORD_INCLUSION_DATE], 
      [RECORD_UPDATE_DATE] AS [RECORD_UPDATE_DATE], 
      [SOURCE_SYSTEM_IDENTIFIER] AS [SOURCE_SYSTEM_IDENTIFIER]
  FROM [STG].[TEXT] 

UNION ALL

SELECT 1 as [MAJOR_GROUP_LINE_BUSINESS_ANCHOR_IDENTIFIER],'PN01' AS MAJOR_LINE_BUSINESS_CODE,'Accident' AS MAJOR_LINE_BUSINESS_NAME,	'PN01 - Accident' AS MAJOR_LINE_BUSINESS_DESCRIPTION,GETDATE(),GETDATE(),-1
UNION ALL
SELECT 2,'PN02',	'Health (Sickness)','PN02 - Health (Sickness)',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 3,'PN03',	'Marine, Aviation, Transport',	'PN03 - Marine, Aviation, Transport',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 4,'PN04',	'Credit/Mortgage and Surety',	'PN04 - Credit/Mortgage and Surety',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 5,'PN05',	'Crime/Fidelity/Pecuniary',	'PN05 - Crime/Fidelity/Pecuniary',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 6,'PN06',	'Motor - 3rd Party/Liab',	'PN06 - Motor - 3rd Party/Liab',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 7,'PN07',	'Motor - all other'	,'PN07 - Motor - all other',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 8,'PN08',	'Property',	'PN08 - Property',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 9,'PN09',	'Property - Engineering Lines',	'PN09 - Property - Engineering Lines',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 10,'PN10',	'Liability - Primary - Products',	'PN10 - Liability - Primary - Products',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 11,'PN11',	'Liability - Primary - Non-Products',	'PN11 - Liability - Primary - Non-Products',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 12,'PN12',	'Liability - Excess Policies	','PN12 - Liability - Excess Policies',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 13,'PN13',	'Professional Indemnity',	'PN13 - Professional Indemnity',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 14,'PN14',	'Work Comp/EL',	'PN14 - Work Comp/EL',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 15,'PN15',	'Work Comp/EL - High Deductible',	'PN15 - Work Comp/EL - High Deductible',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 16,'PN16',	'Multi-Peril',	'PN16 - Multi-Peril',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 17,'PN17',	'Legal Expenses',	'PN17 - Legal Expenses',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 18,'PN18',	'Assistance',	'PN18 - Assistance',GETDATE(),GETDATE(),-1
UNION ALL
SELECT 19,'PN19',	'Miscellaneous',	'PN19 - Miscellaneous',GETDATE(),GETDATE(),-1