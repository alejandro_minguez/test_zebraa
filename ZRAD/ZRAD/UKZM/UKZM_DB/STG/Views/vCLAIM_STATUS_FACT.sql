﻿
CREATE VIEW [STG].[vCLAIM_STATUS_FACT] AS

WITH BASE AS (
	SELECT CLM_SEQ AS [CLAIM_IDENTIFIER],
	COALESCE(ESTIMATE_DATE, '19000101') AS [MOVEMENT_CLAIM_DATE],
	[SETTLEMENT_DATE],
	[NOTIFICATION_DATE],
	[ACCIDENT_DATE]
	FROM [LDG].[TCLM] CLM WITH (NOLOCK)
	INNER JOIN [LDG].[TEST] TE  WITH (NOLOCK) ON CLM.CLM_SEQ=TE.FK_CLM_SEQ
	WHERE COMPANY = 'ZM' AND ACCIDENT_DATE>='19930101' AND NOTIFICATION_DATE>='19930101'

	UNION ALL 

	SELECT CLM_SEQ AS [CLAIM_IDENTIFIER],
	IIF(PA.[RECOVERY] IN ('C','D') AND LEFT(COALESCE(PA.[PAYEE_NAME],''),1) IN (' ',''),PA.[DATE_ENTERED],IIF(PAYMENT_DATE IS NULL OR PAYMENT_DATE = '', 
	COALESCE(PA.[DATE_ENTERED],'19000101'),PAYMENT_DATE)) AS [MOVEMENT_CLAIM_DATE],
	[SETTLEMENT_DATE],
	[NOTIFICATION_DATE],
	[ACCIDENT_DATE]
	FROM [LDG].[TCLM] CLM  WITH (NOLOCK)
	INNER JOIN [LDG].[TPAY] PA  WITH (NOLOCK) ON CLM.CLM_SEQ=PA.FK_CLM_SEQ
	WHERE COMPANY = 'ZM' AND ACCIDENT_DATE>='19930101' AND NOTIFICATION_DATE>='19930101'

),FIRST_DATE AS (
	SELECT [CLAIM_IDENTIFIER],
	[MOVEMENT_CLAIM_DATE],
	[SETTLEMENT_DATE],
	[NOTIFICATION_DATE],
	[ACCIDENT_DATE],
	ROW_NUMBER() OVER(PARTITION BY [CLAIM_IDENTIFIER] ORDER BY [NOTIFICATION_DATE] ASC, [ACCIDENT_DATE] ASC, [MOVEMENT_CLAIM_DATE] ASC) AS FIRST_OPEN,
	ROW_NUMBER() OVER(PARTITION BY [CLAIM_IDENTIFIER] ORDER BY [SETTLEMENT_DATE] DESC) AS FIRST_SETTLED
	FROM BASE
)
SELECT [CLAIM_IDENTIFIER]
,[SETTLEMENT_DATE] AS [CLAIM_STATUS_DATE]
,'CLOSED' AS [LKP_CLAIM_STATUS_IDENTIFIER]
,GETDATE() AS [RECORD_INCLUSION_DATE]
,GETDATE() AS [RECORD_UPDATE_DATE]
,-1 AS [SOURCE_SYSTEM_IDENTIFIER]
FROM FIRST_DATE 
WHERE FIRST_SETTLED = 1 AND [SETTLEMENT_DATE] IS NOT NULL

UNION ALL

SELECT [CLAIM_IDENTIFIER]
,COALESCE([NOTIFICATION_DATE],COALESCE([ACCIDENT_DATE],COALESCE([MOVEMENT_CLAIM_DATE],CAST('19000101' AS DATETIME)))) AS [CLAIM_STATUS_DATE]
,'OPEN' AS [LKP_CLAIM_STATUS_IDENTIFIER]
,GETDATE() AS [RECORD_INCLUSION_DATE]
,GETDATE() AS [RECORD_UPDATE_DATE]
,-1 AS [SOURCE_SYSTEM_IDENTIFIER]
FROM FIRST_DATE
WHERE FIRST_OPEN = 1