﻿CREATE TABLE [STG].[TEXT] (
    [TEXT_IDENTIFIER]          NUMERIC (12)  NOT NULL,
    [TEXT_CODE]                VARCHAR (50)  NOT NULL,
    [TEXT_NAME]                VARCHAR (100) NOT NULL,
    [TEXT_DESCRIPTION]         VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]    DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]       DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER] NUMERIC (12)  NOT NULL,
    CONSTRAINT [PK_STG_TEXT] PRIMARY KEY CLUSTERED ([TEXT_IDENTIFIER] ASC)
);

