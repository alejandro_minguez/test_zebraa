﻿CREATE TABLE [err].[SEGMENTS] (
    [COB]              VARCHAR (255) NULL,
    [BRANCH_CODE]      FLOAT (53)    NULL,
    [LEDGER_CODE]      VARCHAR (255) NULL,
    [SEGMENT]          VARCHAR (255) NULL,
    [SUB_SEGMENT]      VARCHAR (255) NULL,
    [SEGMENT_LV_1]     VARCHAR (255) NULL,
    [SEGMENT_LV_2]     VARCHAR (255) NULL,
    [SEGMENT_LV_3]     VARCHAR (255) NULL,
    [START_DATE]       DATETIME      NULL,
    [END_DATE]         DATETIME      NULL,
    [TIME_STAMP]       DATETIME      NULL,
    [IS_CURR]          FLOAT (53)    NULL,
    [$sq_row_hash]     BIGINT        NULL,
    [$sq_execution_id] BIGINT        NOT NULL,
    [$sq_operation_id] TINYINT       NULL
);

