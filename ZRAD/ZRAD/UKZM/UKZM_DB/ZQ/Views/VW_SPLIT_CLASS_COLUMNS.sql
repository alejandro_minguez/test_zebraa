﻿


CREATE VIEW [ZQ].[VW_SPLIT_CLASS_COLUMNS] AS

SELECT DISTINCT ZRAD_TABLE_ALIAS_NAME, ZRAD_TABLE_COLUMN_NAME, ZRAD_TABLE_ALIAS_NAME +'.'+ ZRAD_TABLE_COLUMN_NAME AS COLUMN_NAME
FROM [SEG].[STG_RES_CLASS_DEF] RCD
INNER JOIN [SEG].[STG_DATA_CATALOGUE] DC ON RCD.LEVEL_1_CATALOG_NAME=DC.CATALOGUE_ATTRIBUTE_NAME
WHERE DC.[ZRAD_JOIN_SOURCE] = 'SJ'

UNION

SELECT DISTINCT ZRAD_TABLE_ALIAS_NAME, ZRAD_TABLE_COLUMN_NAME, ZRAD_TABLE_ALIAS_NAME +'.'+ ZRAD_TABLE_COLUMN_NAME AS COLUMN_NAME
FROM [SEG].[STG_RES_CLASS_DEF] RCD
INNER JOIN [SEG].[STG_DATA_CATALOGUE] DC ON RCD.LEVEL_2_CATALOG_NAME=DC.CATALOGUE_ATTRIBUTE_NAME
WHERE DC.[ZRAD_JOIN_SOURCE] = 'SJ'