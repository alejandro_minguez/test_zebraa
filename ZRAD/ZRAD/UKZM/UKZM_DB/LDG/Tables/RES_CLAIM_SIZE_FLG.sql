﻿CREATE TABLE [LDG].[RES_CLAIM_SIZE_FLG] (
    [ReservingClassCode] VARCHAR (9)  NULL,
    [ReservingClassName] VARCHAR (11) NULL,
    [LCSplit]            FLOAT (53)   NULL,
    [LCCurrency]         VARCHAR (3)  NULL,
    [LC_use]             VARCHAR (3)  NULL,
    [MCSplit]            FLOAT (53)   NULL,
    [MCCurrency]         VARCHAR (3)  NULL,
    [MC_use]             VARCHAR (2)  NULL,
    [ATTSplit]           VARCHAR (1)  NULL,
    [ATTCurrency]        VARCHAR (3)  NULL,
    [ATT_Use]            VARCHAR (3)  NULL,
    [DateValidFrom]      DATE         NULL,
    [DateValidTo]        DATE         NULL
);

