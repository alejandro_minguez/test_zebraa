﻿CREATE TABLE [LDG].[LINE_BUSINESS] (
    [MAJOR_GROUP_LOB]   VARCHAR (50)  NULL,
    [UKGI_LV_1]         VARCHAR (50)  NULL,
    [UKGI_LV_2]         VARCHAR (50)  NULL,
    [UKGI_LV_3]         VARCHAR (50)  NULL,
    [LOB_BASE]          VARCHAR (50)  NULL,
    [GROUP_LOB]         VARCHAR (50)  NULL,
    [REPORTING_LOB]     VARCHAR (50)  NULL,
    [COVER_GROUP_ID]    FLOAT (53)    NULL,
    [COVER_GROUP]       VARCHAR (50)  NULL,
    [COVER_DESCRIPTION] VARCHAR (50)  NULL,
    [PRODUCT_TYPE]      VARCHAR (50)  NULL,
    [COB_CODE]          VARCHAR (50)  NULL,
    [LOB_KEY]           VARCHAR (50)  NULL,
    [$sq_execution_id]  BIGINT        NULL,
    [FILE_NAME]         VARCHAR (100) NULL
);

