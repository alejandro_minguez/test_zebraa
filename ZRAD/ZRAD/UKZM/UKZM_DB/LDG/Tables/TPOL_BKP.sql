﻿CREATE TABLE [LDG].[TPOL_BKP] (
    [POL_SEQ]           FLOAT (53)   NULL,
    [FK_COB]            VARCHAR (3)  NULL,
    [FK_BRANCH_CODE]    VARCHAR (2)  NULL,
    [LEDGER]            VARCHAR (4)  NULL,
    [SEQUENCE_NO]       VARCHAR (3)  NULL,
    [FK_DEPT_CODE]      VARCHAR (1)  NULL,
    [FK_PACKAGE_PREFIX] VARCHAR (1)  NULL,
    [FK_PACKAGE_CODE]   VARCHAR (2)  NULL,
    [SCHEDULE_NO]       VARCHAR (2)  NULL,
    [AGENT_CODE]        VARCHAR (4)  NULL,
    [SOURCE_SYSTEM]     VARCHAR (1)  NULL,
    [ZUR_EFF_DATE]      DATE         NULL,
    [POLICY_NO]         VARCHAR (12) NULL,
    [LOB]               VARCHAR (3)  NULL
);

