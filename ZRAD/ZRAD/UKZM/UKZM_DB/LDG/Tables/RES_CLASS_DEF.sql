﻿CREATE TABLE [LDG].[RES_CLASS_DEF] (
    [ReservingClassCode] VARCHAR (4)   NULL,
    [ReservingClassName] VARCHAR (11)  NULL,
    [LOB_GROUP]          VARCHAR (25)  NULL,
    [LOB_BASE]           VARCHAR (500) NULL
);

