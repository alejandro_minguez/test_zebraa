﻿CREATE TABLE [LDG].[RES_SEGMENT_DEF] (
    [ReservingSegmentVar]  VARCHAR (16) NULL,
    [ReservingSegmentName] VARCHAR (30) NULL,
    [DefaultValueFlag]     VARCHAR (3)  NULL,
    [segment_lv_1]         VARCHAR (20) NULL,
    [segment_lv_2]         VARCHAR (31) NULL,
    [segment_lv_3]         VARCHAR (14) NULL,
    [LOB_GROUP]            VARCHAR (22) NULL,
    [LOB_BASE]             VARCHAR (5)  NULL,
    [TYPE]                 VARCHAR (1)  NULL,
    [INJ_IND]              VARCHAR (4)  NULL,
    [PERIL_1]              VARCHAR (10) NULL,
    [PERIL_2]              VARCHAR (10) NULL,
    [CAUSE_CODE]           VARCHAR (2)  NULL,
    [OTHER_CODE]           VARCHAR (3)  NULL,
    [DESCRIPTION]          VARCHAR (14) NULL,
    [FREEZE]               VARCHAR (1)  NULL
);

