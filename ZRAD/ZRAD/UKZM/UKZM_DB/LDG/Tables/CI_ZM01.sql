﻿CREATE TABLE [LDG].[CI_ZM01] (
    [MEASURE]       VARCHAR (40) NULL,
    [LOB_GROUP]     VARCHAR (25) NULL,
    [Capped_Excess] VARCHAR (15) NULL,
    [ACC_YR]        DATE         NULL,
    [ACC_QTR]       DATE         NULL,
    [DQ]            FLOAT (53)   NULL,
    [DMQ]           FLOAT (53)   NULL,
    [VARIABLE]      FLOAT (53)   NULL
);

