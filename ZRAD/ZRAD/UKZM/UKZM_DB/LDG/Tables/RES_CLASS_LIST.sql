﻿CREATE TABLE [LDG].[RES_CLASS_LIST] (
    [Country]            VARCHAR (2)  NULL,
    [BU]                 VARCHAR (2)  NULL,
    [ReservingClassCode] VARCHAR (4)  NULL,
    [ReservingClassName] VARCHAR (11) NULL
);

