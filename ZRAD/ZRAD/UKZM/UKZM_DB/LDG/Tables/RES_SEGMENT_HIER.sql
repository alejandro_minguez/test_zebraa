﻿CREATE TABLE [LDG].[RES_SEGMENT_HIER] (
    [ReservingClassCode]    VARCHAR (4)  NULL,
    [ReservingClassName]    VARCHAR (11) NULL,
    [ReservingSegmentLevel] FLOAT (53)   NULL,
    [ReservingSegmentVar]   VARCHAR (16) NULL,
    [UseInCITriangle]       BIT          NULL,
    [UseInEPTriangle]       BIT          NULL,
    [Aggregate]             BIT          NULL,
    [DateValidFrom]         DATETIME     NULL,
    [DateValidTo]           DATETIME     NULL,
    [USER_CREATOR]          VARCHAR (50) NULL,
    [USER_MODIFIER]         VARCHAR (50) NULL
);

