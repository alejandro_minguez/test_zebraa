﻿CREATE TABLE [LDG].[RES_INFLATION_] (
    [ReservingClassCode] VARCHAR (4)  NULL,
    [ReservingClassName] VARCHAR (11) NULL,
    [Inflation]          FLOAT (53)   NULL,
    [InflationYear]      FLOAT (53)   NULL,
    [DateValidFrom]      DATE         NULL,
    [DateValidTo]        DATE         NULL
);

