﻿CREATE TABLE [LDG].[RES_SEVERITY] (
    [ReservingClassCode] VARCHAR (4)  NULL,
    [ReservingClassName] VARCHAR (11) NULL,
    [SeverityRate]       FLOAT (53)   NULL,
    [OriginYear]         INT          NULL,
    [SeveriryYear]       FLOAT (53)   NULL,
    [DateValidFrom]      DATE         NULL,
    [DateValidTo]        DATE         NULL
);

