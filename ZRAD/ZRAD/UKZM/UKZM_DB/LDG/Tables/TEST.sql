﻿CREATE TABLE [LDG].[TEST] (
    [ESTIMATE_DATE] DATE        NULL,
    [TYPE]          VARCHAR (1) NULL,
    [GROSS_AMNT]    FLOAT (53)  NULL,
    [NET_AMNT]      FLOAT (53)  NULL,
    [FK_CLM_SEQ]    FLOAT (53)  NULL
);

