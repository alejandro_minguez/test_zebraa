﻿CREATE TABLE [LDG].[TPAY] (
    [PAYMENT_DATE]    DATE         NULL,
    [PAY_SEQUENCE_NO] FLOAT (53)   NULL,
    [PAYMENT_IND]     VARCHAR (1)  NULL,
    [FK_TYPE]         VARCHAR (1)  NULL,
    [RECOVERY]        VARCHAR (1)  NULL,
    [AMNT]            FLOAT (53)   NULL,
    [GROSS_AMNT]      FLOAT (53)   NULL,
    [DATE_ENTERED]    DATE         NULL,
    [PAYEE_NAME]      VARCHAR (40) NULL,
    [DP_ACC_DATE]     FLOAT (53)   NULL,
    [CHEQUE_NO]       VARCHAR (6)  NULL,
    [FK_CLM_SEQ]      FLOAT (53)   NULL
);

