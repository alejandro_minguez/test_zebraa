﻿CREATE TABLE [LDG].[RU_TP] (
    [Category]       NVARCHAR (255) NULL,
    [RU]             NVARCHAR (255) NULL,
    [Company_code]   NVARCHAR (255) NULL,
    [Description]    NVARCHAR (255) NULL,
    [Life_GI]        NVARCHAR (255) NULL,
    [Parent_company] NVARCHAR (255) NULL,
    [Report_Until]   NVARCHAR (255) NULL,
    [Consolid_until] NVARCHAR (255) NULL,
    [Country]        NVARCHAR (255) NULL
);

