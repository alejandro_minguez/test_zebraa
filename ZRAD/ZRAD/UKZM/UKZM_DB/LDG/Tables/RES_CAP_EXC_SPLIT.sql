﻿CREATE TABLE [LDG].[RES_CAP_EXC_SPLIT] (
    [ReservingClassCode] VARCHAR (9)  NULL,
    [ReservingClassName] VARCHAR (11) NULL,
    [SplitName]          VARCHAR (4)  NULL,
    [Split]              FLOAT (53)   NULL,
    [Currency]           VARCHAR (3)  NULL,
    [Capped]             VARCHAR (3)  NULL,
    [Excess]             VARCHAR (3)  NULL,
    [DateValidFrom]      DATE         NULL,
    [DateValidTo]        DATE         NULL
);

