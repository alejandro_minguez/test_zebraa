﻿CREATE TABLE [LDG].[RES_SEGMENT_HIER_] (
    [ReservingClassCode] VARCHAR (4)  NULL,
    [ReservingClassName] VARCHAR (11) NULL,
    [SegmentLevel]       FLOAT (53)   NULL,
    [SegmentVariable]    VARCHAR (16) NULL,
    [Values]             VARCHAR (29) NULL,
    [Aggregate]          VARCHAR (2)  NULL
);

