﻿CREATE TABLE [LDG].[CI_ZM02] (
    [MEASURE]       VARCHAR (100) NULL,
    [LOB_GROUP]     VARCHAR (50)  NULL,
    [CAPPED_EXCESS] VARCHAR (15)  NULL,
    [ACC_YR]        DATE          NULL,
    [ACC_QTR]       DATE          NULL,
    [DQ]            SMALLINT      NULL,
    [DMQ]           SMALLINT      NULL,
    [VARIABLE]      FLOAT (53)    NULL
);

