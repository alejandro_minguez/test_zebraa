﻿CREATE TABLE [LDG].[TRIAN_CI_SUM_ZM01] (
    [LOB_GROUP]             VARCHAR (25) NULL,
    [Capped_Excess]         VARCHAR (15) NULL,
    [ACC_YR]                DATE         NULL,
    [ACC_QTR]               DATE         NULL,
    [DQ]                    FLOAT (53)   NULL,
    [DMQ]                   FLOAT (53)   NULL,
    [INC_NET]               FLOAT (53)   NULL,
    [TPAY_NET]              FLOAT (53)   NULL,
    [TPAY_SETTLED]          FLOAT (53)   NULL,
    [TPAY_REC]              FLOAT (53)   NULL,
    [INC_REC]               FLOAT (53)   NULL,
    [TPAY_ALAE]             FLOAT (53)   NULL,
    [INC_ALAE]              FLOAT (53)   NULL,
    [TPAY_OPEN]             FLOAT (53)   NULL,
    [INC_OPEN]              FLOAT (53)   NULL,
    [CLAIM_COUNT]           FLOAT (53)   NULL,
    [COUNT_SETTLED]         FLOAT (53)   NULL,
    [COUNT_SETTLED_NIL]     FLOAT (53)   NULL,
    [COUNT_OPEN]            FLOAT (53)   NULL,
    [COUNT_SETTLED_NOT_NIL] FLOAT (53)   NULL,
    [COUNT_INC_ZERO]        FLOAT (53)   NULL
);

