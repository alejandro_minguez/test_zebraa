﻿CREATE TABLE [LDG].[CI_ZM03] (
    [MEASURE]          VARCHAR (100) NULL,
    [LOB_GROUP]        VARCHAR (50)  NULL,
    [LOB_BASE]         VARCHAR (50)  NULL,
    [PROPERTY_PERIL_1] VARCHAR (50)  NULL,
    [PROPERTY_PERIL_2] VARCHAR (50)  NULL,
    [FREEZE_FLG]       VARCHAR (50)  NULL,
    [CAPPED_EXCESS]    VARCHAR (15)  NULL,
    [ACC_YR]           DATE          NULL,
    [ACC_QTR]          DATE          NULL,
    [DQ]               SMALLINT      NULL,
    [DMQ]              SMALLINT      NULL,
    [VARIABLE]         FLOAT (53)    NULL
);

