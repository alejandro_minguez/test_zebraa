﻿CREATE TABLE [LDG].[TCLM_BKP] (
    [CLAIM_NO]          VARCHAR (11) NULL,
    [CLM_SEQ]           FLOAT (53)   NULL,
    [FK_POL_SEQ]        FLOAT (53)   NULL,
    [LOB]               VARCHAR (3)  NULL,
    [NO_OF_CLAIMS]      FLOAT (53)   NULL,
    [CAUSE_CODE]        VARCHAR (4)  NULL,
    [CAUSE_MODULE]      VARCHAR (2)  NULL,
    [ACCIDENT_DATE]     DATE         NULL,
    [NOTIFICATION_DATE] DATE         NULL,
    [SETTLEMENT_DATE]   DATE         NULL,
    [PERS_INJURY_IND]   VARCHAR (1)  NULL,
    [STATUS]            VARCHAR (1)  NULL,
    [FK_POLICY_NO]      VARCHAR (12) NULL,
    [OTHER_CODE]        VARCHAR (3)  NULL,
    [DESCRIPTION]       VARCHAR (40) NULL,
    [COMPANY]           VARCHAR (2)  NULL
);

