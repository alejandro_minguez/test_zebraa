﻿CREATE TABLE [LDG].[TCON_RAW] (
    [FK_BRANCH_CODE]     VARCHAR (2)  NULL,
    [FK_LEDGER]          VARCHAR (4)  NULL,
    [FK_SEQUENCE_NO]     VARCHAR (3)  NULL,
    [FK_DEPT_CODE]       VARCHAR (1)  NULL,
    [FK_SCHEDULE_NO]     VARCHAR (2)  NULL,
    [COIN_CONS_DATE]     DATE         NULL,
    [COIN_TOTAL_PERCENT] FLOAT (53)   NULL,
    [REIN_TOTAL_PERCENT] FLOAT (53)   NULL,
    [FK_POLICY_NO]       VARCHAR (12) NULL,
    [CUST_BRANCH_CODE]   VARCHAR (2)  NULL,
    [CUST_LEDGER]        VARCHAR (4)  NULL
);

