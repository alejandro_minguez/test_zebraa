﻿CREATE TABLE [LOG].[EXECUTION] (
    [ID]                    INT           IDENTITY (1, 1) NOT NULL,
    [ID_PAQUETE]            VARCHAR (80)  NULL,
    [DES_PAQUETE]           VARCHAR (254) NULL,
    [FEC_EJEC]              DATETIME      NOT NULL,
    [FEC_FIN_EJEC]          DATETIME      NULL,
    [DES_USUARIO]           VARCHAR (40)  NOT NULL,
    [COD_ESTADO]            INT           NOT NULL,
    [DES_ESTADO]            VARCHAR (40)  NOT NULL,
    [DES_REGOK]             INT           NULL,
    [DES_REGERR]            INT           NULL,
    [ExecutionInstanceGUID] VARCHAR (40)  NOT NULL
);

