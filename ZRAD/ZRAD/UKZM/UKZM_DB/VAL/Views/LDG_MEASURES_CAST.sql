﻿



CREATE VIEW  [VAL].[LDG_MEASURES$CAST] AS

 
 with mysource as ( 
 select *,  
 	try_cast(TECH_PARAMETER_CLASS as varchar(100)) as TECH_PARAMETER_CLASS_output,
	try_cast(TECH_PARAMETER_CODE  as varchar(100)) as TECH_PARAMETER_CODE_output,
	try_cast([DOMAIN] as varchar(50)) as [DOMAIN_output], 
	try_cast([MEASURE_NAME] as varchar(100)) as [MEASURE_NAME_output], 
	try_cast([MEASURE_DESCRIPTION] as varchar(255)) as [MEASURE_DESCRIPTION_output], 
	try_cast([USE] as varchar(3)) as [USE_output], 
	try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
	try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
 from  [SEG].[LDG_MEASURES]
 WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos
 
 ,mySourceWithFlags as ( 
 select *,
	case when  TECH_PARAMETER_CLASS_output is null and TECH_PARAMETER_CLASS is not null then 1 else 0 end as TECH_PARAMETER_CLASS_error, 
	case when  TECH_PARAMETER_CODE_output is null and TECH_PARAMETER_CODE is not null then 1 else 0 end as TECH_PARAMETER_CODE_error, 
	case when  [DOMAIN_output] is null and [DOMAIN] is not null then 1 else 0 end as [DOMAIN_error], 
	case when  [MEASURE_NAME_output] is null and [MEASURE_NAME] is not null then 1 else 0 end as [MEASURE_NAME_error], 
	case when  [MEASURE_DESCRIPTION_output] is null and [MEASURE_DESCRIPTION] is not null then 1 else 0 end as [MEASURE_DESCRIPTION_error], 
	case when  [USE_output] is null and [USE] is not null then 1 else 0 end as [USE_error], 
	case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
	case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
 from mysource) 
 
 select * , [DOMAIN_error]+ [MEASURE_NAME_error]+ TECH_PARAMETER_CLASS_error+TECH_PARAMETER_CODE_error+ [MEASURE_DESCRIPTION_error]+ [USE_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;