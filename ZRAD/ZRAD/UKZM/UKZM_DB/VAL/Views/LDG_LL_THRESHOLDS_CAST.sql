﻿


CREATE VIEW  [VAL].[LDG_LL_THRESHOLDS$CAST] AS

with mysource as ( 
	select *,  
		try_cast([RESERVING_CLASS_CODE] as varchar(50)) as [RESERVING_CLASS_CODE_output], 
		try_cast([RESERVING_CLASS_NAME] as varchar(100)) as [RESERVING_CLASS_NAME_output], 
		try_cast([LARGE_LOSS_METHOD] as varchar(50)) as [LARGE_LOSS_METHOD_output], 
		try_cast([BASED_MEASURE] as varchar(50)) as [BASED_MEASURE_output],
		try_cast([THRESHOLD_1] as float) as [THRESHOLD_1_output], 
		try_cast([THRESHOLD_2] as float) as [THRESHOLD_2_output], 
		try_cast([THRESHOLD_3] as float) as [THRESHOLD_3_output], 
		try_cast([THRESHOLD_4] as float) as [THRESHOLD_4_output], 
		try_cast([CURRENCY] as varchar(10)) as [CURRENCY_output], 
		try_cast([INFLATION_INDEX] as varchar(50)) as [INFLATION_INDEX_output], 
		try_cast([INFLATION_BASE_YEARMONTH] as int) as [INFLATION_BASE_YEARMONTH_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
	from  [SEG].[LDG_LL_THRESHOLDS]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos 

,mySourceWithFlags as ( 
	select *, 
		case when  [RESERVING_CLASS_CODE_output] is null and [RESERVING_CLASS_CODE] is not null then 1 else 0 end as [RESERVING_CLASS_CODE_error], 
		case when  [RESERVING_CLASS_NAME_output] is null and [RESERVING_CLASS_NAME] is not null then 1 else 0 end as [RESERVING_CLASS_NAME_error], 
		case when  [LARGE_LOSS_METHOD_output] is null and [LARGE_LOSS_METHOD] is not null then 1 else 0 end as [LARGE_LOSS_METHOD_error],
		case when  [BASED_MEASURE_output] is null and [BASED_MEASURE] is not null then 1 else 0 end as [BASED_MEASURE_error],
		case when  [THRESHOLD_1_output] is null and [THRESHOLD_1] is not null then 1 else 0 end as [THRESHOLD_1_error], 
		case when  [THRESHOLD_2_output] is null and [THRESHOLD_2] is not null then 1 else 0 end as [THRESHOLD_2_error], 
		case when  [THRESHOLD_3_output] is null and [THRESHOLD_3] is not null then 1 else 0 end as [THRESHOLD_3_error], 
		case when  [THRESHOLD_4_output] is null and [THRESHOLD_4] is not null then 1 else 0 end as [THRESHOLD_4_error], 
		case when  [CURRENCY_output] is null and [CURRENCY] is not null then 1 else 0 end as [CURRENCY_error], 
		case when  [INFLATION_INDEX_output] is null and [INFLATION_INDEX] is not null then 1 else 0 end as [INFLATION_INDEX_error], 
		case when  [INFLATION_BASE_YEARMONTH_output] is null and [INFLATION_BASE_YEARMONTH] is not null then 1 else 0 end as [INFLATION_BASE_YEARMONTH_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
	from mysource) 

select * , [RESERVING_CLASS_CODE_error]+ [RESERVING_CLASS_NAME_error]+ [LARGE_LOSS_METHOD_error]+ [BASED_MEASURE_error]+[THRESHOLD_1_error]+ [THRESHOLD_2_error]+ [THRESHOLD_3_error]+ [THRESHOLD_4_error]+ [CURRENCY_error]+ [INFLATION_INDEX_error]+ [INFLATION_BASE_YEARMONTH_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;