﻿


 CREATE VIEW [VAL].[SEGMENTS] AS 
 
SELECT 
[COB_output] AS [COB]
,[BRANCH_CODE_output] AS [BRANCH_CODE]
,[LEDGER_CODE_output] AS [LEDGER_CODE]
,[SEGMENT_output] AS [SEGMENT]
,[SUB_SEGMENT_output] AS [SUB_SEGMENT]
,[SEGMENT_LV_1_output] AS [SEGMENT_LV_1]
,[SEGMENT_LV_2_output] AS [SEGMENT_LV_2]
,[SEGMENT_LV_3_output] AS [SEGMENT_LV_3]
,[START_DATE_output] as [START_DATE]
,[END_DATE_output] as [END_DATE]
,[TIME_STAMP_output] as [TIME_STAMP]
,[IS_CURR_output] as [IS_CURR]
FROM [VAL].[SEGMENTS$CAST]
WHERE [$sq_transformation_error_count]=0