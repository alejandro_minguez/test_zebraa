﻿
CREATE VIEW [VAL].[SEGMENTS$CAST]
AS
WITH mysource
AS (
	SELECT *
		,try_cast([COB] AS VARCHAR(255)) AS [COB_output]
		,try_cast([BRANCH_CODE] AS FLOAT) AS [BRANCH_CODE_output]
		,try_cast([LEDGER_CODE] AS VARCHAR(255)) AS [LEDGER_CODE_output]
		,try_cast([SEGMENT] AS VARCHAR(255)) AS [SEGMENT_output]
		,try_cast([SUB_SEGMENT] AS VARCHAR(255)) AS [SUB_SEGMENT_output]
		,try_cast([SEGMENT_LV_1] AS VARCHAR(255)) AS [SEGMENT_LV_1_output]
		,try_cast([SEGMENT_LV_2] AS VARCHAR(255)) AS [SEGMENT_LV_2_output]
		,try_cast([SEGMENT_LV_3] AS VARCHAR(255)) AS [SEGMENT_LV_3_output]
		,try_cast([START_DATE] AS DATETIME) AS [START_DATE_output]
		,try_cast([END_DATE] AS DATETIME) AS [END_DATE_output]
		,try_cast([TIME_STAMP] AS DATETIME) AS [TIME_STAMP_output]
		,try_cast([IS_CURR] AS FLOAT) AS [IS_CURR_output]
	FROM [LDG].[SEGMENTS]
	)
	,mySourceWithFlags
AS (
	SELECT *
		,CASE 
			WHEN [COB_output] IS NULL
				AND [COB] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [COB_error]
		,CASE 
			WHEN [BRANCH_CODE_output] IS NULL
				AND [BRANCH_CODE] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [BRANCH_CODE_error]
		,CASE 
			WHEN [LEDGER_CODE_output] IS NULL
				AND [LEDGER_CODE] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [LEDGER_CODE_error]
		,CASE 
			WHEN [SEGMENT_output] IS NULL
				AND [SEGMENT] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [SEGMENT_error]
		,CASE 
			WHEN [SUB_SEGMENT_output] IS NULL
				AND [SUB_SEGMENT] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [SUB_SEGMENT_error]
		,CASE 
			WHEN [SEGMENT_LV_1_output] IS NULL
				AND [SEGMENT_LV_1] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [SEGMENT_LV_1_error]
		,CASE 
			WHEN [SEGMENT_LV_2_output] IS NULL
				AND [SEGMENT_LV_2] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [SEGMENT_LV_2_error]
		,CASE 
			WHEN [SEGMENT_LV_3_output] IS NULL
				AND [SEGMENT_LV_3] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [SEGMENT_LV_3_error]
		,CASE 
			WHEN [START_DATE_output] IS NULL
				AND [START_DATE] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [START_DATE_error]
		,CASE 
			WHEN [END_DATE_output] IS NULL
				AND [END_DATE] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [END_DATE_error]
		,CASE 
			WHEN [TIME_STAMP_output] IS NULL
				AND [TIME_STAMP] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [TIME_STAMP_error]
		,CASE 
			WHEN [IS_CURR_output] IS NULL
				AND [IS_CURR] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [IS_CURR_error]
	FROM mysource
	)
SELECT *
	,[COB_error] + [BRANCH_CODE_error] + [LEDGER_CODE_error] + [SEGMENT_error] + [SUB_SEGMENT_error] + [SEGMENT_LV_1_error] + [SEGMENT_LV_2_error] 
	+ [SEGMENT_LV_3_error] + [START_DATE_error] + [END_DATE_error] + [TIME_STAMP_error] + [IS_CURR_error] AS [$sq_transformation_error_count]
FROM mySourceWithFlags;