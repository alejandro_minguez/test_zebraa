﻿




CREATE VIEW  [VAL].[LDG_RES_CLASS_DEF$CAST] AS

with mysource as ( 
	select *,  
		try_cast([RESERVING_CLASS_CODE] as varchar(50)) as [RESERVING_CLASS_CODE_output], 
		try_cast([RESERVING_CLASS_NAME] as varchar(100)) as [RESERVING_CLASS_NAME_output], 
		try_cast([LEVEL_1_CATALOG_NAME] as varchar(100)) as [LEVEL_1_CATALOG_NAME_output], 
		try_cast([LEVEL_1_FILTER_TYPE] as varchar(20)) as [LEVEL_1_FILTER_TYPE_output], 
		try_cast([LEVEL_1_VALUES] as varchar(150)) as [LEVEL_1_VALUES_output], 
		try_cast([LEVEL_2_CATALOG_NAME] as varchar(100)) as [LEVEL_2_CATALOG_NAME_output], 
		try_cast([LEVEL_2_FILTER_TYPE] as varchar(20)) as [LEVEL_2_FILTER_TYPE_output], 
		try_cast([LEVEL_2_VALUES] as varchar(150)) as [LEVEL_2_VALUES_output], 
		try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
		try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output] 
	from  [SEG].[LDG_RES_CLASS_DEF]
    WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos 

,mySourceWithFlags as ( 
	select *, 
		case when  [RESERVING_CLASS_CODE_output] is null and [RESERVING_CLASS_CODE] is not null then 1 else 0 end as [RESERVING_CLASS_CODE_error], 
		case when  [RESERVING_CLASS_NAME_output] is null and [RESERVING_CLASS_NAME] is not null then 1 else 0 end as [RESERVING_CLASS_NAME_error], 
		case when  [LEVEL_1_CATALOG_NAME_output] is null and [LEVEL_1_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_1_CATALOG_NAME_error], 
		case when  [LEVEL_1_FILTER_TYPE_output] is null and [LEVEL_1_FILTER_TYPE] is not null then 1 else 0 end as [LEVEL_1_FILTER_TYPE_error], 
		case when  [LEVEL_1_VALUES_output] is null and [LEVEL_1_VALUES] is not null then 1 else 0 end as [LEVEL_1_VALUES_error], 
		case when  [LEVEL_2_CATALOG_NAME_output] is null and [LEVEL_2_CATALOG_NAME] is not null then 1 else 0 end as [LEVEL_2_CATALOG_NAME_error], 
		case when  [LEVEL_2_FILTER_TYPE_output] is null and [LEVEL_2_FILTER_TYPE] is not null then 1 else 0 end as [LEVEL_2_FILTER_TYPE_error], 
		case when  [LEVEL_2_VALUES_output] is null and [LEVEL_2_VALUES] is not null then 1 else 0 end as [LEVEL_2_VALUES_error], 
		case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
		case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
	from mysource) 

select * , [RESERVING_CLASS_CODE_error]+ [RESERVING_CLASS_NAME_error]+ [LEVEL_1_CATALOG_NAME_error]+ [LEVEL_1_FILTER_TYPE_error]+ [LEVEL_1_VALUES_error]+ [LEVEL_2_CATALOG_NAME_error]+ [LEVEL_2_FILTER_TYPE_error]+ [LEVEL_2_VALUES_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;