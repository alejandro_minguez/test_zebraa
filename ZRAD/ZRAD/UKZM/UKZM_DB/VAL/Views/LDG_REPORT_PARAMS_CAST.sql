﻿



CREATE VIEW  [VAL].[LDG_REPORT_PARAMS$CAST] AS

 with mysource as ( 
 select *,  
 	try_cast(TECH_PARAMETER_CLASS as varchar(100)) as TECH_PARAMETER_CLASS_output,
	try_cast(TECH_PARAMETER_CODE  as varchar(100)) as TECH_PARAMETER_CODE_output,
	try_cast([PARAMETER] as varchar(70)) as [PARAMETER_output], 
	try_cast([VALUE] as varchar(20)) as [VALUE_output], 
	try_cast([DATEVALID_FROM] as date) as [DATEVALID_FROM_output], 
	try_cast([DATEVALID_TO] as date) as [DATEVALID_TO_output]
 from  [SEG].[LDG_REPORT_PARAMS]
  WHERE [ACTIVE] = 1)  -- Cogemos solo los registros activos),
 
 ,mySourceWithFlags as ( 
 select *,
 	case when  TECH_PARAMETER_CLASS_output is null and TECH_PARAMETER_CLASS is not null then 1 else 0 end as TECH_PARAMETER_CLASS_error, 
	case when  TECH_PARAMETER_CODE_output is null and TECH_PARAMETER_CODE is not null then 1 else 0 end as TECH_PARAMETER_CODE_error, 
	case when  [PARAMETER_output] is null and [PARAMETER] is not null then 1 else 0 end as [PARAMETER_error], 
	case when  [VALUE_output] is null and [VALUE] is not null then 1 else 0 end as [VALUE_error], 
	case when  [DATEVALID_FROM_output] is null and [DATEVALID_FROM] is not null then 1 else 0 end as [DATEVALID_FROM_error], 
	case when  [DATEVALID_TO_output] is null and [DATEVALID_TO] is not null then 1 else 0 end as [DATEVALID_TO_error] 
 from mysource) 
 
 select * , TECH_PARAMETER_CLASS_error+TECH_PARAMETER_CODE_error+[PARAMETER_error]+ [VALUE_error]+ [DATEVALID_FROM_error]+ [DATEVALID_TO_error] as [$sq_transformation_error_count]  from mySourceWithFlags;