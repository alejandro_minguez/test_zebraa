﻿CREATE TABLE [TST].[VPM_445_01_RS08_ChkColumn] (
    [TableName1]     VARCHAR (100) NULL,
    [TableName2]     VARCHAR (100) NULL,
    [MatchKey]       VARCHAR (900) NULL,
    [FieldName]      VARCHAR (100) NULL,
    [FieldValue1]    VARCHAR (100) NULL,
    [FieldValue2]    VARCHAR (100) NULL,
    [FieldValueDiff] FLOAT (53)    NULL,
    [MEASURE]        VARCHAR (100) NULL,
    [LOB_GROUP]      VARCHAR (50)  NULL,
    [MOT_COVERAGE2]  VARCHAR (50)  NULL,
    [Capped_Excess]  VARCHAR (15)  NULL,
    [ACC_YR]         DATE          NULL,
    [ACC_QTR]        DATE          NULL,
    [DQ]             SMALLINT      NULL,
    [DMQ]            SMALLINT      NULL
);

