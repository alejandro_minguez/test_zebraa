﻿CREATE TABLE [dbo].[SUB_LINE_BUSINESS_DIMENSION] (
    [SUB_LINE_BUSINESS_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [SUB_LINE_BUSINESS_CODE]              VARCHAR (50)  NOT NULL,
    [SUB_LINE_BUSINESS_NAME]              VARCHAR (100) NOT NULL,
    [SUB_LINE_BUSINESS_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [LINE_BUSINESS_IDENTIFIER]            NUMERIC (12)  NOT NULL,
    [RECORD_INCLUSION_DATE]               DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                  DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]            NUMERIC (12)  NOT NULL
);

