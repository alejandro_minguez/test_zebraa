﻿CREATE TABLE [dbo].[All_UK_ZM] (
    [Excel Sheet]  NVARCHAR (255) NULL,
    [MEASURE]      NVARCHAR (255) NULL,
    [LOB_GROUP]    NVARCHAR (255) NULL,
    [ACCYR_LONG]   DATETIME       NULL,
    [ACC_QTR_LONG] DATETIME       NULL,
    [DISEASE]      NVARCHAR (255) NULL,
    [PL_INJURY]    NVARCHAR (255) NULL,
    [DQ]           INT            NULL,
    [DMQ]          INT            NULL,
    [VARIABLE]     INT            NULL
);

