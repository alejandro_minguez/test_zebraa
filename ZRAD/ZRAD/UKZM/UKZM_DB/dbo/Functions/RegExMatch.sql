﻿CREATE FUNCTION [dbo].[RegExMatch]
(@Pattern NVARCHAR (4000), @Input NVARCHAR (MAX), @Options INT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExMatch]

