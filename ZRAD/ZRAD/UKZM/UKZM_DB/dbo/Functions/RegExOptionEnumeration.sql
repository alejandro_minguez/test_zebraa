﻿CREATE FUNCTION [dbo].[RegExOptionEnumeration]
(@IgnoreCase BIT, @MultiLine BIT, @ExplicitCapture BIT, @Compiled BIT, @SingleLine BIT, @IgnorePatternWhitespace BIT, @RightToLeft BIT, @ECMAScript BIT, @CultureInvariant BIT)
RETURNS INT
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExOptionEnumeration]

