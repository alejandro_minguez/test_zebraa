﻿<#@ template language="C#" #>
<#@ import namespace="System.Data" #>
<#@ import namespace="System.Collections" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ include file="Connections.biml" #>

<# 
string qrySourceViews = "select source_object_name, destination_table_name, load_pattern_id, active_for_load,HelperId, 'HLP'+ cast(HelperId as varchar) HelperName,active_for_creation, primary_key_columns from [md].[extract_phase_info] where source_type_id = 'VIW' and active_for_creation='Y' and load_pattern_id='DMH' and differential_column_name is null";
DataTable tblSourceViews = ExternalDataAccess.GetDataTable(connMD, qrySourceViews);

bool autoFlushHistory = true;
#>

<Biml xmlns="http://schemas.varigence.com/biml.xsd">
	<Connections>
		<OleDbConnection Name="MD" ConnectionString="<#=connMD#>" CreateInProject="true" />
		<#  int helpercounter=1;
		 foreach(string s in connHLP)
		{#>
        <OleDbConnection Name="HLP<#=helpercounter.ToString()#>" ConnectionString="<#=s#>" CreateInProject="true" />
		<# helpercounter=helpercounter+1; }#>
        <OleDbConnection Name="STG" ConnectionString="<#=connSTG#>" CreateInProject="true" />
        <OleDbConnection Name="LOG" ConnectionString="<#=connLOG#>" CreateInProject="true" />
    </Connections>
    <Packages>
    <!--
	Load Packages
    -->
    <# foreach (DataRow row in tblSourceViews.Rows) { #>
		
	<#	
		List<String> lstPrimaryKeyColumnsList = row["primary_key_columns"].ToString().Split(',').Select(i => i.Trim()).ToList<string>();
	
		string inList = string.Join(",", lstPrimaryKeyColumnsList.Select(i => "'" + i + "'").ToArray());

		string qrySourceViewNonKeyColumns = "SELECT * FROM sys.columns WHERE [object_id] = OBJECT_ID('bi." + row["source_object_name"] + "') AND name NOT IN (" + inList + ")";
		DataTable tblSourceViewNonKeyColumns = ExternalDataAccess.GetDataTable(connHLP[int.Parse(row["HelperId"].ToString())-1], qrySourceViewNonKeyColumns);
		
		List<String> lstSourceViewNonKeyColumns = new List<String>();
		foreach(DataRow col in tblSourceViewNonKeyColumns.Rows) 
		{
			lstSourceViewNonKeyColumns.Add(col["name"].ToString());
        }
	#>
    
    <Package Name="Load_STG_<#=row["load_pattern_id"]#>_<#=row["destination_table_name"]#>" ConstraintMode="Parallel" ProtectionLevel="EncryptSensitiveWithUserKey">
    	<Connections>
		<OleDbConnection Name="MD" ConnectionString="<#=connMD#>" CreateInProject="true" />
		<#   helpercounter=1;
		 foreach(string s2 in connHLP)
		{#>
        <OleDbConnection Name="HLP<#=helpercounter.ToString()#>" ConnectionString="<#=s2#>" CreateInProject="true" />
		<# helpercounter=helpercounter+1; }#>
        <OleDbConnection Name="STG" ConnectionString="<#=connSTG#>" CreateInProject="true" />
        <OleDbConnection Name="LOG" ConnectionString="<#=connLOG#>" CreateInProject="true" />
    </Connections>
        <Variables>
            <Variable Name="LoadedRows" DataType="Int32">0</Variable>
            <Variable Name="InsertedRows" DataType="Int32">0</Variable>
            <Variable Name="UpdatedRows" DataType="Int32">0</Variable>
            <Variable Name="DeletedRows" DataType="Int32">0</Variable>
            <Variable Name="IsActive" DataType="String">N</Variable>
            <Variable Name="LogRowId" DataType="Int32">0</Variable>
			<Variable Name="LastLoadedRowValue" DataType="Int32">0</Variable>
			<Variable Name="ServerExecutionID" DataType="Int64" Namespace="User">0</Variable> 
			
			<Variable Name="XMLCS" DataType="String" EvaluateAsExpression="true">&quot;&lt;inserted_rows&gt;&quot; + (DT_WSTR, 9)@[User::InsertedRows] + &quot;&lt;/inserted_rows&gt;&lt;updated_rows&gt;&quot; + (DT_WSTR, 9)@[User::UpdatedRows] + &quot;&lt;/updated_rows&gt;&lt;deleted_rows&gt;&quot; + (DT_WSTR, 9)@[User::DeletedRows] + &quot;&lt;/deleted_rows&gt;&quot;</Variable>
        </Variables>
        <Tasks>
            <ExecuteSQL Name="Check Load Active" ConnectionName="MD" ResultSet="SingleRow">
                <DirectInput>SELECT active_for_load AS active, vs_execution_id = case when ? = 0 then -1*cast(replace(replace(replace(convert(varchar(100),getdate(),120),'-',''),':',''),' ','') as bigint) else ? end FROM md.[extract_phase_info] WHERE source_object_name = '<#=row["source_object_name"]#>';</DirectInput>
				<Parameters>
                  <Parameter Name="@ExecutionId" VariableName="System.ServerExecutionID" DataType="Int64"></Parameter>
				  <Parameter Name="@ExecutionId2" VariableName="System.ServerExecutionID" DataType="Int64"></Parameter>
               </Parameters>
                <Results>
                    <Result Name="active" VariableName="User.IsActive"/>
					<Result Name="vs_execution_id" VariableName="User.ServerExecutionID"/>
                </Results>
            </ExecuteSQL>							
            <ExecuteSQL Name="Set Load Start" ConnectionName="LOG" ResultSet="SingleRow">
                <PrecedenceConstraints>
                    <Inputs>
                        <Input OutputPathName="Check Load Active.Output" EvaluationOperation="ExpressionAndConstraint" EvaluationValue="Success" Expression='@[User::IsActive] == "Y"'/>
                    </Inputs>
                </PrecedenceConstraints>
                <DirectInput>
                    DECLARE @rowId INT;
			        EXEC @rowId = [log].stp_etl_table_load_info_set_start 'stg', '<#=row["destination_table_name"]#>', 'I', ?;
			        SELECT CAST(@rowId AS INT) AS RowId
		        </DirectInput>
				<Parameters>
                	<Parameter Name="@serverExecutionId" VariableName="User.ServerExecutionID" DataType="Int64" Direction="Input"/>
            	</Parameters>
                <Results>
                    <Result Name="RowId" VariableName="User.ServerExecutionID" />
                </Results>
            </ExecuteSQL>
            <Container ConstraintMode="Linear" Name="Load Data">
                <PrecedenceConstraints>
                    <Inputs>
                        <Input OutputPathName="Set Load Start.Output" EvaluationOperation="Constraint" EvaluationValue="Success"/>
                    </Inputs>
                </PrecedenceConstraints>
                <Tasks>
					<ExecuteSQL Name="Clean Changes Table" ConnectionName="STG" Disabled="<#= (!autoFlushHistory).ToString().ToLower() #>">
		            	<DirectInput>TRUNCATE TABLE stg.<#=row["destination_table_name"]#>$Changes;</DirectInput>
		            </ExecuteSQL>
                    <Dataflow Name="Load Staging Table">
                        <Transformations>
                            <OleDbSource Name="HLP" ConnectionName="<#=row["HelperName"].ToString()#>">
                                <DirectInput>SELECT * FROM bi.<#=row["source_object_name"]#> ORDER BY <#=row["primary_key_columns"]#> OPTION (RECOMPILE);</DirectInput>							
								<Columns>
									<# int s = 1; #>
									<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
									<Column SourceColumn="<#=pkc#>" TargetColumn="<#=pkc#>" SortKeyPosition="<#=s#>" IsUsed="true" />
									<# s+= 1; #>
									<# } #>
                                </Columns>
								<DataflowOverrides>
									<OutputPath OutputPathName="Output">
										<Properties>
											<Property PropertyName="IsSorted">True</Property>
																			</Properties>
									</OutputPath>
                                </DataflowOverrides>
                            </OleDbSource>
							<OleDbSource Name="STG - SRC" ConnectionName="STG">
                                <DirectInput>SELECT <#=row["primary_key_columns"]#>, [$sq_row_hash] FROM stg.<#=row["destination_table_name"]#> ORDER BY <#=row["primary_key_columns"]#> OPTION (RECOMPILE);</DirectInput>							
								<Columns>
<# s = 1; #>
<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
<Column SourceColumn="<#=pkc#>" TargetColumn="stg_<#=pkc#>" SortKeyPosition="<#=s#>" IsUsed="true" />
<# s+= 1; #>
<# } #>
                                </Columns>
								<DataflowOverrides>
<OutputPath OutputPathName="Output">
	<Properties>
		<Property PropertyName="IsSorted">True</Property>
                                        </Properties>
</OutputPath>
                                </DataflowOverrides>								
							</OleDbSource>
							<CustomComponent Name="Calculate Row Hash" ComponentTypeName="Martin.SQLServer.Dts.MultipleHash, MultipleHash2014, Version=1.0.0.0, Culture=neutral, PublicKeyToken=51c551904274ab44">
				            <CustomProperties>
				                <CustomProperty Name="MultipleThreads" DataType="Int32" TypeConverter="Martin.SQLServer.Dts.MultipleHash+MultipleThread, MultipleHash2014, Version=1.0.0.0, Culture=neutral, PublicKeyToken=51c551904274ab44">0</CustomProperty>
				                <CustomProperty Name="SafeNullHandling" DataType="Int32" TypeConverter="Martin.SQLServer.Dts.MultipleHash+SafeNullHandling, MultipleHash2014, Version=1.0.0.0, Culture=neutral, PublicKeyToken=51c551904274ab44">1</CustomProperty>
							    <CustomProperty Name="IncludeMillsecond" DataType="Int32" TypeConverter="Martin.SQLServer.Dts.MultipleHash+SafeNullHandling, MultipleHash2014, Version=1.0.0.0, Culture=neutral, PublicKeyToken=51c551904274ab44">1</CustomProperty>								
				            </CustomProperties>
              				<InputPaths>
                				<InputPath Identifier="Input" OutputPathName="HLP.Output">
                  					<InputColumns>
	<# foreach(string colName in lstSourceViewNonKeyColumns) { #>
					                    <InputColumn SourceColumn="<#=colName#>" />
	<# } #>
                  					</InputColumns>
                				</InputPath>
				            </InputPaths>
				            <OutputPaths>
				                <OutputPath Name="HashedOutput" SynchronousInput="Input">
				                  <OutputColumns>
				                    <OutputColumn Name="$sq_row_hash" DataType="Binary" Length="8">
				                      <CustomProperties>
				                        <Property Name="HashType" DataType="Int32" TypeConverter="Martin.SQLServer.Dts.MultipleHash+HashTypeEnumerator, MultipleHash2012, Version=1.0.0.0, Culture=neutral, PublicKeyToken=51c551904274ab44">10</Property>
				                        <Property Name="InputColumnLineageIDs" ContainsId="true" DataType="String" TranslateValueToLineageId="true" LineageIdListSeparator=","><#=string.Join(",", lstSourceViewNonKeyColumns.ToArray())#></Property>
				                      </CustomProperties>
				                    </OutputColumn>
				                  </OutputColumns>
				                </OutputPath>
				              </OutputPaths>
				            </CustomComponent>
							<MergeJoin Name="Find Differences" JoinType="FullOuterJoin">
								<JoinKeys>
<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
<JoinKey LeftColumn="<#=pkc#>" RightColumn="stg_<#=pkc#>" />
<# } #>			
                                </JoinKeys>
								<LeftInputPath OutputPathName="Calculate Row Hash.HashedOutput" />
								<RightInputPath OutputPathName="STG - SRC.Output">
<Columns>
	<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
	<Column SourceColumn="stg_<#=pkc#>" TargetColumn="stg_<#=pkc#>" IsUsed="true" />	
	<# } #>
	<Column SourceColumn="$sq_row_hash" TargetColumn="$stg_sq_row_hash" IsUsed="true" />
                                    </Columns>
                                </RightInputPath>							
                            </MergeJoin>
							<ConditionalSplit Name="Split Changes">
								<InputPath OutputPathName="Find Differences.Output" />
								<OutputPaths>
<OutputPath Name="Deleted Rows">
	<Expression>ISNULL(<#=lstPrimaryKeyColumnsList[0]#>)</Expression>
                                    </OutputPath>
<OutputPath Name="Inserted Rows">
	<Expression>ISNULL([$stg_sq_row_hash])</Expression>
                                    </OutputPath>
<OutputPath Name="Updated Rows">
	<Expression>[$stg_sq_row_hash] != (DT_I8)[$sq_row_hash]</Expression>
                                    </OutputPath>
                                </OutputPaths>
                            </ConditionalSplit>
                            <RowCount Name="Inserted Count Rows" VariableName="User.InsertedRows">
                                <InputPath OutputPathName="Split Changes.Inserted Rows" />
                            </RowCount>
                            <RowCount Name="Updated Count Rows" VariableName="User.UpdatedRows">
                                <InputPath OutputPathName="Split Changes.Updated Rows" />
                            </RowCount>
                            <RowCount Name="Deleted Count Rows" VariableName="User.DeletedRows">
                                <InputPath OutputPathName="Split Changes.Deleted Rows" />
                            </RowCount>
							<DerivedColumns Name="Insert Operation">
								<InputPath OutputPathName="Inserted Count Rows.Output" />
								<Columns>
<Column Name="$sq_operation_id" DataType="Byte">1</Column>
<Column Name="$sq_execution_id" DataType="Int64">@[User::ServerExecutionID]</Column>
                                </Columns>
                            </DerivedColumns>				
							<DerivedColumns Name="Update Operation">
								<InputPath OutputPathName="Updated Count Rows.Output" />
								<Columns>
<Column Name="$sq_operation_id" DataType="Byte">3</Column>
<Column Name="$sq_execution_id" DataType="Int64">@[User::ServerExecutionID]</Column>

                                </Columns>
                            </DerivedColumns>				
							<DerivedColumns Name="Delete Operation">
								<InputPath OutputPathName="Deleted Count Rows.Output" />
								<Columns>
<Column Name="$sq_operation_id" DataType="Byte">4</Column>
<Column Name="$sq_execution_id" DataType="Int64">@[User::ServerExecutionID]</Column>
                                </Columns>
                            </DerivedColumns>				
							<UnionAll Name="Union All">
								<InputPaths>
<InputPath OutputPathName="Insert Operation.Output">
	<Columns>
		<Column SourceColumn="$stg_sq_row_hash" TargetColumn="$stg_sq_row_hash" IsUsed="false" />	
		<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
		<Column SourceColumn="stg_<#=pkc#>" TargetColumn="stg_<#=pkc#>" IsUsed="false"/>
		<# } #>		
	</Columns>
                                    </InputPath>
<InputPath OutputPathName="Update Operation.Output">
	<Columns>
		<Column SourceColumn="$stg_sq_row_hash" TargetColumn="$stg_sq_row_hash" IsUsed="false" />	
		<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
		<Column SourceColumn="stg_<#=pkc#>" TargetColumn="stg_<#=pkc#>" IsUsed="false"/>
		<# } #>		
	</Columns>
                                    </InputPath>
<InputPath OutputPathName="Delete Operation.Output">
	<Columns>
		<Column SourceColumn="$stg_sq_row_hash" TargetColumn="$stg_sq_row_hash" IsUsed="false" />	
		<# foreach (string pkc in lstPrimaryKeyColumnsList) { #>
		<Column SourceColumn="stg_<#=pkc#>" TargetColumn="<#=pkc#>" />
		<Column SourceColumn="<#=pkc#>" TargetColumn="stg_<#=pkc#>" IsUsed="false" />
		<# } #>		
                                        </Columns>	
                                    </InputPath>
                                </InputPaths>							
							</UnionAll>
							<OleDbDestination Name="STG - Changes" ConnectionName="STG" UseFastLoadIfAvailable="true" MaximumInsertCommitSize="2147483647" FastLoadOptions="ORDER([$sq_execution_id],<#=row["primary_key_columns"]#>)">
								<InputPath OutputPathName="Union All.Output" />
								<ExternalTableOutput Table="stg.<#=row["destination_table_name"]#>$Changes" />
                            </OleDbDestination>
						</Transformations>
                    </Dataflow>
					<Dataflow Name="Insert Inserted Rows" DelayValidation="true">
						<Transformations>
							<OleDbSource Name="STG - Changes" ConnectionName="STG">
								<DirectInput>
SELECT * FROM stg.<#=row["destination_table_name"]#>$Changes WHERE [$sq_operation_id] = 1 AND [$sq_execution_id] = ?
                                </DirectInput>
								<Parameters>
<Parameter Name="@serverExecutionId" VariableName="User.ServerExecutionID" />
                                </Parameters>								
                            </OleDbSource>
							<OleDbDestination Name="STG" ConnectionName="STG" UseFastLoadIfAvailable="true" MaximumInsertCommitSize="2147483647">
								<InputPath OutputPathName="STG - Changes.Output" />
								<ExternalTableOutput Table="stg.<#=row["destination_table_name"]#>" />
                            </OleDbDestination>
                        </Transformations>
                    </Dataflow>
					<ExecuteSQL Name="Merge Updated and Deleted" ConnectionName="STG">
						<DirectInput>
							MERGE INTO 
								stg.[<#=row["destination_table_name"]#>] t
							USING
								stg.[<#=row["destination_table_name"]#>$Changes] s
							ON
								<#=string.Join(" AND ", lstPrimaryKeyColumnsList.Select(i => "t.[" + i + "] = s.[" + i + "]").ToArray()) #>
							WHEN MATCHED AND [s].[$sq_operation_id] = 3 THEN
								UPDATE SET
<#=string.Join(", ", lstSourceViewNonKeyColumns.Select(i => "t.[" + i + "] = s.[" + i + "]").ToArray()) #>
,t.[$sq_execution_id] = s.[$sq_execution_id]
,t.[$sq_row_hash] = s.[$sq_row_hash]
							WHEN MATCHED AND [s].[$sq_operation_id] = 4 THEN
								DELETE
							;
                        </DirectInput>
                    </ExecuteSQL>
                </Tasks>
            </Container>
            <ExecuteSQL Name="Set Load End Success" ConnectionName="LOG">
			<PrecedenceConstraints>
				<Inputs>
					<Input OutputPathName="Load Data.Output" EvaluationOperation="Constraint" EvaluationValue="Success"/>
				</Inputs>
			</PrecedenceConstraints>
			<DirectInput>EXEC [log].stp_etl_table_load_info_set_end_cs ?, ?, 'S';</DirectInput>
			<Parameters>
				<Parameter Name="@rowId" VariableName="User.LogRowId" DataType="Int32" Direction="Input" />
				<Parameter Name="@xcs" VariableName="User.XMLCS" DataType="String" Direction="Input" />
			</Parameters>
		</ExecuteSQL>
		<ExecuteSQL Name="Set Load End Failure" ConnectionName="LOG">
			<PrecedenceConstraints>
				<Inputs>
					<Input OutputPathName="Load Data.Output" EvaluationOperation="Constraint" EvaluationValue="Failure"/>
				</Inputs>
			</PrecedenceConstraints>
			<DirectInput>EXEC [log].stp_etl_table_load_info_set_end ?, ?, 'F';</DirectInput>
			<Parameters>
				<Parameter Name="@rowId" VariableName="User.LogRowId" DataType="Int32" Direction="Input" />
				<Parameter Name="@rows" VariableName="User.LoadedRows" DataType="Int32" Direction="Input" />
			</Parameters>
		</ExecuteSQL>
        </Tasks>
    </Package>
    <# } #> 
    </Packages>
</Biml>