﻿CREATE VIEW LDG.RES_CLAIM_SIZE_FLG$final
AS
WITH mysource
AS (
	SELECT *
		,try_cast([ReservingClassCode] AS VARCHAR(9)) AS [ReservingClassCode_output]
		,try_cast([ReservingClassName] AS VARCHAR(11)) AS [ReservingClassName_output]
		,try_cast([LCSplit] AS FLOAT) AS [LCSplit_output]
		,try_cast([LCCurrency] AS VARCHAR(3)) AS [LCCurrency_output]
		,try_cast([LC_use] AS VARCHAR(3)) AS [LC_use_output]
		,try_cast([MCSplit] AS FLOAT) AS [MCSplit_output]
		,try_cast([MCCurrency] AS VARCHAR(3)) AS [MCCurrency_output]
		,try_cast([MC_use] AS VARCHAR(2)) AS [MC_use_output]
		,try_cast([ATTSplit] AS VARCHAR(1)) AS [ATTSplit_output]
		,try_cast([ATTCurrency] AS VARCHAR(3)) AS [ATTCurrency_output]
		,try_cast([ATT_Use] AS VARCHAR(3)) AS [ATT_Use_output]
		,try_cast([DateValidFrom] AS DATE) AS [DateValidFrom_output]
		,try_cast([DateValidTo] AS DATE) AS [DateValidTo_output]
	FROM [LDG].[RES_CLAIM_SIZE_FLG]
	)
	,mySourceWithFlags
AS (
	SELECT *
		,CASE 
			WHEN [ReservingClassCode_output] IS NULL
				AND [ReservingClassCode] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [ReservingClassCode_error]
		,CASE 
			WHEN [ReservingClassName_output] IS NULL
				AND [ReservingClassName] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [ReservingClassName_error]
		,CASE 
			WHEN [LCSplit_output] IS NULL
				AND [LCSplit] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [LCSplit_error]
		,CASE 
			WHEN [LCCurrency_output] IS NULL
				AND [LCCurrency] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [LCCurrency_error]
		,CASE 
			WHEN [LC_use_output] IS NULL
				AND [LC_use] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [LC_use_error]
		,CASE 
			WHEN [MCSplit_output] IS NULL
				AND [MCSplit] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [MCSplit_error]
		,CASE 
			WHEN [MCCurrency_output] IS NULL
				AND [MCCurrency] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [MCCurrency_error]
		,CASE 
			WHEN [MC_use_output] IS NULL
				AND [MC_use] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [MC_use_error]
		,CASE 
			WHEN [ATTSplit_output] IS NULL
				AND [ATTSplit] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [ATTSplit_error]
		,CASE 
			WHEN [ATTCurrency_output] IS NULL
				AND [ATTCurrency] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [ATTCurrency_error]
		,CASE 
			WHEN [ATT_Use_output] IS NULL
				AND [ATT_Use] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [ATT_Use_error]
		,CASE 
			WHEN [DateValidFrom_output] IS NULL
				AND [DateValidFrom] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [DateValidFrom_error]
		,CASE 
			WHEN [DateValidTo_output] IS NULL
				AND [DateValidTo] IS NOT NULL
				THEN 1
			ELSE 0
			END AS [DateValidTo_error]
	FROM mysource
	)
SELECT *
	,[ReservingClassCode_error] + [ReservingClassName_error] + [LCSplit_error] + [LCCurrency_error] + [LC_use_error] + [MCSplit_error] + [MCCurrency_error] + [MC_use_error] + [ATTSplit_error] + [ATTCurrency_error] + [ATT_Use_error] + [DateValidFrom_error] + [DateValidTo_error] AS [$sq_transformation_error_count]
FROM mySourceWithFlags;