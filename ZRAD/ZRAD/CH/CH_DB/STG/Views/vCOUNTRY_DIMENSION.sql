﻿

CREATE VIEW [STG].[vCOUNTRY_DIMENSION] AS

WITH CTE AS (

SELECT DISTINCT LAND FROM STG.HISTORY WITH (NOLOCK) WHERE LAND !='-3'

)


SELECT 
	  [TEXT_IDENTIFIER] AS [COUNTRY_ANCHOR_IDENTIFIER],
      [TEXT_CODE] AS [COUNTRY_CODE],
      [TEXT_NAME] AS [COUNTRY_NAME],
      [TEXT_DESCRIPTION] AS [COUNTRY_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      [SOURCE_SYSTEM_IDENTIFIER] AS [SOURCE_SYSTEM_IDENTIFIER]
FROM [STG].[TEXT]

UNION ALL

SELECT 
	  ROW_NUMBER() OVER(ORDER BY LAND) AS [COUNTRY_ANCHOR_IDENTIFIER],
      LAND AS [COUNTRY_CODE],
      LAND AS [COUNTRY_NAME],
      LAND AS [COUNTRY_DESCRIPTION],
      GETDATE() AS [RECORD_INCLUSION_DATE],
      GETDATE() AS [RECORD_UPDATE_DATE],
      -1 AS [SOURCE_SYSTEM_IDENTIFIER]
FROM CTE