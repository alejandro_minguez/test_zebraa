﻿



CREATE VIEW [ZQ].[VW_CLAIM_OVERVIEW] 
--WITH SCHEMABINDING
AS
	SELECT C.CLAIM_NUMBER
		, F.CLAIM_DESCRIPTION
		, S.CLAIM_STATUS_CODE AS CLAIM_STATUS
		, CAST (F.LOSS_OCCURRENCE_DATE AS DATE) AS LOSS_OCCURRENCE_DATE
		, F.TOTAL_NET_PAID_AMOUNT AS TOTAL_PAID_NET
		, L.NET_AMOUNT AS LATEST_ESTIMATE
		, CAST(L.MOVEMENT_CLAIM_DATE AS DATE) AS LATEST_EST_DATE
		, CASE F.TOTAL_NET_PAID_AMOUNT
			WHEN 0 THEN 1 ELSE 0 END
			AS NIL_CLAIM_INDICATOR
		, CASE C.CLAIM_EVENT_IDENTIFIER
			WHEN 1 THEN 1 ELSE 0 END
			AS FREEZE_INDICATOR
	FROM [ZRAD].[CLAIM_DIMENSION] C
		, [ZRAD].[CLAIM_FOLDER_FACT] F
		, [ZRAD].[CLAIM_STATUS_DIMENSION] S
		, (SELECT CLAIM_IDENTIFIER, MOVEMENT_CATEGORY_IDENTIFIER, TRANSACTION_TYPE_IDENTIFIER, MOVEMENT_CLAIM_DATE
			, RANK() OVER (PARTITION BY CLAIM_IDENTIFIER ORDER BY MOVEMENT_CLAIM_DATE DESC) AS ORDEN, NET_AMOUNT
			FROM [ZRAD].[CLAIM_MOVEMENT_FACT]) L
	WHERE C.CLAIM_ANCHOR_IDENTIFIER = F.CLAIM_IDENTIFIER
		AND C.CURRENT_CLAIM_STATUS_IDENTIFIER = S.CLAIM_STATUS_ANCHOR_IDENTIFIER
		AND C.CLAIM_ANCHOR_IDENTIFIER = L.CLAIM_IDENTIFIER
		AND L.MOVEMENT_CATEGORY_IDENTIFIER = 1 AND L.ORDEN = 1 AND L.TRANSACTION_TYPE_IDENTIFIER = -2