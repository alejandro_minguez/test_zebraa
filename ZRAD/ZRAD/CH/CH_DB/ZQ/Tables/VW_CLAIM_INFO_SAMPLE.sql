﻿CREATE TABLE [ZQ].[VW_CLAIM_INFO_SAMPLE] (
    [BUCH_MONAT]                   FLOAT (53)      NULL,
    [CLAIM_NUMBER]                 VARCHAR (19)    NULL,
    [CLAIM_DESCRIPTION]            VARCHAR (1)     NOT NULL,
    [CLAIM_STATUS]                 VARCHAR (1)     NULL,
    [POLICI_CODE]                  VARCHAR (23)    NULL,
    [LOSS_OCCURRENCE_DATE]         DATE            NULL,
    [TOTAL_PAID_NET]               INT             NOT NULL,
    [NET_AMOUNT]                   DECIMAL (15, 2) NULL,
    [PAYMENT_DATE]                 DATE            NULL,
    [MOVEMENT_YEAR]                INT             NULL,
    [ACCIDENT_DATE]                DATE            NULL,
    [NOTIFICATION_DATE]            DATE            NULL,
    [UNDERWRITING_DATE]            DATE            NULL,
    [TRANSACTION_TYPE_CODE]        VARCHAR (50)    NOT NULL,
    [MOVEMENT_CATEGORY_CODE]       VARCHAR (50)    NOT NULL,
    [INSURANCE_MOVEMENT_TYPE_CODE] VARCHAR (50)    NOT NULL,
    [NIL_CLAIM_INDICATOR]          INT             NOT NULL
);

