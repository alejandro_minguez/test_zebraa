﻿CREATE TABLE [ZRAD].[PHYSICAL_OBJECT_RELATIONSHIP_DIMENSION] (
    [PHYSICAL_OBJECT_IDENTIFIER]                   NUMERIC (12) NOT NULL,
    [PHYSICAL_OBJECT_RELATIONSHIP_IDENTIFIER]      NUMERIC (12) NOT NULL,
    [RECORD_INCLUSION_DATE]                        DATETIME     NOT NULL,
    [RECORD_UPDATE_DATE]                           DATETIME     NOT NULL,
    [PHYSICAL_OBJECT_RELATIONSHIP_TYPE_IDENTIFIER] NUMERIC (12) NOT NULL,
    CONSTRAINT [PHYSICAL_OBJECT_RELATIONSHIP_DIMENSION_PK] PRIMARY KEY CLUSTERED ([PHYSICAL_OBJECT_IDENTIFIER] ASC, [PHYSICAL_OBJECT_RELATIONSHIP_IDENTIFIER] ASC),
    CONSTRAINT [PHYSICAL_OBJECT_RELATIONSHIP_DIMENSION_PHYSICAL_OBJECT_DIMENSION_FK] FOREIGN KEY ([PHYSICAL_OBJECT_IDENTIFIER]) REFERENCES [ZRAD].[PHYSICAL_OBJECT_DIMENSION] ([PHYSICAL_OBJECT_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PHYSICAL_OBJECT_RELATIONSHIP_DIMENSION_PHYSICAL_OBJECT_DIMENSION_FK1] FOREIGN KEY ([PHYSICAL_OBJECT_RELATIONSHIP_IDENTIFIER]) REFERENCES [ZRAD].[PHYSICAL_OBJECT_DIMENSION] ([PHYSICAL_OBJECT_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PHYSICAL_OBJECT_RELATIONSHIP_DIMENSION_PHYSICAL_OBJECT_RELATIONSHIP_TYPE_DIMENSION_FK] FOREIGN KEY ([PHYSICAL_OBJECT_RELATIONSHIP_TYPE_IDENTIFIER]) REFERENCES [ZRAD].[PHYSICAL_OBJECT_RELATIONSHIP_TYPE_DIMENSION] ([PHYSICAL_OBJECT_RELATIONSHIP_TYPE_ANCHOR_IDENTIFIER])
);

