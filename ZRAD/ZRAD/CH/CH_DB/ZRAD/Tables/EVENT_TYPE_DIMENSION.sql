﻿CREATE TABLE [ZRAD].[EVENT_TYPE_DIMENSION] (
    [EVENT_TYPE_ANCHOR_IDENTIFIER]                  NUMERIC (12)  NOT NULL,
    [EVENT_TYPE_CODE]                               VARCHAR (50)  NOT NULL,
    [EVENT_TYPE_NAME]                               VARCHAR (100) NOT NULL,
    [EVENT_TYPE_DESCRIPTION]                        VARCHAR (300) NOT NULL,
    [CATASTROPHE_INDICATOR_IDENTIFIER]              NUMERIC (3)   NOT NULL,
    [GENERAL_INSURANCE_GLOBAL_INDICATOR_IDENTIFIER] NUMERIC (3)   NOT NULL,
    [RECORD_INCLUSION_DATE]                         DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                            DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]                      NUMERIC (12)  NOT NULL,
    CONSTRAINT [EVENT_TYPE_DIMENSION_PK] PRIMARY KEY CLUSTERED ([EVENT_TYPE_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [EVENT_TYPE_DIMENSION_FLAG_INDICATOR_DIMENSION_FK] FOREIGN KEY ([CATASTROPHE_INDICATOR_IDENTIFIER]) REFERENCES [ZRAD].[FLAG_INDICATOR_DIMENSION] ([FLAG_INDICATOR_ANCHOR_IDENTIFIER]),
    CONSTRAINT [EVENT_TYPE_DIMENSION_FLAG_INDICATOR_DIMENSION_FK1] FOREIGN KEY ([GENERAL_INSURANCE_GLOBAL_INDICATOR_IDENTIFIER]) REFERENCES [ZRAD].[FLAG_INDICATOR_DIMENSION] ([FLAG_INDICATOR_ANCHOR_IDENTIFIER]),
    CONSTRAINT [EVENT_TYPE_DIMENSION_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [EVENT_TYPE_DIMENSION_AK] UNIQUE NONCLUSTERED ([EVENT_TYPE_CODE] ASC)
);

