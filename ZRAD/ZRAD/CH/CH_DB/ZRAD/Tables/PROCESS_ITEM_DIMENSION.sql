﻿CREATE TABLE [ZRAD].[PROCESS_ITEM_DIMENSION] (
    [PROCESS_ITEM_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [PROCESS_ITEM_CODE]              VARCHAR (50)  NOT NULL,
    [PROCESS_ITEM_NAME]              VARCHAR (100) NOT NULL,
    [PROCESS_ITEM_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [PROCESS_TYPE_IDENTIFIER]        NUMERIC (12)  NOT NULL,
    [RECORD_INCLUSION_DATE]          DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]             DATETIME      NOT NULL,
    CONSTRAINT [PROCESS_ITEM_DIMENSION_PK] PRIMARY KEY CLUSTERED ([PROCESS_ITEM_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [PROCESS_ITEM_DIMENSION_PROCESS_TYPE_DIMENSION_FK] FOREIGN KEY ([PROCESS_TYPE_IDENTIFIER]) REFERENCES [ZRAD].[PROCESS_TYPE_DIMENSION] ([PROCESS_TYPE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PROCESS_ITEM_DIMENSION_AK] UNIQUE NONCLUSTERED ([PROCESS_ITEM_CODE] ASC)
);

