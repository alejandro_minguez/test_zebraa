﻿CREATE TABLE [ZRAD].[TIME_DAY_DIMENSION] (
    [TIME_DAY_ANCHOR_IDENTIFIER] NUMERIC (12) NOT NULL,
    [TIME_DAY_DATE]              DATETIME     NOT NULL,
    [TIME_MONTH_IDENTIFIER]      NUMERIC (12) NOT NULL,
    [TIME_QUARTER_IDENTIFIER]    NUMERIC (12) NOT NULL,
    [TIME_SEMESTER_IDENTIFIER]   NUMERIC (12) NOT NULL,
    [TIME_YEAR_IDENTIFIER]       NUMERIC (12) NOT NULL,
    CONSTRAINT [TIME_DAY_DIMENSION_PK] PRIMARY KEY CLUSTERED ([TIME_DAY_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [TIME_DAY_DIMENSION_TIME_MONTH_DIMENSION_FK] FOREIGN KEY ([TIME_MONTH_IDENTIFIER]) REFERENCES [ZRAD].[TIME_MONTH_DIMENSION] ([TIME_MONTH_ANCHOR_IDENTIFIER]),
    CONSTRAINT [TIME_DAY_DIMENSION_TIME_QUARTER_DIMENSION_FK] FOREIGN KEY ([TIME_QUARTER_IDENTIFIER]) REFERENCES [ZRAD].[TIME_QUARTER_DIMENSION] ([TIME_QUARTER_ANCHOR_IDENTIFIER]),
    CONSTRAINT [TIME_DAY_DIMENSION_TIME_SEMESTER_DIMENSION_FK] FOREIGN KEY ([TIME_SEMESTER_IDENTIFIER]) REFERENCES [ZRAD].[TIME_SEMESTER_DIMENSION] ([TIME_SEMESTER_ANCHOR_IDENTIFIER]),
    CONSTRAINT [TIME_DAY_DIMENSION_TIME_YEAR_DIMENSION_FK] FOREIGN KEY ([TIME_YEAR_IDENTIFIER]) REFERENCES [ZRAD].[TIME_YEAR_DIMENSION] ([TIME_YEAR_ANCHOR_IDENTIFIER])
);

