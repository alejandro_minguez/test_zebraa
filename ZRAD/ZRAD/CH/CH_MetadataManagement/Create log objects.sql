SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [log].[etl_table_load_info](
[id] [int] IDENTITY(1,1) NOT NULL,
[schema_name] [sysname] NOT NULL,
[table_name] [sysname] NOT NULL,
[type] [char](1) NOT NULL,
[ssis_server_execution_id] [bigint] NOT NULL,
[start_date] [datetime2](7) NOT NULL,
[end_date] [datetime2](7) NULL,
[status] [char](1) NULL,
[loaded_by] [sysname] NULL CONSTRAINT [DF__loaded_by]  DEFAULT (suser_sname()),
[rows] [bigint] SPARSE  NULL,
[inserted_rows] [int] SPARSE  NULL,
[updated_rows] [int] SPARSE  NULL,
[deleted_rows] [int] SPARSE  NULL,
UpdatedRowst1 int sparse null,
UpdatedRowst1h int sparse null,
UpdatedRowst2 int sparse null,
[$xcs] [xml] COLUMN_SET FOR ALL_SPARSE_COLUMNS  NULL,
PRIMARY KEY CLUSTERED
(
[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [log].[etl_table_load_info]  WITH CHECK ADD  CONSTRAINT [CK__table_load_info__status] CHECK  (([status]='F' OR [status]='S' OR [status]='C'))
GO
ALTER TABLE [log].[etl_table_load_info] CHECK CONSTRAINT [CK__table_load_info__status]
GO
ALTER TABLE [log].[etl_table_load_info]  WITH CHECK ADD  CONSTRAINT [CK__table_load_info__type] CHECK  (([type]='F' OR [type]='I' OR [type]='D' OR [type]='M'))
GO
ALTER TABLE [log].[etl_table_load_info] CHECK CONSTRAINT [CK__table_load_info__type]
GO
/****** Object:  StoredProcedure [log].[stp_etl_table_load_info_set_end]    Script Date: 13/04/2016 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [log].[stp_etl_table_load_info_set_end]
@rowId int,
@rows bigint = null,
@status char(1)
as

update [log].etl_table_load_info
set [rows] = @rows, [end_date] = SYSDATETIME(), [status] = @status
where id = @rowId


GO
/****** Object:  StoredProcedure [log].[stp_etl_table_load_info_set_end_cs]    Script Date: 13/04/2016 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [log].[stp_etl_table_load_info_set_end_cs]
@rowId INT,
@xcs XML,
@status CHAR(1)
AS

UPDATE [log].etl_table_load_info
SET [$xcs] = @xcs, [end_date] = SYSDATETIME(), [status] = @status
WHERE id = @rowId


GO
/****** Object:  StoredProcedure [log].[stp_etl_table_load_info_set_start]    Script Date: 13/04/2016 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [log].[stp_etl_table_load_info_set_start]
@schemaName AS SYSNAME,
@tableName AS SYSNAME,
@type AS CHAR(1),
@serverExecutionId BIGINT
AS

insert into [log].etl_table_load_info
([schema_name], [table_name], [type], [start_date], [ssis_server_execution_id])
values
(@schemaName, @tableName, @type, SYSDATETIME(), @serverExecutionId)

return scope_identity()


GO
