﻿
GO
/****** Object:  Table [md].[Connections]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [md].[Connections](
[connectionId] [int] NOT NULL,
[ConnectionType] [char](3) NOT NULL,
[ConnectionString] [varchar](8000) NULL,
[provider] [nvarchar](1000) NULL,
Description nvarchar(100),
db_schemas nvarchar(100),
CONSTRAINT [pk_connections] PRIMARY KEY CLUSTERED
(
[connectionId] ASC,
[ConnectionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
go
/****** Object:  Table [md].[dimensions_load_phase_info]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [md].[dimensions_load_phase_info](
source_schema sysname not null,
[source_table_name] [sysname] NOT NULL,
[destination_table_name] [sysname] NOT NULL,
[active_for_creation] [char](1) NULL,
[active_for_load] [char](1) NULL,
[primary_key_columns] [nvarchar](1000) SPARSE  NULL,
[incremental_column_name] [sysname] SPARSE  NULL,
CONSTRAINT [PK_dimension_load_phase_info] PRIMARY KEY CLUSTERED
(source_schema,
[source_table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [md].[dimensions_load_phase_info_columns]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[dimensions_load_phase_info_columns](
source_schema sysname not null,
[source_table_name] [sysname] NOT NULL,
[source_column_name] [sysname] NOT NULL,
[changeType] [tinyint] NOT NULL,
ordinal int 
CONSTRAINT [pk_dimensions_load_phase_info] PRIMARY KEY CLUSTERED
(
source_schema asc,
[source_table_name] ASC,
[source_column_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [md].[extract_phase_info]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [md].[extract_phase_info](
source_object_schema sysname not null,
[source_object_name] [sysname] NOT NULL,
destination_table_Schema sysname not null,
[destination_table_name] [sysname] NOT NULL,
[load_pattern_id] [varchar](3) NOT NULL,
[active_for_load] [char](1) NOT NULL,
[active_for_creation] [char](1) NOT NULL,
[source_type_id] [char](3) NOT NULL,
[primary_key_columns] sysname NULL,
[incremental_column_name] [sysname] NULL,
[differential_column_name] [sysname] NULL,
[HelperId] [int] NOT NULL,
[Create_SK_in_Staging] [char](1) NOT NULL default 'N',
[OrderGroup] [int] NOT NULL default 1,
Active_for_orquestator char(1) not null default 'Y',
Detect_Deletes char(1) default 'N'
PRIMARY KEY CLUSTERED
(
source_object_schema,
[source_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
SET ANSI_PADDING OFF
GO
/****** Object:  Table [md].[extract_phase_info_columns]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[extract_phase_info_columns](
[source_object_name] [sysname] NOT NULL,
[Column_sk_name] [sysname] NOT NULL,
[lookup_table] [sysname] NOT NULL,
[lookup_columns] [nvarchar](2000) NOT NULL,
[table_columns] [nvarchar](200) NULL,
[alternative_lookup_columns] [nvarchar](2000) NULL,
[alternative_table_columns] [nvarchar](2000) NULL,
[orden] int NULL,
CONSTRAINT [pk_extract_phase_info_columns] PRIMARY KEY CLUSTERED
(
[source_object_name] ASC,
[Column_sk_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [md].[incremental_load_info]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[incremental_load_info](
[source_object_name] [sysname] NOT NULL,
[last_loaded_row_value] [bigint] NULL,
[execution_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED
(
[source_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [md].[load_patterns]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [md].[load_patterns](
[load_pattern_id] [varchar](3) NOT NULL,
[load_pattern] [varchar](100) NULL,
PRIMARY KEY CLUSTERED
(
[load_pattern_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [md].[load_phase_info]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [md].[load_phase_info](
[source_table_name] [sysname] NOT NULL,
[destination_table_name] [sysname] NOT NULL,
[active_for_creation] [char](1) NULL,
[active_for_load] [char](1) NULL,
[primary_key_columns] [nvarchar](1000) SPARSE  NULL,
[incremental_column_name] [sysname] SPARSE  NULL,
CONSTRAINT [PK_load_phase_info] PRIMARY KEY CLUSTERED
(
[source_table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [md].[load_phase_info_columns]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[load_phase_info_columns](
[fact_table_name] [sysname] NOT NULL,
[fact_table_column_name] [sysname] NOT NULL,
[dimension_table_name] [sysname] NOT NULL,
[dimension_table_column_name] [sysname] NOT NULL,
[column_output_name] [sysname] NOT NULL,
[dimension_role_suffix] [sysname] NOT NULL,
CONSTRAINT [pk__fact_table_mapping] PRIMARY KEY CLUSTERED
(
[fact_table_name] ASC,
[fact_table_column_name] ASC,
[dimension_table_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [md].[SCD_Types]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[SCD_Types](
[idType] [tinyint] NOT NULL,
[TypeDescription] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED
(
[idType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [md].[Source_Schema]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [md].[Source_Schema](
[HelperId] [int] NOT NULL,
table_schema_name sysname not null,
[Table_Name] [sysname] NOT NULL,
[Column_Name] [sysname] NOT NULL,
[Destination_Column_Name] [sysname] NOT NULL,
[Data_Type] [sysname] NOT NULL,
[Destination_Data_Type] [sysname] NOT NULL,
ordinal_position int,
CONSTRAINT [pk_Source_Schema] PRIMARY KEY CLUSTERED
(
[HelperId] ASC,
table_schema_name asc,
[Table_Name] ASC,
[Column_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [md].[source_types]    Script Date: 13/04/2016 9:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [md].[source_types](
[source_type_id] [char](3) NOT NULL,
[source_type] [varchar](100) NULL,
PRIMARY KEY CLUSTERED
(
[source_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

INSERT [md].[Connections] ([connectionId], [ConnectionType], [ConnectionString], [provider],db_schemas,Description) VALUES (1, N'DWH', N'Data Source=localhost;Initial Catalog=demoABI_DWH;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;', NULL,'dim,fact',NULL)
GO
INSERT [md].[Connections] ([connectionId], [ConnectionType], [ConnectionString], [provider],db_schemas,Description) VALUES (1, N'HLP', N'Provider=SQLNCLI11.1;Data Source=localhost;Initial Catalog=demoABI_HLP;Integrated Security=SSPI;Auto Translate=False;', NULL,'bi','Primer Helper')
GO
INSERT [md].[Connections] ([connectionId], [ConnectionType], [ConnectionString], [provider],db_schemas,Description) VALUES (1, N'LOG', N'Data Source=localhost;Initial Catalog=demoABI_LOG;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;', NULL,null,null)
GO
INSERT [md].[Connections] ([connectionId], [ConnectionType], [ConnectionString], [provider],db_schemas,Description) VALUES (1, N'STG', N'Data Source=localhost;Initial Catalog=demoABI_STG;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;', NULL,'stg,di',null)
GO

GO
INSERT [md].[load_patterns] ([load_pattern_id], [load_pattern]) VALUES (N'CDC', N'Change Data Capture')
GO
INSERT [md].[load_patterns] ([load_pattern_id], [load_pattern]) VALUES (N'DLH', N'Differential Lookup Hash')
GO
INSERT [md].[load_patterns] ([load_pattern_id], [load_pattern]) VALUES (N'DLT', N'Differential Lookup Timestamp')
GO
INSERT [md].[load_patterns] ([load_pattern_id], [load_pattern]) VALUES (N'DMH', N'Differential Merge Hash')
GO
INSERT [md].[load_patterns] ([load_pattern_id], [load_pattern]) VALUES (N'F', N'Full')
GO
INSERT [md].[SCD_Types] ([idType], [TypeDescription]) VALUES (0, N'1 with history')
GO
INSERT [md].[SCD_Types] ([idType], [TypeDescription]) VALUES (1, N'type 1')
GO
INSERT [md].[SCD_Types] ([idType], [TypeDescription]) VALUES (2, N'type 2')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'CSV', N'Comma Separated File')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'QRY', N'Query')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'STP', N'Stored Procedure')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'TSV', N'Tab Saperated File')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'VIW', N'View')
GO
INSERT [md].[source_types] ([source_type_id], [source_type]) VALUES (N'XLS', N'Excel')
GO
ALTER TABLE [md].[dimensions_load_phase_info] ADD  DEFAULT ('Y') FOR [active_for_creation]
GO
ALTER TABLE [md].[dimensions_load_phase_info] ADD  DEFAULT ('Y') FOR [active_for_load]
GO

ALTER TABLE [md].[load_phase_info] ADD  DEFAULT ('Y') FOR [active_for_creation]
GO
ALTER TABLE [md].[load_phase_info] ADD  DEFAULT ('Y') FOR [active_for_load]
GO
ALTER TABLE [md].[load_phase_info_columns] ADD  CONSTRAINT [df_1]  DEFAULT ('') FOR [dimension_role_suffix]
GO
ALTER TABLE [md].[Source_Schema] ADD  DEFAULT ((1)) FOR [HelperId]
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [fk__load_pattern_id] FOREIGN KEY([load_pattern_id])
REFERENCES [md].[load_patterns] ([load_pattern_id])
GO
ALTER TABLE [md].[extract_phase_info] CHECK CONSTRAINT [fk__load_pattern_id]
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [fk__source_type_id] FOREIGN KEY([source_type_id])
REFERENCES [md].[source_types] ([source_type_id])
GO
ALTER TABLE [md].[extract_phase_info] CHECK CONSTRAINT [fk__source_type_id]
GO
ALTER TABLE [md].[Connections]  WITH CHECK ADD CHECK  (([ConnectionType]='LOG' OR [ConnectionType]='DWH' OR [ConnectionType]='STG' OR [ConnectionType]='HLP'))
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__active_for_creation] CHECK  (([active_for_creation]='N' OR [active_for_creation]='Y'))
GO
ALTER TABLE [md].[extract_phase_info] CHECK CONSTRAINT [ck__active_for_creation]
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__active_for_load] CHECK  (([active_for_load]='N' OR [active_for_load]='Y'))
GO
ALTER TABLE [md].[extract_phase_info] CHECK CONSTRAINT [ck__active_for_load]
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [ck_csis] CHECK  (([Create_SK_in_Staging]='N' OR [Create_SK_in_Staging]='Y'))
GO
ALTER TABLE [md].[extract_phase_info] CHECK CONSTRAINT [ck_csis]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [md].[extract_phase_info_dm](
	[source_object_schema] [sysname] NOT NULL,
	[source_object_name] [sysname] NOT NULL,
	[destination_table_Schema] [sysname] NOT NULL,
	[destination_table_name] [sysname] NOT NULL,
	[load_pattern_id] [varchar](3) NOT NULL,
	[active_for_load] [char](1) NOT NULL,
	[active_for_creation] [char](1) NOT NULL,
	[source_type_id] [char](3) NOT NULL,
	[primary_key_columns] [sysname] NULL,
	[incremental_column_name] [sysname] NULL,
	[differential_column_name] [sysname] NULL,
	[Create_SK_in_Staging] [char](1) NOT NULL DEFAULT ('N'),
	[OrderGroup] [int] NOT NULL DEFAULT ((1)),
	[Active_for_orquestator] [char](1) NOT NULL DEFAULT ('Y'),
PRIMARY KEY CLUSTERED 
(
	[source_object_schema] ASC,
	[source_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [fk__load_pattern_id_dm] FOREIGN KEY([load_pattern_id])
REFERENCES [md].[load_patterns] ([load_pattern_id])
GO

ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [fk__load_pattern_id_dm]
GO

ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [fk__source_type_id_dm] FOREIGN KEY([source_type_id])
REFERENCES [md].[source_types] ([source_type_id])
GO

ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [fk__source_type_id_dm]
GO

ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [ck__active_for_creation_dm] CHECK  (([active_for_creation]='N' OR [active_for_creation]='Y'))
GO

ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [ck__active_for_creation_dm]
GO

ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [ck__active_for_load_dm] CHECK  (([active_for_load]='N' OR [active_for_load]='Y'))
GO

ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [ck__active_for_load_dm]
GO

ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [ck_csis_dm] CHECK  (([Create_SK_in_Staging]='N' OR [Create_SK_in_Staging]='Y'))
GO

ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [ck_csis_dm]
GO
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__Detect_Deletes] CHECK  ((Detect_Deletes='N' OR Detect_Deletes='Y')) 
GO
ALTER TABLE [md].[extract_phase_info_dm] CHECK CONSTRAINT [ck__Detect_Deletes]



SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [md].[Source_Schema_dm](
	[table_schema_name] [sysname] NOT NULL,
	[Table_Name] [sysname] NOT NULL,
	[Column_Name] [sysname] NOT NULL,
	[Destination_Column_Name] [sysname] NOT NULL,
	[Data_Type] [sysname] NOT NULL,
	[Destination_Data_Type] [sysname] NOT NULL,
	[ordinal_position] [int] NULL,
 CONSTRAINT [pk_Source_Schema_Dm] PRIMARY KEY CLUSTERED 
(
	[table_schema_name] ASC,
	[Table_Name] ASC,
	[Column_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
alter table md.extract_phase_info ADD Detect_Deletes char(1) default 'N'
go
ALTER TABLE [md].[extract_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__Detect_Deletes] CHECK  ((Detect_Deletes='N' OR Detect_Deletes='Y')) 
go
alter table md.dimensions_load_phase_info ADD Detect_Deletes char(1) not null default 'N';
go
ALTER TABLE [md].[dimensions_load_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__Detect_Deletes_dim] CHECK  ((Detect_Deletes='N' OR Detect_Deletes='Y')) 
go
alter table md.load_phase_info ADD Detect_Deletes char(1) not null default 'N';
go
ALTER TABLE [md].[load_phase_info]  WITH CHECK ADD  CONSTRAINT [ck__Detect_Deletes_fact] CHECK  ((Detect_Deletes='N' OR Detect_Deletes='Y')) 
go
alter table md.extract_phase_info_dm ADD Detect_Deletes char(1) not null default 'N'
go
ALTER TABLE [md].[extract_phase_info_dm]  WITH CHECK ADD  CONSTRAINT [ck__Detect_Deletes_dm] CHECK  ((Detect_Deletes='N' OR Detect_Deletes='Y')) 
go
alter table md.load_phase_info add snapshotbehaviour varchar(10) not null default 'none' check( snapshotbehaviour in ('yyyy', 'yyyymm' , 'yyyymmdd' ,'yyyyww','none'))
go
alter table md.load_phase_info add snapshotColumnName sysname not null default ''
go
alter table md.load_phase_info_columns add DateForSc2 sysname default ''
go
CREATE TABLE [md].[transformation_phase_info](
	[source_object_schema] [sysname] NOT NULL,
	[source_object_name] [sysname] NOT NULL,
	[load_pattern_id] [varchar](3) NOT NULL,
	[incremental_column_name] [sysname] NULL,
	[OrderGroup] [int] NOT NULL,
	[Active_for_orquestator] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[source_object_schema] ASC,
	[source_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/* versión Nov 2016 */ 
alter table md.Connections add id  uniqueidentifier not null default newid()
alter table [md].[dimensions_load_phase_info] add OrderGroup tinyint not null default 1
alter table [md].[load_phase_info] add OrderGroup tinyint not null default 1
alter table md.load_phase_info add UpdateSkWhenBKChange char(1) not null default 'Y' check(UpdateSkWhenBKChange in ('Y','N') )
go