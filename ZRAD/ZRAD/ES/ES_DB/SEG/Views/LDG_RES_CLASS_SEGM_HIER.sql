﻿




CREATE VIEW [SEG].[LDG_RES_CLASS_SEGM_HIER] 
AS
 SELECT sh.[ReservingClassCode]
      ,sh.[ReservingClassName]
      ,sh.[ReservingSegmentLevel]
      ,sh.ReservingSegmentVar
	  ,sv.[ReservingSegmentName]
      ,[segment_lv_1]
      ,[segment_lv_2]
      ,[segment_lv_3]
      ,[LOB_GROUP]
      ,[LOB_BASE]
      ,[TYPE]
      ,[INJ_IND]
      ,[PERIL_1]
      ,[PERIL_2]
      ,[CAUSE_CODE]
      ,[OTHER_CODE]
      ,[DESCRIPTION]
      ,[FREEZE]
      ,[DefaultValueFlag]
FROM
			[ESDZRAD].[LDG].[RES_SEGMENT_HIER] SH
	 join   [ESDZRAD].[LDG].[RES_SEGMENT_DEF] SV   on ( sh.ReservingSegmentVar = sv.ReservingSegmentVar )
--order by 1,2,3