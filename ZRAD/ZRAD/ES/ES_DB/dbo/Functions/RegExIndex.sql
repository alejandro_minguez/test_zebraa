﻿CREATE FUNCTION [dbo].[RegExIndex]
(@Pattern NVARCHAR (4000), @Input NVARCHAR (MAX), @Options INT)
RETURNS INT
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExIndex]

