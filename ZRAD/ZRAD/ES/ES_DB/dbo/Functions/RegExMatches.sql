﻿CREATE FUNCTION [dbo].[RegExMatches]
(@Pattern NVARCHAR (4000), @Input NVARCHAR (MAX), @Options INT)
RETURNS 
     TABLE (
        [Match]       NVARCHAR (MAX) NULL,
        [MatchIndex]  INT            NULL,
        [MatchLength] INT            NULL)
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExMatches]

