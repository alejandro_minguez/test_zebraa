﻿CREATE FUNCTION [dbo].[RegExReplaceX]
(@Pattern NVARCHAR (4000), @Input NVARCHAR (MAX), @Repacement NVARCHAR (MAX), @Options INT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExReplacex]

