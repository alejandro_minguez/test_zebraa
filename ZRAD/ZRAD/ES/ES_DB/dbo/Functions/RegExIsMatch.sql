﻿CREATE FUNCTION [dbo].[RegExIsMatch]
(@Pattern NVARCHAR (4000), @Input NVARCHAR (MAX), @Options INT)
RETURNS BIT
AS
 EXTERNAL NAME [RegexFunction].[SimpleTalk.Phil.Factor.RegularExpressionFunctions].[RegExIsMatch]

