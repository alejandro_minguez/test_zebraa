﻿CREATE TABLE [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION] (
    [UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER] NUMERIC (12) NOT NULL,
    [UNDERWRITING_POLICY_IDENTIFIER]             NUMERIC (12) NOT NULL,
    [ITEM_POLICY_NUMBER]                         NUMERIC (12) NOT NULL,
    [ITEM_POLICY_TERM_START_DATE]                DATETIME     NOT NULL,
    [ITEM_POLICY_TERM_END_DATE]                  DATETIME     NOT NULL,
    [RECORD_INCLUSION_DATE]                      DATETIME     NOT NULL,
    [RECORD_UPDATE_DATE]                         DATETIME     NOT NULL,
    [POLICY_STATUS_IDENTIFIER]                   NUMERIC (12) NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]                   NUMERIC (12) NOT NULL,
    CONSTRAINT [UNDERWRITING_ITEM_POLICY_DIMENSION_PK] PRIMARY KEY CLUSTERED ([UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [UNDERWRITING_ITEM_POLICY_DIMENSION_POLICY_STATUS_DIMENSION_FK] FOREIGN KEY ([POLICY_STATUS_IDENTIFIER]) REFERENCES [ZRAD].[POLICY_STATUS_DIMENSION] ([POLICY_STATUS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_ITEM_POLICY_DIMENSION_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [UNDERWRITING_ITEM_POLICY_DIMENSION_AK] UNIQUE NONCLUSTERED ([ITEM_POLICY_NUMBER] ASC, [UNDERWRITING_POLICY_IDENTIFIER] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNDERWRITING_ITEM_POLICY_DIMENSION_UNDERWRITING_POLICY_IDENTIFIER_IDX]
    ON [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]([UNDERWRITING_POLICY_IDENTIFIER] ASC, [ITEM_POLICY_NUMBER] ASC);


GO
CREATE NONCLUSTERED INDEX [UNDERWRITING_ITEM_POLICY_DIMENSION_UNDERWRITING_POLICY_DIMENSION_IDX0]
    ON [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]([UNDERWRITING_POLICY_IDENTIFIER] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNDERWRITING_ITEM_POLICY_DIMENSION_UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]([UNDERWRITING_ITEM_POLICY_ANCHOR_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [UNDERWRITING_ITEM_POLICY_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [UNDERWRITING_ITEM_POLICY_DIMENSION_POLICY_STATUS_DIMENSION_IDX0]
    ON [ZRAD].[UNDERWRITING_ITEM_POLICY_DIMENSION]([POLICY_STATUS_IDENTIFIER] ASC);

