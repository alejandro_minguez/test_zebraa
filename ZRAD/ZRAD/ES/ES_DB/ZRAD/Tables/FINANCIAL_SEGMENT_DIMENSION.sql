﻿CREATE TABLE [ZRAD].[FINANCIAL_SEGMENT_DIMENSION] (
    [FINANCIAL_SEGMENT_ANCHOR_IDENTIFIER] NUMERIC (12)  NOT NULL,
    [FINANCIAL_SEGMENT_CODE]              VARCHAR (50)  NOT NULL,
    [FINANCIAL_SEGMENT_NAME]              VARCHAR (100) NOT NULL,
    [FINANCIAL_SEGMENT_DESCRIPTION]       VARCHAR (300) NOT NULL,
    [RECORD_INCLUSION_DATE]               DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]                  DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]            NUMERIC (12)  NOT NULL,
    CONSTRAINT [FINANCIAL_SEGMENT_DIMENSION_PK] PRIMARY KEY CLUSTERED ([FINANCIAL_SEGMENT_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [FINANCIAL_SEGMENT_DIMENSION_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [FINANCIAL_SEGMENT_DIMENSION_AK] UNIQUE NONCLUSTERED ([FINANCIAL_SEGMENT_CODE] ASC)
);


GO
CREATE NONCLUSTERED INDEX [FINANCIAL_SEGMENT_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[FINANCIAL_SEGMENT_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);

