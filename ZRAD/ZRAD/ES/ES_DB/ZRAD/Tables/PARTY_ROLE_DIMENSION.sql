﻿CREATE TABLE [ZRAD].[PARTY_ROLE_DIMENSION] (
    [PARTY_ROLE_ANCHOR_IDENTIFIER]     NUMERIC (12)  NOT NULL,
    [PARTY_ROLE_CODE]                  VARCHAR (50)  NOT NULL,
    [PARTY_ROLE_NAME]                  VARCHAR (100) NOT NULL,
    [PARTY_ROLE_DESCRIPTION]           VARCHAR (300) NOT NULL,
    [PARTY_ROLE_DRIVING_NUMBER]        VARCHAR (100) NOT NULL,
    [PARTY_ROLE_IDENTIFICATION_NUMBER] VARCHAR (100) NOT NULL,
    [PARTY_ROLE_BANK_NUMBER]           VARCHAR (50)  NOT NULL,
    [PARTY_ROLE_BANK_ACCOUNT_NUMBER]   VARCHAR (50)  NOT NULL,
    [PLACE_IDENTIFIER]                 NUMERIC (12)  NOT NULL,
    [PARTY_ROLE_TYPE_IDENTIFIER]       NUMERIC (12)  NOT NULL,
    [PARTY_IDENTIFIER]                 NUMERIC (12)  NOT NULL,
    [BUSINESS_SEGMENT_IDENTIFIER]      NUMERIC (12)  NOT NULL,
    [INDUSTRY_CLASS_IDENTIFIER]        NUMERIC (12)  NOT NULL,
    [RECORD_INCLUSION_DATE]            DATETIME      NOT NULL,
    [RECORD_UPDATE_DATE]               DATETIME      NOT NULL,
    [SOURCE_SYSTEM_IDENTIFIER]         NUMERIC (12)  NOT NULL,
    CONSTRAINT [PARTY_ROLE_DIMENSION_PK] PRIMARY KEY CLUSTERED ([PARTY_ROLE_ANCHOR_IDENTIFIER] ASC),
    CONSTRAINT [PARTY_ROLE_DIMENSION_BUSINESS_SEGMENT_DIMENSION_FK] FOREIGN KEY ([BUSINESS_SEGMENT_IDENTIFIER]) REFERENCES [ZRAD].[BUSINESS_SEGMENT_DIMENSION] ([BUSINESS_SEGMENT_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_INDUSTRY_CLASS_DIMENSION_FK] FOREIGN KEY ([INDUSTRY_CLASS_IDENTIFIER]) REFERENCES [ZRAD].[INDUSTRY_CLASS_DIMENSION] ([INDUSTRY_CLASS_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_PARTY_DIMENSION_FK] FOREIGN KEY ([PARTY_IDENTIFIER]) REFERENCES [ZRAD].[PARTY_DIMENSION] ([PARTY_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_PARTY_ROLE_TYPE_DIMENSION_FK] FOREIGN KEY ([PARTY_ROLE_TYPE_IDENTIFIER]) REFERENCES [ZRAD].[PARTY_ROLE_TYPE_DIMENSION] ([PARTY_ROLE_TYPE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_PLACE_DIMENSION_FK] FOREIGN KEY ([PLACE_IDENTIFIER]) REFERENCES [ZRAD].[PLACE_DIMENSION] ([PLACE_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_SOURCE_SYSTEM_DIMENSION_FK] FOREIGN KEY ([SOURCE_SYSTEM_IDENTIFIER]) REFERENCES [ZRAD].[SOURCE_SYSTEM_DIMENSION] ([SOURCE_SYSTEM_ANCHOR_IDENTIFIER]),
    CONSTRAINT [PARTY_ROLE_DIMENSION_AK] UNIQUE NONCLUSTERED ([PARTY_ROLE_CODE] ASC)
);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_SOURCE_SYSTEM_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([SOURCE_SYSTEM_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_PLACE_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([PLACE_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_PARTY_ROLE_TYPE_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([PARTY_ROLE_TYPE_IDENTIFIER] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_PARTY_ROLE_ANCHOR_IDENTIFIER_IDX]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([PARTY_ROLE_ANCHOR_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_PARTY_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([PARTY_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_INDUSTRY_CLASS_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([INDUSTRY_CLASS_IDENTIFIER] ASC);


GO
CREATE NONCLUSTERED INDEX [PARTY_ROLE_DIMENSION_BUSINESS_SEGMENT_DIMENSION_IDX0]
    ON [ZRAD].[PARTY_ROLE_DIMENSION]([BUSINESS_SEGMENT_IDENTIFIER] ASC);

